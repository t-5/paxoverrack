"""
QComboBox with support of checkboxes, inspired by code from:
https://gist.github.com/mistic100/c3b7f3eabc65309687153fe3e0a9a720
"""
from PyQt5.QtCore import QEvent, Qt
from PyQt5.QtGui import QStandardItemModel, QStandardItem
from PyQt5.QtWidgets import QComboBox, QStyledItemDelegate


class QCheckList(QComboBox):

    StateUnknown = 3
    NoneCheckedText = "No FRD Dialog curves"
    AllCheckedText = "All FRD Dialog curves"
    SomeItemsCheckedText = "Some FRD Dialog curves"

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self._model = QStandardItemModel()
        self.setModel(self._model)
        self.setEditable(True)
        self.lineEdit().setReadOnly(True)
        self.lineEdit().installEventFilter(self)
        self.setItemDelegate(QCheckListStyledItemDelegate(self))

        # noinspection PyUnresolvedReferences
        self.lineEdit().selectionChanged.connect(self.lineEdit().deselect)
        # noinspection PyUnresolvedReferences
        self.view().pressed.connect(self.on_itemPressed)
        # noinspection PyUnresolvedReferences
        self._model.dataChanged.connect(self.on_modelDataChanged)


    def addCheckItem(self, label, data, checkState):
        """
        Adds a item to the checklist (setChecklist must have been called)
        returns the new QStandardItem
        """
        item = QStandardItem(label)
        item.setCheckState(checkState)
        item.setData(data)
        item.setFlags(Qt.ItemIsUserCheckable | Qt.ItemIsEnabled)
        self._model.appendRow(item)
        self._updateText()
        return item


    def eventFilter(self, _object, _event):
        if _object == self.lineEdit() and _event.type() == QEvent.MouseButtonPress:
            self.showPopup()
            return True
        else:
            return False


    def item(self, idx):
        return self._model.item(idx)


    def rowCount(self):
        return self._model.rowCount()


    def _globalCheckState(self):
        """
        Computes the global state of the checklist:
         - if there is no item: StateUnknown
         - if there is at least one item partially checked: StateUnknown
         - if all items are checked: Qt::Checked
         - if no item is checked: Qt::Unchecked
         - else: Qt::PartiallyChecked
        """
        nbRows = self._model.rowCount()
        nbChecked = 0
        nbUnchecked = 0

        if nbRows == 0:
            return self.StateUnknown

        for i in range(nbRows):
            if self._model.item(i).checkState() == Qt.Checked:
                nbChecked += 1
            elif self._model.item(i).checkState() == Qt.Unchecked:
                nbUnchecked += 1
            else:
                return self.StateUnknown
        return nbChecked == nbRows and Qt.Checked or (nbUnchecked == nbRows and Qt.Unchecked or Qt.PartiallyChecked)


    def on_modelDataChanged(self):
        self._updateText()


    def on_itemPressed(self, index):
        item = self._model.itemFromIndex(index)
        if item.checkState() == Qt.Checked:
            item.setCheckState(Qt.Unchecked)
        else:
            item.setCheckState(Qt.Checked)


    def _updateText(self):
        text = ""
        globalCheckState = self._globalCheckState()
        if globalCheckState == Qt.Checked:
            text = self.AllCheckedText
        elif globalCheckState == Qt.Unchecked:
            text = self.NoneCheckedText
        elif globalCheckState == Qt.PartiallyChecked:
            for i in range(self._model.rowCount()):
                if self._model.item(i).checkState() == Qt.Checked:
                    if text != "":
                        text += ", "
                    text += self._model.item(i).text()
        else:
            text = self.SomeItemsCheckedText
        self.lineEdit().setText(text)


class QCheckListStyledItemDelegate(QStyledItemDelegate):

        def __init__(self, parent=None):
            super().__init__(parent=parent)


        def paint(self, painter_, option_, index_):
            option_.showDecorationSelected = False
            super().paint(painter_, option_, index_)
