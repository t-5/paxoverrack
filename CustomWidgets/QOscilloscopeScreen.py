from math import ceil

import numpy
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QColor, QFont, QPainter
from PyQt5.QtWidgets import QWidget

from helpers.functions import GRAPH_SR


class QOscilloscopeScreen(QWidget):

    # colors
    BG_COLOR = QColor(0, 0, 0)
    BORDER_COLOR = QColor(200, 200, 200)
    TICK_COLOR = QColor(100, 100, 100)
    SMALL_TICK_COLOR = QColor(50, 50, 50)
    # fonts & dimensions
    TIME_AXIS_FONTSIZE = 10
    TIME_AXIS_FONT = QFont("sans-serif", TIME_AXIS_FONTSIZE)
    BOTTOM_MARGIN = 25
    TICK_LENGTH = 7
    SMALL_TICK_LENGTH = 4


    def __init__(self, parent, line1Color, line2Color, marker1Color, marker2Color, sampleRate, signal_updateMarkerPositions):
        super().__init__(parent)
        self._parent = parent
        self._line1Color = line1Color
        self._line2Color = line2Color
        self._marker1Color = marker1Color
        self._marker2Color = marker2Color
        self._sampleRate = sampleRate
        self._signal_updateMarkerPositions = signal_updateMarkerPositions
        # internal state
        self._verticalZoom = 1.0
        self._samples1 = numpy.zeros(int(GRAPH_SR[0] / 10))
        self._samples2 = numpy.zeros(int(GRAPH_SR[0] / 10))
        self._startSample = 0
        self._endSample = self._samples1.size - 1
        # markers
        self._marker1 = 0
        self._marker2 = 0
        self.setMarker1(int(self._endSample * 0.1))
        self.setMarker2(int(self._endSample * 0.9))
        self._markerPixels = [-1, -1]
        # mouse states
        self._mouseLeftButtonPressed = False
        self._mouseRightButtonPressed = False


    def getEndSample(self):
        return self._endSample


    def getSampleSize(self):
        return self._samples1.size


    def getSamples1(self):
        return self._samples1


    def getSamples2(self):
        return self._samples1


    def getStartSample(self):
        return self._startSample


    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self._mouseLeftButtonPressed = True
            self._updateMarker(event.pos().x(), 1)
        elif event.button() == Qt.RightButton:
            self._mouseRightButtonPressed = True
            self._updateMarker(event.pos().x(), 2)


    def mouseMoveEvent(self, event):
        if self._mouseLeftButtonPressed:
            self._updateMarker(event.pos().x(), 1)
        if self._mouseRightButtonPressed:
            self._updateMarker(event.pos().x(), 2)


    def mouseReleaseEvent(self, event):
        if event.button() == Qt.LeftButton:
            self._mouseLeftButtonPressed = False
        elif event.button() == Qt.RightButton:
            self._mouseRightButtonPressed = False


    def paintEvent(self, event):
        qp = QPainter(self)
        rect = self.rect()
        w = rect.width()
        h = rect.height()
        # draw widget background
        qp.setPen(self.BORDER_COLOR)
        qp.setBrush(self.BG_COLOR)
        qp.drawRect(0, 0, w - 1, h - self.BOTTOM_MARGIN)
        # draw time axis
        pixels = w - 1
        startSeconds = self._startSample / self._sampleRate
        endSeconds = (self._endSample + 1) / self._sampleRate
        timeSpanSeconds = endSeconds - startSeconds
        if timeSpanSeconds == 0:
            return
        secondsToPixelFactor = (w - 3) / timeSpanSeconds
        # calculate time step for ticks
        divisor = 1000 # assume max 1000s
        while timeSpanSeconds / divisor <= 0.1:
            divisor = divisor / 10
        # calculate number of steps for small and normal ticks
        majorTimeStep = ceil(timeSpanSeconds / divisor) * divisor / 10
        minorTimeStep = majorTimeStep / 5
        majorSteps = int(timeSpanSeconds / majorTimeStep)
        minorSteps = int(timeSpanSeconds / minorTimeStep)
        # draw minor ticks
        startSecondsCeiled = ceil(startSeconds / minorTimeStep) * minorTimeStep
        t = startSecondsCeiled
        y1 = 1
        y2 = h - self.BOTTOM_MARGIN - 2
        y3 = h - self.BOTTOM_MARGIN
        y4 = y3 + self.SMALL_TICK_LENGTH
        while t < endSeconds:
            x = 1 + int(round(secondsToPixelFactor * (t - startSeconds)))
            qp.setPen(self.SMALL_TICK_COLOR)
            qp.drawLine(x, y1, x, y2)
            qp.setPen(self.BORDER_COLOR)
            qp.drawLine(x, y3, x, y4)
            t += minorTimeStep
        # draw major ticks
        startSecondsCeiled = ceil(startSeconds / majorTimeStep) * majorTimeStep
        t = startSecondsCeiled
        y1 = 1
        y2 = h - self.BOTTOM_MARGIN - 2
        y3 = h - self.BOTTOM_MARGIN
        y4 = y3 + self.TICK_LENGTH
        while t < endSeconds:
            x = 1 + int(round(secondsToPixelFactor * (t - startSeconds)))
            qp.setPen(self.TICK_COLOR)
            qp.drawLine(x, y1, x, y2)
            qp.setPen(self.BORDER_COLOR)
            qp.drawLine(x, y3, x, y4)
            t += majorTimeStep
        # draw time legend
        if timeSpanSeconds < 0.01:
            fmt = "%0.1f"
        else:
            fmt = "%0.0fms"
        qp.setPen(self.BORDER_COLOR)
        qp.setFont(self.TIME_AXIS_FONT)
        metrics = qp.fontMetrics()
        fontHeight = metrics.height()
        i = 0
        if pixels / minorSteps > metrics.width("100ms"):
            textTimeStep = minorTimeStep
            textSteps = minorSteps
        else:
            textTimeStep = majorTimeStep
            textSteps = majorSteps
        startSecondsCeiled = ceil(startSeconds / textTimeStep) * textTimeStep
        t = startSecondsCeiled
        yf = h - self.BOTTOM_MARGIN + self.TICK_LENGTH + 3
        while t <= endSeconds:
            s = fmt % (t * 1000)
            fontWidth = metrics.width(s)
            x = 1 + int(round(secondsToPixelFactor * (t - startSeconds) - fontWidth / 2))
            if i == 0:
                x = max(2, x)
            elif i == textSteps:
                x = min(w - 2 - fontWidth, x)
            qp.drawText(x, yf, fontWidth, fontHeight, Qt.AlignLeft | Qt.AlignTop, s)
            i += 1
            t += textTimeStep
        # draw curves
        step = (self._endSample - self._startSample) / pixels
        verticalScale = (h - self.BOTTOM_MARGIN - 2) / 2 - 1
        verticalOffset = verticalScale + 2.49
        verticalScale = verticalScale * self._verticalZoom * -1
        # draw zero line
        qp.setPen(self.BORDER_COLOR)
        qp.drawLine(0, int(round(verticalOffset)), w, int(round(verticalOffset)))
        # maximum and minimum y values
        yMin = 1
        yMax = h - self.BOTTOM_MARGIN - 1
        lineColors = (self._line1Color, self._line2Color)
        qp.setRenderHint(QPainter.Antialiasing)
        # draw line1 and line2
        for lineIdx, samples in ((0, self._samples1), (1, self._samples2)):
            qp.setPen(lineColors[lineIdx])
            startIdx = self._startSample + 0.49
            endIdx = self._startSample + step
            if int(startIdx) == int(endIdx):
                v = samples[int(startIdx)]
            else:
                v = numpy.mean(samples[int(startIdx):int(endIdx)])
            prevY = int(verticalOffset + verticalScale * v)
            x = 0
            startIdx += step
            endIdx += step
            if step >= 2:
                while int(endIdx) <= self._endSample:
                    v = numpy.mean(samples[int(startIdx):int(endIdx)])
                    y = min(yMax, max(yMin, int(verticalOffset + verticalScale * v)))
                    qp.drawLine(x, prevY, x + 1, y)
                    prevY = y
                    x += 1
                    startIdx += step
                    endIdx += step
            else:
                xStep = pixels / (self._endSample - self._startSample)
                for idx in range(self._startSample + 1, self._endSample + 1):
                    x = int((idx - self._startSample - 1) * xStep) + 1
                    x2 = int((idx - self._startSample) * xStep) + 1
                    y = min(yMax, max(yMin, int(verticalOffset + verticalScale * samples[idx])))
                    qp.drawLine(x, prevY, x2, y)
                    prevY = y
        # draw markers
        for idx, markerColor, markerSample in ((0, self._marker1Color, self._marker1),
                                               (1, self._marker2Color, self._marker2)):
            if self._startSample <= markerSample <= self._endSample:
                x = pixels / (self._endSample - self._startSample) * (markerSample - self._startSample)
                qp.setPen(markerColor)
                qp.drawLine(int(round(x)), int(round(yMin)), int(round(x)), int(round(yMax)))
                self._markerPixels[idx] = x
            else:
                self._markerPixels[idx] = -1


    def setEndSample(self, endSample):
        if endSample <= self._startSample:
            raise ValueError("endSample must be > startSample")
        self._endSample = endSample
        self.update()


    def setVerticalZoom(self, zf):
        self._verticalZoom = zf
        self.update()


    def setMarker1(self, marker1):
        self._marker1 = marker1
        self._signal_updateMarkerPositions.emit(marker1, self._marker2)


    def setMarker2(self, marker2):
        self._marker2 = marker2
        self._signal_updateMarkerPositions.emit(self._marker1, marker2)


    def setSamples(self, samples1, samples2):
        if samples1.size != samples2.size:
            raise ValueError("size of both sample arrays must be equal.")
        self._samples1 = samples1
        self._samples2 = samples2
        self._startSample = 0
        self._endSample = samples1.size - 1
        self.update()


    def setStartEndSample(self, startSample, endSample):
        self._startSample = startSample
        self._endSample = endSample
        self.update()


    def setStartSample(self, startSample, ignoreError=False):
        if not ignoreError and startSample >= self._endSample:
            raise ValueError("startSample must be < endSample")
        self._startSample = startSample
        self.update()


    def showEvent(self, event):
        self._signal_updateMarkerPositions.emit(self._marker1, self._marker2)


    def _updateMarker(self, x, num):
        markerAttr = "_marker%s" % num
        pixels = self.rect().width() - 2
        if x < 1:
            return
        if x > pixels + 1:
            return
        newPos = int(round(((x - 1) / pixels) * (self._endSample - self._startSample))) + self._startSample
        setattr(self, markerAttr, newPos)
        self._signal_updateMarkerPositions.emit(self._marker1, self._marker2)
        self.update()

