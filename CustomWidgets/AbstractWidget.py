from PyQt5 import QtCore, QtGui
from PyQt5.QtCore import QRect, QSize, Qt
from PyQt5.QtGui import QColor, QFont, QPainter, QPainterPath, QPen
from PyQt5.QtWidgets import QMenu, QMessageBox, QToolButton, QWidget

from helpers.functions import bezierIntermediatesFor


class AbstractWidget(QWidget):

    BGCOLOR = QColor(0, 0, 0)
    BUTTON_HEIGHT = 25
    BUTTON_WIDTH = 25
    BUTTON_ICONSIZE = 21
    BUTTON_STYLESHEET = "background-color: #aaaaaa; padding: 3px;"
    CAPTION_FONT = 'DejaVu Sans', 11, QFont.Bold
    CAPTION_COLOR = QColor(255, 255, 255)
    CAPTION_HEIGHT = 28
    CAPTION_LEFTMARGIN = 6
    DOT_RADIUS = 6
    DOT_LINECOLOR = QColor(255, 255, 255)
    DOT_BGCOLOR_UNCONNECTED = QColor(0, 0, 0)
    DOT_BGCOLOR_CONNECTED = QColor(255, 255, 255)
    GRAPH_BGCOLOR = QColor(51, 51, 51)
    GRAPH_X = 13
    GRAPH_Y = 36
    GRAPH_WIDTH = 245
    GRAPH_HEIGHT = 95
    INFO_COLOR = QColor(200, 200, 170)
    INFO_COLOR2 = QColor(255, 255, 255)
    INFO_FONT = 'DejaVu Sans', 9, QFont.Normal
    INFO_TOPMARGIN = 3
    INFO_LEFTMARGIN = 6
    INFO_LEFTMARGIN_EXT = 16
    TEXT_COLOR = QColor(255, 255, 255)

    def __init__(self, parent, xoverObject, x=0, y=0):
        super().__init__(parent=parent)
        self._xoverObject = xoverObject
        self._mainWindow = parent.mainWindow()
        self._unzoomedX = x
        self._unzoomedY = y
        self._connectDragStartPos = None
        self._dragStartPosition = None
        self._dragStartWidgetPosition = None
        self.resizeToZoomedSize()
        parent.addWidget(self)
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        # noinspection PyUnresolvedReferences
        self.customContextMenuRequested.connect(self.openContextMenu)


    def connectWidget(self, widget):
        self._xoverObject.connect(widget.xoverObject())
        self._xoverObject.updateOutputFrequencyResponsePngs()
        self._mainWindow.markDirty()


    def connectionPaths(self):
        ret = []
        for widget in self._xoverObject.outputWidgets():
            path = QPainterPath()
            # start
            start = self.outputPos()
            startX = start.x()
            startY = start.y()
            # dest
            dest = widget.inputPos()
            destX = dest.x()
            destY = dest.y()
            im1X, im1Y, im2X, im2Y = bezierIntermediatesFor(start, dest)
            # draw path
            path.moveTo(startX, startY)
            path.cubicTo(im1X, im1Y, im2X, im2Y, destX, destY)
            ret.append(path)
        return ret


    def disconnectWidget(self, widget):
        self._xoverObject.disconnect(widget.xoverObject())
        self._xoverObject.updateOutputFrequencyResponsePngs()
        self._mainWindow.markDirty()


    def inputDotRect(self):
        x = self.x()
        y = self.y() + int(round(self.height() / 2)) - self._zoom(self.DOT_RADIUS)
        wh = self._zoom(self.DOT_RADIUS * 2)
        return QRect(x, y, wh, wh)


    def mainWindow(self):
        return self._mainWindow


    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            if self.myOutputDotRect().contains(event.pos()):
                self._connectDragStartPos = event.pos()
                # noinspection PyUnresolvedReferences
                self.parent().connectDragStart(self.pos() + event.pos(), "ltr")
            elif self.myInputDotRect().contains(event.pos()):
                self._connectDragStartPos = event.pos()
                # noinspection PyUnresolvedReferences
                self.parent().connectDragStart(self.pos() + event.pos(), "rtl")
            else:
                self._dragStartPosition = event.pos()
                self._dragStartWidgetPosition = self.pos()


    def mouseMoveEvent(self, event):
        if self._dragStartPosition is not None:
            # noinspection PyUnresolvedReferences
            destination = self._dragStartWidgetPosition + event.pos() - self._dragStartPosition
            self.move(max(0, destination.x()), max(0, destination.y()))
            self._unzoomedX = self._unzoom(max(0, destination.x()))
            self._unzoomedY = self._unzoom(max(0, destination.y()))
            self._dragStartWidgetPosition = self.pos()
            self.parent().repaint()
        elif self._connectDragStartPos is not None:
            # noinspection PyUnresolvedReferences
            self.parent().connectDragMove(self.pos() + event.pos())


    def mouseReleaseEvent(self, event):
        if self._dragStartPosition is not None:
            self._dragStartWidgetPosition = None
            self._dragStartPosition = None
            self._moveXoverObject()
            self._mainWindow.markDirty()
        elif self._connectDragStartPos is not None:
            # noinspection PyUnresolvedReferences
            self.parent().connectDragStop(self.pos() + event.pos(), self)


    def moveAbsolute(self, x, y):
        """ unzoom and save new coordinates """
        self._unzoomedX = self._unzoom(x)
        self._unzoomedY = self._unzoom(y)


    def myInputDotRect(self):
        wh = self._zoom(self.DOT_RADIUS * 2)
        x = 0
        y = int(round((self.height()) / 2)) - self._zoom(self.DOT_RADIUS)
        return QRect(x, y, wh, wh)


    def myOutputDotRect(self):
        wh = self._zoom(self.DOT_RADIUS * 2)
        x = self.width() - wh
        y = int(round((self.height()) / 2)) - self._zoom(self.DOT_RADIUS)
        return QRect(x, y, wh, wh)


    def onContextMenuDisconnectClicked(self):
        self.sender().xoverObjectInput.disconnect(self.sender().xoverObjectOutput)


    def onDelete(self):
        msg = "Do you want to delete '%s'?" % self._xoverObject.sinkName()
        reply = QMessageBox().question(self, 'Really delete?',
                                       msg,
                                       QMessageBox.No | QMessageBox.Yes,
                                       QMessageBox.Yes)
        if reply == QMessageBox.Yes:
            self.parent().removeWidget(self)
            self._xoverObject.disconnectLinks()
            self._xoverObject.disconnectAll()
            self._mainWindow.delObject(self._xoverObject)
            self._mainWindow.updateStatusBar()


    def onEdit(self):
        raise NotImplementedError("Abstract method AbstractWidget::onEdit() called.")


    def openContextMenu(self, position):
        """ show context menu when over input or output dots """
        menu = QMenu()
        showMenu = False
        i = 0
        if self.myInputDotRect().contains(position):
            for input_ in self._xoverObject.inputs():
                showMenu = True
                menu.addAction("Disconnect '%s'" % input_.sinkName(),
                               self.onContextMenuDisconnectClicked)
                action = menu.actions()[i]
                action.xoverObjectInput = input_
                action.xoverObjectOutput = self._xoverObject
                i += 1
        if self.myOutputDotRect().contains(position):
            for output in self._xoverObject.outputs():
                menu.addAction("Disconnect '%s'" % output.sinkName(),
                               self.onContextMenuDisconnectClicked)
                action = menu.actions()[i]
                action.xoverObjectInput = self._xoverObject
                action.xoverObjectOutput = output
                showMenu = True
                i += 1
        if showMenu:
            menu.exec_(self.mapToGlobal(position))


    def outputDotRect(self):
        wh = self._zoom(self.DOT_RADIUS * 2)
        x = self.x() + self.width() - wh
        y = self.y() + int(round(self.height() / 2)) - self._zoom(self.DOT_RADIUS)
        return QRect(x, y, wh, wh)


    def paintEvent(self, e):
        qp = QPainter()
        qp.begin(self)
        self.paint(qp)
        qp.end()


    def repaint(self):
        self.resizeToZoomedSize()
        super().repaint()


    def resizeToZoomedSize(self):
        self.setGeometry(self._zoom(self._unzoomedX), self._zoom(self._unzoomedY),
                         self._zoom(self.WIDTH), self._zoom(self.HEIGHT))


    def xoverObject(self):
        return self._xoverObject


    # noinspection PyUnresolvedReferences
    def _addCaptionButtons(self):
        # delete button
        self._deleteButton = QToolButton(self)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/MainWindow/close.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self._deleteButton.setStyleSheet(self.BUTTON_STYLESHEET)
        self._deleteButton.setIcon(icon)
        self._deleteButton.setToolTip("Delete...")
        self._deleteButton.clicked.connect(self.onDelete)
        # edit button
        self._editButton = QToolButton(self)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/MainWindow/edit.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self._editButton.setStyleSheet(self.BUTTON_STYLESHEET)
        self._editButton.setIcon(icon)
        self._editButton.setToolTip("Edit...")
        self._editButton.clicked.connect(self.onEdit)
        # resize buttons
        self.resizeToZoomedSize()


    def _drawCaption(self, qp, leftMargin=DOT_RADIUS+1, rightMargin=DOT_RADIUS):
        qp.setFont(QFont(self.CAPTION_FONT[0], self._zoom(self.CAPTION_FONT[1]), self.CAPTION_FONT[2]))
        qp.setPen(self.LINECOLOR)
        qp.setBrush(self.CAPTION_BGCOLOR)
        qp.drawRect(self._zoom(leftMargin),
                    0,
                    self.width() - 1 - self._zoom(leftMargin) - self._zoom(rightMargin),
                    self._zoom(self.CAPTION_HEIGHT))
        metrics = qp.fontMetrics()
        fontHeight = metrics.height()
        l = self._zoom(self.CAPTION_LEFTMARGIN + leftMargin)
        t = int(round((self._zoom(self.CAPTION_HEIGHT) - fontHeight) / 2))
        w = self._zoom(self.WIDTH - 2 * self.CAPTION_LEFTMARGIN - leftMargin - rightMargin)
        h = fontHeight
        qp.setPen(self.CAPTION_COLOR)
        qp.drawText(l, t, w, h, Qt.AlignLeft | Qt.AlignTop, self._xoverObject.sinkName())


    def _drawInputDot(self, qp):
        pen = QPen(self.DOT_LINECOLOR)
        pen.setWidth(2)
        qp.setPen(pen)
        x = 1
        y = int(round(self.height() / 2)) - self._zoom(self.DOT_RADIUS)
        w = self._zoom(2 * self.DOT_RADIUS) - 1
        h = w
        if self._xoverObject.hasInputConnections():
            qp.setBrush(self.DOT_BGCOLOR_CONNECTED)
            qp.drawEllipse(x, y, w, h)
        else:
            qp.drawArc(x, y, w, h, 90 * 16, 180 * 16)


    def _drawOutputDot(self, qp):
        pen = QPen(self.DOT_LINECOLOR)
        pen.setWidth(2)
        qp.setPen(pen)
        x = self.width() - self._zoom(2 * self.DOT_RADIUS)
        y = int(round(self.height() / 2)) - self._zoom(self.DOT_RADIUS)
        w = self._zoom(2 * self.DOT_RADIUS) - 1
        h = w
        if self._xoverObject.hasOutputConnections():
            qp.setBrush(self.DOT_BGCOLOR_CONNECTED)
            qp.drawEllipse(x, y, w, h)
        else:
            qp.drawArc(x, y, w, h, 90 * 16, -180 * 16)


    def _moveXoverObject(self):
        self._xoverObject.setWidgetX(self._unzoom(self.x()))
        self._xoverObject.setWidgetY(self._unzoom(self.y()))


    def _resizeCaptionButtons(self):
        iconsize = self._zoom(self.BUTTON_ICONSIZE)
        margin = int(round(self._zoom(self.CAPTION_HEIGHT - self.BUTTON_HEIGHT) / 2))
        rightMargin = self._buttonsRightMargin()
        buttonHeight = self._zoom(self.BUTTON_WIDTH)
        buttonWidth = self._zoom(self.BUTTON_HEIGHT)
        try:
            self._deleteButton.setGeometry(
                    self._zoom(self.WIDTH - self.BUTTON_WIDTH) - margin - rightMargin,
                    margin,
                    buttonHeight,
                    buttonWidth)
        except AttributeError:
            pass
        try:
            self._editButton.setGeometry(
                    self._zoom(self.WIDTH - self.BUTTON_WIDTH * 2) - margin * 2 - rightMargin,
                    margin,
                    buttonHeight,
                    buttonWidth)
        except AttributeError:
            pass
        try:
            self._frequencyResponseButton.setGeometry(
                    self._zoom(self.WIDTH - self.BUTTON_WIDTH * 3) - margin * 3 - rightMargin,
                    margin,
                    buttonHeight,
                    buttonWidth)
            self._frequencyResponseButton.setIconSize(QSize(iconsize, iconsize))
        except AttributeError:
            pass
        try:
            self._frdButton.setGeometry(
                    self._zoom(self.WIDTH - self.BUTTON_WIDTH * 3) - margin * 3 - rightMargin,
                    margin,
                    buttonHeight,
                    buttonWidth)
        except AttributeError:
            pass


    def _unzoom(self, value):
        return int(round(value / self._mainWindow.zoomFactor()))


    def _resizeFrequencyResponseLabel(self):
        if getattr(self, "_frequencyResponseLabel", None) is None:
            return
        self._frequencyResponseLabel.setGeometry(self._zoom(self.GRAPH_X),
                                                 self._zoom(self.GRAPH_Y),
                                                 self._zoom(self.GRAPH_WIDTH),
                                                 self._zoom(self.GRAPH_HEIGHT))


    def _zoom(self, value):
        return int((value * self._mainWindow.zoomFactor()))
