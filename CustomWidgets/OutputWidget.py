from PyQt5 import QtGui
from PyQt5.QtCore import Qt, QPoint, QRect
from PyQt5.QtGui import QColor, QFont
from PyQt5.QtWidgets import QToolButton, QLabel

from CustomWidgets.AbstractWidget import AbstractWidget
from WindowClasses.EditOutputDialog import EditOutputDialog
from WindowClasses.FrequencyResponseDialog import FrequencyResponseDialog
from helpers.constants import OUTPUT_CHANNELS_FRONT, OUTPUT_CHANNELS_SIDE, OUTPUT_CHANNELS_REAR, \
    OUTPUT_CHANNELS_CENTER_LFE


class OutputWidget(AbstractWidget):

    COLOR_OUTPUT_CHANNELS_MAPPING = {
        OUTPUT_CHANNELS_FRONT: QColor(50, 200, 50),
        OUTPUT_CHANNELS_CENTER_LFE: QColor(200, 140, 0),
        OUTPUT_CHANNELS_SIDE: QColor(120, 120, 120),
        OUTPUT_CHANNELS_REAR: QColor(0, 0, 0),
    }
    COLOR_OUTPUT_CHANNELS_UNKNOWN = QColor(100, 0, 0)
    MARKER_WIDTH = 7

    CAPTION_BGCOLOR = QColor(102, 0, 0)
    LINECOLOR = QColor(255, 180, 180)
    HEIGHT = 137
    WIDTH = 270

    def __init__(self, parent, xoverObject, x=0, y=0):
        super().__init__(parent=parent, xoverObject=xoverObject, x=x, y=y)
        self._showingFrequencyResponse = False
        self._addCaptionButtons()
        self._frequencyResponseLabel = QLabel("", self)
        self._frequencyResponseLabel.hide()
        self._frequencyResponseLabel.setScaledContents(True)
        self._frequencyResponseLabel.mousePressEvent = self.onFrequencyResponseLabelClicked
        self.updateFrequencyResponseLabel()
        self._resizeFrequencyResponseLabel()


    def connectionPaths(self):
        return ()


    def inputPos(self):
        return QPoint(self.x(), int(round(self.y() + self.height() / 2)))


    def onEdit(self):
        dlg = EditOutputDialog(self._mainWindow, self._xoverObject)
        dlg.exec()
        self.mainWindow().markDirty()
        self.mainWindow().updateStatusBar()


    # noinspection PyUnusedLocal
    def onFrequencyResponseLabelClicked(self, ignore):
        dlg = FrequencyResponseDialog(
            "Cumulated frequency response of '%s'" % self._xoverObject.sinkName(),
            self._xoverObject.frequencyResponsePixmap())
        dlg.exec()
        dlg.deleteLater()


    def onShowFrequencyResponse(self):
        self._showingFrequencyResponse = not self._showingFrequencyResponse
        icon = QtGui.QIcon()
        disable = self._showingFrequencyResponse and "_disable" or ""
        icon.addPixmap(QtGui.QPixmap(":/MainWindow/frequency_response%s.png" % disable),
                       QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self._frequencyResponseButton.setIcon(icon)
        if self._showingFrequencyResponse:
            self.updateFrequencyResponseLabel()
            self._frequencyResponseLabel.show()
        else:
            self._frequencyResponseLabel.hide()
        self.repaint()


    def outputDotRect(self):
        return QRect(0, 0, 0, 0)


    @staticmethod
    def outputWidgets():
        return ()


    def paint(self, qp):
        self._drawInputDot(qp)
        # overall rectangle
        qp.setPen(self.LINECOLOR)
        qp.setBrush(self.BGCOLOR)
        qp.drawRect(self._zoom(self.DOT_RADIUS) + 1,
                    0,
                    self.width() - 2 - self._zoom(self.DOT_RADIUS),
                    self.height() - 1)
        # output surround color marker
        qp.setPen(self.LINECOLOR)
        qp.setBrush(self._outputMarkerColor())
        qp.drawRect(self.width() - self._zoom(self.MARKER_WIDTH + 1),
                    0,
                    self._zoom(self.MARKER_WIDTH),
                    self.height() - 1)
        if self._showingFrequencyResponse:
            # graph rectangle
            qp.setPen(self.GRAPH_BGCOLOR)
            qp.setBrush(self.GRAPH_BGCOLOR)
            qp.drawRect(self._zoom(self.DOT_RADIUS) + 2,
                        self._zoom(self.GRAPH_Y - 8) + 1,
                        self.width() - 4 - self._zoom(self.DOT_RADIUS),
                        self._zoom(self.GRAPH_HEIGHT + 11) - 2)
        else:
            self._drawInfo(qp)
        self._drawCaption(qp, rightMargin=0)


    def resizeToZoomedSize(self):
        super().resizeToZoomedSize()
        self._resizeCaptionButtons()
        self._resizeFrequencyResponseLabel()


    def _addCaptionButtons(self):
        # show frequency response button
        self._frequencyResponseButton = QToolButton(self)
        icon = QtGui.QIcon()
        disable = self._showingFrequencyResponse and "_disable" or ""
        icon.addPixmap(QtGui.QPixmap(":/MainWindow/frequency_response%s.png" % disable),
                       QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self._frequencyResponseButton.setStyleSheet(self.BUTTON_STYLESHEET)
        self._frequencyResponseButton.setIcon(icon)
        self._frequencyResponseButton.setToolTip("Edit...")
        # noinspection PyUnresolvedReferences
        self._frequencyResponseButton.clicked.connect(self.onShowFrequencyResponse)
        # add edit and delete buttons
        super()._addCaptionButtons()


    def updateFrequencyResponseLabel(self):
        self._frequencyResponseLabel.setPixmap(self._xoverObject.frequencyResponsePixmap())


    @staticmethod
    def _buttonsRightMargin():
        return 0


    def _drawInfo(self, qp):
        font = QFont(self.INFO_FONT[0], self._zoom(self.INFO_FONT[1]), self.INFO_FONT[2])
        font2 = QFont(self.INFO_FONT[0], self._zoom(self.INFO_FONT[1]), self.INFO_FONT[2])
        qp.setFont(font)
        metrics = qp.fontMetrics()
        fontHeight = metrics.height()
        l = self._zoom(self.INFO_LEFTMARGIN + self.DOT_RADIUS)
        le = self._zoom(self.INFO_LEFTMARGIN_EXT)
        t = self._zoom(self.CAPTION_HEIGHT + self.INFO_TOPMARGIN) + 1
        w = self._zoom(self.WIDTH - 2 * self.CAPTION_LEFTMARGIN - self.DOT_RADIUS)
        h = fontHeight
        qp.setPen(self.INFO_COLOR)
        qp.drawText(l, t, w, h, Qt.AlignLeft | Qt.AlignTop, "Output Sink:")
        t += self._zoom(self.INFO_TOPMARGIN) + fontHeight
        qp.setPen(self.INFO_COLOR2)
        qp.setFont(font2)
        qp.drawText(le, t, w, h, Qt.AlignLeft | Qt.AlignTop, self._xoverObject.outputSinkDescription())
        t += self._zoom(self.INFO_TOPMARGIN) + fontHeight
        qp.setPen(self.INFO_COLOR)
        qp.setFont(font)
        qp.drawText(l, t, w, h, Qt.AlignLeft | Qt.AlignTop, "Output Channels:")
        t += self._zoom(self.INFO_TOPMARGIN) + fontHeight
        qp.setPen(self.INFO_COLOR2)
        qp.setFont(font2)
        mono = self._xoverObject.mono() and " (mono)" or ""
        for channel in self._xoverObject.outputSinkChannels():
            qp.drawText(le, t, w, h, Qt.AlignLeft | Qt.AlignTop, channel + mono)
            t += self._zoom(self.INFO_TOPMARGIN) + fontHeight


    def _outputMarkerColor(self):
        return self.COLOR_OUTPUT_CHANNELS_MAPPING.get(tuple(self._xoverObject.outputSinkChannels()),
                                                      self.COLOR_OUTPUT_CHANNELS_UNKNOWN)