from PyQt5.QtCore import QPoint, Qt
from PyQt5.QtGui import QColor, QFont
from PyQt5.QtWidgets import QLabel

from CustomWidgets.AbstractWidget import AbstractWidget
from WindowClasses.EditFilterDialog import EditFilterDialog
from WindowClasses.FrdComparisonDialog import FrdComparisonDialog


class FilterWidget(AbstractWidget):

    LINECOLOR = QColor(180, 180, 255)
    CAPTION_BGCOLOR = QColor(0, 0, 102)
    HEIGHT = 176
    WIDTH = 270


    def __init__(self, parent, xoverObject, x=0, y=0):
        super().__init__(parent=parent, xoverObject=xoverObject, x=x, y=y)
        self._frdButton = None
        self._frdDialog = None
        self._addCaptionButtons()
        self._frequencyResponseLabel = QLabel("", self)
        self._frequencyResponseLabel.setScaledContents(True)
        self.updateFrequencyResponseLabel()
        self._resizeFrequencyResponseLabel()


    def filterName(self):
        return self._xoverObject.filterName()


    def inputPos(self):
        return QPoint(self.x(), int(round(self.y() + self.height() / 2)))


    def onEdit(self):
        dlg = EditFilterDialog(self._mainWindow, self._xoverObject)
        dlg.exec()
        self.updateFrequencyResponseLabel()
        dlg.deleteLater()
        self.mainWindow().markDirty()


    def onFrdDialog(self):
        if self._frdDialog is None:
            self._frdDialog = FrdComparisonDialog(self._mainWindow, self._xoverObject)
            self._frdDialog.show()
            self._mainWindow.addFrdDialog(self._frdDialog)
        else:
            self._frdDialog.show()
            self._frdDialog.raise_()
            self._frdDialog.activateWindow()


    def outputPos(self):
        return QPoint(self.x() + self.width(), int(round(self.y() + self.height() / 2)))


    @staticmethod
    def outputWidgets():
        return ()


    def paint(self, qp):
        self._drawInputDot(qp)
        self._drawOutputDot(qp)
        # overall rectangle
        qp.setPen(self.LINECOLOR)
        qp.setBrush(self.BGCOLOR)
        qp.drawRect(self._zoom(self.DOT_RADIUS) + 1,
                    0,
                    self.width() - 2 - self._zoom(2 * self.DOT_RADIUS),
                    self.height() - 1)
        # graph rectangle
        qp.setBrush(self.GRAPH_BGCOLOR)
        qp.drawRect(self._zoom(self.DOT_RADIUS) + 1,
                    self._zoom(self.GRAPH_Y - 8),
                    self.width() - 2 - self._zoom(2 * self.DOT_RADIUS),
                    self._zoom(self.GRAPH_HEIGHT + 12))
        self._drawCaption(qp)
        self._drawInfo(qp)


    def resetFrdDialog(self):
        self._frdDialog = None


    def resizeToZoomedSize(self):
        super().resizeToZoomedSize()
        self._resizeCaptionButtons()
        self._resizeFrequencyResponseLabel()


    def updateFrequencyResponseLabel(self):
        self._frequencyResponseLabel.setPixmap(self._xoverObject.frequencyResponsePixmap())


    def _buttonsRightMargin(self):
        return self._zoom(self.DOT_RADIUS)


    def _drawInfo(self, qp):
        # fonts & metrics
        font = QFont(self.INFO_FONT[0], self._zoom(self.INFO_FONT[1]), self.INFO_FONT[2])
        font2 = QFont(self.INFO_FONT[0], self._zoom(self.INFO_FONT[1]), self.INFO_FONT[2])
        font3 = QFont(self.INFO_FONT[0], self._zoom(self.INFO_FONT[1]), self.INFO_FONT[2])
        font3.setBold(True)
        qp.setFont(font)
        metrics = qp.fontMetrics()
        # calculate positions and dimensions
        x = self._zoom(self.INFO_LEFTMARGIN + self.DOT_RADIUS)
        y = self._zoom(self.CAPTION_HEIGHT + self.GRAPH_HEIGHT + 16)
        h = metrics.height()
        maxW = self._zoom(self.WIDTH - 2 * self.INFO_LEFTMARGIN - 2 * self.DOT_RADIUS)
        # draw filter name
        qp.setFont(font3)
        qp.setPen(self.INFO_COLOR2)
        qp.drawText(x, y, maxW, h, Qt.AlignLeft | Qt.AlignTop, self.filterName())
        y += h + self._zoom(1)
        # draw text info
        i = 0
        # split info into outer parts and cycle through them
        outerInfoParts = self._xoverObject.info().split("|")
        for outerInfoPart in outerInfoParts:
            # split outer info part into inner parts
            infoParts = outerInfoPart.split(":")
            # draw inner part 1
            qp.setFont(font)
            qp.setPen(self.INFO_COLOR)
            if i > 0:
                s = "|"
            else:
                s = ""
            s += infoParts[0] + ":"
            w = metrics.width(s)
            qp.drawText(x, y, maxW, h, Qt.AlignLeft | Qt.AlignTop, s)
            x += w
            maxW -= w
            # draw inner part 2
            qp.setFont(font2)
            qp.setPen(self.INFO_COLOR2)
            w = metrics.width(infoParts[1])
            qp.drawText(x, y, maxW, h, Qt.AlignLeft | Qt.AlignTop, infoParts[1])
            x += w
            maxW -= w
            # finally increment i
            i += 1
