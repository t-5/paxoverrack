from PyQt5.QtCore import Qt, QPoint, QRect
from PyQt5.QtGui import QColor, QFont

from CustomWidgets.AbstractWidget import AbstractWidget


class InputWidget(AbstractWidget):

    LINECOLOR = QColor(180, 255, 180)
    CAPTION_BGCOLOR = QColor(0, 102, 0)
    HEIGHT = 90
    WIDTH = 130


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


    def inputDotRect(self):
        return QRect(0, 0, 0, 0)


    def onEdit(self):
        raise NotImplemented("InputWidget has no onEdit() method!")


    def outputPos(self):
        return QPoint(self.x() + self.width(), int(round(self.y() + self.height() / 2)))


    def paint(self, qp):
        self._drawOutputDot(qp)
        qp.setPen(self.LINECOLOR)
        qp.setBrush(self.BGCOLOR)
        qp.drawRect(0, 0, self.width() - 1 - self._zoom(self.DOT_RADIUS), self.height() - 1)
        self._drawCaption(qp, leftMargin=0)
        self._drawInfo(qp)


    def _drawInfo(self, qp):
        font = QFont(self.INFO_FONT[0], self._zoom(self.INFO_FONT[1]), self.INFO_FONT[2])
        font.setItalic(True)
        font2 = QFont(self.INFO_FONT[0], self._zoom(self.INFO_FONT[1]), self.INFO_FONT[2])
        qp.setFont(font)
        metrics = qp.fontMetrics()
        fontHeight = metrics.height()
        l = self._zoom(self.INFO_LEFTMARGIN)
        le = self._zoom(self.INFO_LEFTMARGIN_EXT)
        t = self._zoom(self.CAPTION_HEIGHT + self.INFO_TOPMARGIN) + 1
        w = self._zoom(self.WIDTH - 2 * self._zoom(self.CAPTION_LEFTMARGIN))
        h = fontHeight
        qp.setPen(self.INFO_COLOR)
        qp.setFont(font)
        qp.drawText(l, t, w, h, Qt.AlignLeft | Qt.AlignTop, "Input Channels:")
        t += self._zoom(self.INFO_TOPMARGIN) + fontHeight
        qp.setPen(self.INFO_COLOR2)
        qp.setFont(font2)
        for channel in ("front-left", "front-right"):
            qp.drawText(le, t, w, h, Qt.AlignLeft | Qt.AlignTop, channel)
            t += self._zoom(self.INFO_TOPMARGIN) + fontHeight

