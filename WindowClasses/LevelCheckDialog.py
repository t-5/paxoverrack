from time import time

import numpy
from PyQt5.QtCore import QTimer, pyqtSignal
from PyQt5.QtWidgets import QDialog
from designer_qt5.Ui_LevelCheckDialog import Ui_LevelCheckDialog
from qt5_t5darkstyle import darkstyle_css

from helpers.functions import dbToGainFactor, prettyDbFromFactor, GRAPH_SR
from helpers.noise import noise_generator
from helpers.threads import SoundcardPlaybackCaptureThread


class LevelCheckDialog(QDialog, Ui_LevelCheckDialog):


    signal_processCapturedSamples = pyqtSignal(numpy.ndarray)


    NOISE_TYPES = ('white', 'pink', 'blue', 'brown', 'violet')


    def __init__(self, mainWindow, inputChannels):
        super().__init__()
        self.setupUi(self)
        self.setStyleSheet(darkstyle_css())
        self._mainWindow = mainWindow
        self._inputChannels = inputChannels
        # setup noise types
        for t in self.NOISE_TYPES:
            self.comboBoxNoiseType.addItem(t)
        # internal state
        self._samplesToPlay = -1
        self._peakValues = [0, 0]
        self._started = time()
        # capture threads
        self._playbackCaptureThread = None
        # settings
        self._settings = mainWindow.settings()
        self._readIniSettings()
        # connect signals and actions
        self.pushButtonClose.clicked.connect(self.close)
        self.pushButtonStart.clicked.connect(self.onStartClicked)
        # noinspection PyUnresolvedReferences
        self.signal_processCapturedSamples.connect(self._processCapturedSamples)
        # progress bar update timer
        self._updateTimer = QTimer(self)
        self._updateTimer.setInterval(100)
        # noinspection PyUnresolvedReferences
        self._updateTimer.timeout.connect(self._updateProgressBar)


    def closeEvent(self, evt):
        if self._playbackCaptureThread is not None:
            self._playbackCaptureThread.stopAndWait()
        self._writeIniSettings()
        super().closeEvent(evt)


    def onClose(self):
        self.close()


    def onStartClicked(self):
        self._peakValues = [0, 0]
        self._samplesToPlay = self.spinBoxDuration.value() * GRAPH_SR[0]
        singleChannelNoise = numpy.array(noise_generator(self._samplesToPlay, self.comboBoxNoiseType.currentText()))
        # noinspection PyArgumentList
        absmax = numpy.abs(singleChannelNoise.max())
        singleChannelNoise = dbToGainFactor(self.spinBoxLevel.value()) * singleChannelNoise / absmax
        noise = numpy.array([singleChannelNoise] * 2).transpose()
        self._playbackCaptureThread = SoundcardPlaybackCaptureThread(GRAPH_SR[0], noise, finishedCallback=self.signal_processCapturedSamples)
        self._updateTimer.start()
        self._started = time()
        self._playbackCaptureThread.start()


    def _processCapturedSamples(self, data):
        self._updateTimer.stop()
        transposed = data.transpose()
        for i in range(2):
            self._peakValues[i] = max(self._peakValues[i], abs(transposed[i].max()))
        if self._inputChannels[0] == "Left" and self._inputChannels[1] == "Left":
            self._peakValues = (self._peakValues[0], self._peakValues[0])
        elif self._inputChannels[0] == "Left" and self._inputChannels[1] == "Right":
            self._peakValues = (self._peakValues[0], self._peakValues[1])
        elif self._inputChannels[0] == "Right" and self._inputChannels[1] == "Left":
            self._peakValues = (self._peakValues[1], self._peakValues[0])
        elif self._inputChannels[0] == "Right" and self._inputChannels[1] == "Right":
            self._peakValues = (self._peakValues[1], self._peakValues[1])
        self.labelMicrophonePeak.setText("Microphone: %s" % prettyDbFromFactor(self._peakValues[0]))
        self.labelLoopbackPeak.setText("Loopback: %s" % prettyDbFromFactor(self._peakValues[1]))
        self.progressBar.setValue(100)


    def _readIniSettings(self):
        s = self._settings
        # dialog geometry and settings
        try:
            x, y, w, h = (int(s.value("levelCheckDialog/x", None)),
                          int(s.value("levelCheckDialog/y", None)),
                          int(s.value("levelCheckDialog/width", None)),
                          int(s.value("levelCheckDialog/height", None)))
        except (TypeError, ValueError):
            x, y, w, h = None, None, None, None
        if None not in (x, y, w, h):
            self.setGeometry(x, y, w, h)
        # noise generator settings
        try:
            v = int(s.value("levelCheckDialog/duration", 2))
        except ValueError:
            v = 2
        self.spinBoxDuration.setValue(v)
        try:
            v = float(s.value("levelCheckDialog/level", -12))
        except ValueError:
            v = -12
        self.spinBoxLevel.setValue(v)
        v = s.value("levelCheckDialog/noiseType", "pink")
        if v in self.NOISE_TYPES:
            self.comboBoxNoiseType.setCurrentText(v)
        else:
            self.comboBoxNoiseType.setCurrentText("pink")


    def _updateProgressBar(self):
        self.progressBar.setValue(int(round(100.0 * (time() - self._started) / self.spinBoxDuration.value())))


    def _writeIniSettings(self):
        s = self._settings
        # dialog geometry
        s.setValue("levelCheckDialog/x", self.x())
        s.setValue("levelCheckDialog/y", self.y())
        s.setValue("levelCheckDialog/height", self.height())
        s.setValue("levelCheckDialog/width", self.width())
        # burst settings
        s.setValue("levelCheckDialog/duration", str(self.spinBoxDuration.value()))
        s.setValue("levelCheckDialog/level", str(self.spinBoxLevel.value()))
        s.setValue("levelCheckDialog/noiseType", str(self.comboBoxNoiseType.currentText()))
