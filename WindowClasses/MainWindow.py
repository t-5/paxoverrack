import argparse
import io
import math
import os
import re
import sys
from tempfile import mkstemp
from time import sleep
from traceback import print_exc

import numpy
from PyQt5.QtCore import QSettings, QTimer, QUrl
from PyQt5.QtGui import QDesktopServices, QFont
from PyQt5.QtWidgets import QAction, QFileDialog, QInputDialog, QLabel, QMainWindow, QMessageBox, QScrollArea, \
    QSizePolicy
from designer_qt5.Ui_MainWindow import Ui_MainWindow
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

from DataClasses.XoverFile import XoverFile
from DataClasses.XoverFoHighpass import XoverFoHighpass
from DataClasses.XoverFoLowpass import XoverFoLowpass
from DataClasses.XoverInverter import XoverInverter
from DataClasses.XoverLinkwitzTransform import XoverLinkwitzTransform
from DataClasses.XoverLr2Highpass import XoverLr2Highpass
from DataClasses.XoverLr2Lowpass import XoverLr2Lowpass
from DataClasses.XoverLr4Highpass import XoverLr4Highpass
from DataClasses.XoverLr4Lowpass import XoverLr4Lowpass
from DataClasses.XoverOutput import XoverOutput
from DataClasses.XoverParamEq import XoverParamEq
from DataClasses.XoverSampleDelay import XoverSampleDelay
from DataClasses.XoverSoHighpass import XoverSoHighpass
from DataClasses.XoverSoHighpassWithQ import XoverSoHighpassWithQ
from DataClasses.XoverSoLowpass import XoverSoLowpass
from DataClasses.XoverSoLowpassWithQ import XoverSoLowpassWithQ
from DataClasses.XoverSubsampleDelay import XoverSubsampleDelay
from DataClasses.XoverToHighpass import XoverToHighpass
from DataClasses.XoverToLowpass import XoverToLowpass
from WindowClasses.AboutDialog import AboutDialog
from WindowClasses.ConsoleDialog import ConsoleDialog
from WindowClasses.DrawArea import DrawArea
from WindowClasses.EditFilterDialog import EditFilterDialog
from WindowClasses.EditOutputDialog import EditOutputDialog
from WindowClasses.FrMeasurementDialog import FrMeasurementDialog
from WindowClasses.GlobalFrdDialog import GlobalFrdDialog
from WindowClasses.MeasurementDialog import MeasurementDialog
from WindowClasses.TaMeasurementDialog import TaMeasurementDialog
from helpers.constants import CURRENT_DATAMODEL_VERSION, DEFAULT_PA_LINE2_FMT, DEFAULT_PA_LINE3_FMT, \
    DEFAULT_PA_LINE_FIRST, \
    DEFAULT_PA_LINE_LAST, FILTER_NAMES, ZOOM_STEPS
from helpers.functions import GRAPH_SR, cmdRunAndRead, configSnippetToCommandList, keepPhaseBounded, \
    pulse_get_default_sink, pulse_get_hw_sinks, pulse_get_sink_volume, pulse_get_status, pulse_set_sink_volume, \
    setupPltFigure

try:
    from ZODB.FileStorage.FileStorage import FileStorageError
except ImportError:
    FileStorageError = None # import checks actually done in _importCheck()


class MainWindow(QMainWindow, Ui_MainWindow):

    def __init__(self):
        super(MainWindow, self).__init__()
        # setup matplotlib canvas for drawing frequency response graphs things
        self._mplFigure = Figure()
        self._mplFigureSetup = False
        self._mplCanvas = FigureCanvas(self._mplFigure)
        self._mplCanvas.setParent(self)
        self._mplCanvas.setGeometry(0, 0, 490, 191)
        # setupUi after MPL Canvas is added -> MPL canvas will be hidden behind all the stuff
        self.setupUi(self)
        ## remove unwanted menu items
        self.actionShowMeasurementWindow.deleteLater()
        ## module import checks
        for modulename in ("pulsectl", "sounddevice", "ZODB", "pyfftw", "scipy"):
            self._importCheck(modulename)
        ## instance variables
        self._dlgPath = os.path.expanduser("~")
        self._settings = QSettings("PaXoverRack", "PaXoverRack")
        self._xoverFile = None
        self._zoomFactor = 1
        self._modulesInserted = False
        self._last_pulseaudio_default_sink = ""
        self._rereadHwStatus = True
        ## call setup helpers
        self._setupStatusBar()
        self._connectSignals()
        self._setupAddFilterComboBox()
        # setup QScrollArea and DrawArea
        self._scrollArea = QScrollArea()
        self._drawArea = DrawArea(parent=self._scrollArea, mainWindow=self)
        self._scrollArea.setWidget(self._drawArea)
        self.verticalLayout.addWidget(self._scrollArea)
        ## setup dialogs
        self._frdDialogs = []
        self._consoleDialog = ConsoleDialog(self)
        self._consoleDialog.hide()
        self._globalFrdDialog = GlobalFrdDialog(self)
        self._globalFrdDialog.hide()
        self._measurementDialog = MeasurementDialog(self)
        self._measurementDialog.hide()
        self._frMeasurementDialog = FrMeasurementDialog(self)
        self._frMeasurementDialog.hideEvent(None) # ensure threads are stopped.
        self._taMeasurementDialog = TaMeasurementDialog(self)
        self._taMeasurementDialog.hideEvent(None) # ensure threads are stopped.
        ## initial setup (read settings, update menu entries, set stylesheet)
        self._readIniSettings()
        self._updateFileMenuEntries()
        # setup zoom slider
        self.sliderZoom.setMaximum(len(ZOOM_STEPS) - 1)
        ## setup update status bar timer
        self._timerUpdateStatusbar = QTimer(self)
        self._timerUpdateStatusbar.setInterval(50)
        # noinspection PyUnresolvedReferences
        self._timerUpdateStatusbar.timeout.connect(self._updateStatusBarHandler)
        self._timerUpdateStatusbar.start()
        ## setup filter application timer
        self._timerApplyFilters = QTimer(self)
        self._timerApplyFilters.setInterval(50)
        # noinspection PyUnresolvedReferences
        self._timerApplyFilters.timeout.connect(self._timerApplyFiltersHandler)
        self._timerApplyFilters.start()
        ## parse command line arguments
        parser = argparse.ArgumentParser(prog='pulseaudio-crossover-rack')
        parser.add_argument("--file", nargs="?", help=".paxor file to open")
        parser.add_argument("--apply", help="if a file was loaded, immediately load the crossover modules", action="store_true")
        parser.add_argument("--exit", help="exit after loading modules (only applicable if --apply was given)", action="store_true")
        parser.add_argument("--debug", help="set debug flag, only used for development", action="store_true")
        try:
            args = parser.parse_args()
            if args.debug:
                self.actionResize.setVisible(True)
            if args.file:
                fn = os.path.expanduser(args.file)
                if os.path.isfile(fn):
                    self._openFile(fn)
                if args.apply:
                    self.onModulesInsert()
                    if args.exit:
                        exit(0)
            else:
                self._checkForInsertedFile()
        except argparse.ArgumentError:
            parser.print_help()
            exit(1)
        ## check if modules are inserted
        self._checkModulesInserted()


    def addFrdDialog(self, dlg):
        self._frdDialogs.append(dlg)


    def allLinkableFilters(self):
        if self._xoverFile is None:
            return []
        return self._xoverFile.allLinkableFilters()


    @staticmethod
    def checkSinkName(name):
        """ check a sink name for correctness """
        ret = ""
        if len(name) == 0:
            ret += "- Name is too short!\n"
        else:
            if len(name) > 100:
                ret += "- Name is too long (max 100 characters)\n"
            if not re.match(r"^[A-Za-z0-9._]+$", name):
                ret += "- Invalid characters in name (use A-Z, a-z, 0-9, '.' and '_' only)!\n"
        if ret:
            return ret
        else:
            return None


    def closeEvent(self, evt):
        """ do stuff necessary before closing the main window """
        # close all dialog windows
        self._consoleDialog.close()
        self._globalFrdDialog.close()
        self._frMeasurementDialog.close()
        self._taMeasurementDialog.close()
        for dlg in list(self._frdDialogs):
            dlg.close()
        # need to save file?
        if self._xoverFile is not None:
            reply = self.onFileClose()
            if reply == QMessageBox.Cancel:
                evt.ignore()
                return
        self._writeIniSettings()


    def delFrdDialog(self, dlg):
        """ remove frd dialog from open frd dialog list """
        if dlg in self._frdDialogs:
            self._frdDialogs.remove(dlg)
            dlg.deleteLater()


    def delObject(self, obj):
        self._xoverFile.delObject(obj)


    def drawArea(self):
        return self._drawArea


    def findCyclicGraph(self):
        """
        if there is a cyclic connection in the graph of XoverObjects,
        this function returns True
        """
        if self._xoverFile is None:
            return False
        return self._xoverFile.findCyclicGraph()


    def frdDialogs(self):
        return self._frdDialogs


    def frequencyResponseGraphPngFor(self, w, h):
        w = w * GRAPH_SR[0] / math.pi * 0.5
        if not self._mplFigureSetup:
            self._setupPltFig(w, h)
            self._mplFigureSetup = True
        self._pltLine1.set_ydata(20 * numpy.log10(abs(h)))
        self._pltLine2.set_ydata(keepPhaseBounded(numpy.unwrap(numpy.angle(h)) / math.pi * 180))
        bio = io.BytesIO()
        self._mplCanvas.draw()
        self._mplCanvas.print_png(bio)
        bio.seek(0)
        return bio.read()


    def isDirty(self):
        """ return if XoverFile is dirty """
        if self._xoverFile is None:
            return False
        return self._xoverFile.isDirty()


    def isUniqueName(self, name, exclude=None):
        return self._xoverFile.isUniqueName(name, exclude=exclude)


    def lowHighPassFilters(self):
        return self._xoverFile.lowHighPassFilters()


    def markDirty(self):
        if self._xoverFile is not None:
            self._xoverFile.markDirty()
        self.updateStatusBar()


    def maybeSave(self):
        """ save file if user chooses to do so """
        msg = "Do you want to save the file %s" % self.openedFileName()
        dlg = QMessageBox()
        dlg.setStyleSheet(self.styleSheet())
        reply = dlg.question(self, 'File modified. Save?',
                             msg,
                             QMessageBox.No | QMessageBox.Yes | QMessageBox.Cancel,
                             QMessageBox.Yes)
        if reply == QMessageBox.Yes:
            self.onFileSave()
        return reply


    def nameExists(self, name):
        return self._xoverFile.nameExists(name)


    def onAddFilter(self):
        """ add a filter """
        new = None
        name = self.comboBoxAddFilter.currentText()
        if name == "Add..." or self._xoverFile is None:
            self.comboBoxAddFilter.setCurrentIndex(0)
            return
        if name == "Output":
            new = XoverOutput("", "", "", [])
            dlg = EditOutputDialog(self, new, newlyCreated=True)
            dlg.exec()
        elif name == "LR-2 Lowpass":
            new = XoverLr2Lowpass()
            dlg = EditFilterDialog(self, new, newlyCreated=True)
            dlg.exec()
        elif name == "LR-2 Highpass":
            new = XoverLr2Highpass()
            dlg = EditFilterDialog(self, new, newlyCreated=True)
            dlg.exec()
        elif name == "LR-4 Lowpass":
            new = XoverLr4Lowpass()
            dlg = EditFilterDialog(self, new, newlyCreated=True)
            dlg.exec()
        elif name == "LR-4 Highpass":
            new = XoverLr4Highpass()
            dlg = EditFilterDialog(self, new, newlyCreated=True)
            dlg.exec()
        elif name == "First Order Butterworth Lowpass":
            new = XoverFoLowpass()
            dlg = EditFilterDialog(self, new, newlyCreated=True)
            dlg.exec()
        elif name == "First Order Butterworth Highpass":
            new = XoverFoHighpass()
            dlg = EditFilterDialog(self, new, newlyCreated=True)
            dlg.exec()
        elif name == "Second Order Butterworth Lowpass":
            new = XoverSoLowpass()
            dlg = EditFilterDialog(self, new, newlyCreated=True)
            dlg.exec()
        elif name == "Second Order Butterworth Highpass":
            new = XoverSoHighpass()
            dlg = EditFilterDialog(self, new, newlyCreated=True)
            dlg.exec()
        elif name == "Second Order Lowpass with Q":
            new = XoverSoLowpassWithQ()
            dlg = EditFilterDialog(self, new, newlyCreated=True)
            dlg.exec()
        elif name == "Second Order Highpass with Q":
            new = XoverSoHighpassWithQ()
            dlg = EditFilterDialog(self, new, newlyCreated=True)
            dlg.exec()
        elif name == "Third Order Butterworth Lowpass":
            new = XoverToLowpass()
            dlg = EditFilterDialog(self, new, newlyCreated=True)
            dlg.exec()
        elif name == "Third Order Butterworth Highpass":
            new = XoverToHighpass()
            dlg = EditFilterDialog(self, new, newlyCreated=True)
            dlg.exec()
        elif name == "Parametric Equalizer":
            new = XoverParamEq()
            dlg = EditFilterDialog(self, new, newlyCreated=True)
            dlg.exec()
        elif name == "Sample Accurate Delay":
            new = XoverSampleDelay()
            dlg = EditFilterDialog(self, new, newlyCreated=True)
            dlg.exec()
        elif name == "Subsample Accurate Delay":
            new = XoverSubsampleDelay()
            dlg = EditFilterDialog(self, new, newlyCreated=True)
            dlg.exec()
        elif name == "Inverter":
            new = XoverInverter()
            dlg = EditFilterDialog(self, new, newlyCreated=True)
            dlg.exec()
        elif name == "Linkwitz Transform":
            new = XoverLinkwitzTransform()
            dlg = EditFilterDialog(self, new, newlyCreated=True)
            dlg.exec()
        self.comboBoxAddFilter.setCurrentIndex(0)
        if new is None or new.widget() is None:
            return
        self.updateStatusBar()


    def onExit(self):
        """ exit! """
        self.close()


    def onFileClose(self):
        """ close opened file """
        if self.isDirty():
            reply = self.maybeSave()
            if reply == QMessageBox.Cancel:
                return reply
        self._xoverFile.close()
        self._xoverFile = None
        self._drawArea.clear()
        self._updateFileMenuEntries()
        self.updateStatusBar()


    def onFileNew(self):
        """ open a newly created/empty file """
        if self.isDirty():
            reply = self.maybeSave()
            if reply == QMessageBox.Cancel:
                return
            self._xoverFile.close()
        self._modulesInserted = False
        self._xoverFile = XoverFile("")
        self._xoverFile.restoreWidgets(self._drawArea)
        self._updateFileMenuEntries()
        self.updateStatusBar()


    def onFileOpen(self):
        """ open a file """
        if self.isDirty():
            reply = self.maybeSave()
            if reply == QMessageBox.Cancel:
                return
        dlg = QFileDialog()
        dlg.setStyleSheet(self.styleSheet())
        fn, ignore = dlg.getOpenFileName(self, 'Open an existing PAXOR file...', self._dlgPath, '*.paxor')
        if fn == "":
            return
        if self._xoverFile is not None:
            self._xoverFile.close()
        self._openFile(fn)


    def onFileSave(self):
        """ save opened file """
        assert(self._xoverFile is not None)
        if self.openedFileName() == "":
            wasDirty = True
            self.onFileSaveAs()
        else:
            wasDirty = self.isDirty()
            self._xoverFile.save()
        if wasDirty and self._checkModulesInserted():
            configSnippet = self._xoverFile.xoverInput().generatePulseaudioConfig()
            self._removeDefaultPaSnippet()
            self._insertDefaultPaSnippet(configSnippet)


    def onFileSaveAs(self):
        """ save opened file """
        assert(self._xoverFile is not None)
        dlg = QFileDialog()
        dlg.setStyleSheet(self.styleSheet())
        dlg.setAcceptMode(QFileDialog.AcceptSave)
        dlg.setDefaultSuffix("paxor")
        dlg.setWindowTitle('Save Crossover File as...')
        dlg.setDirectory(self._dlgPath)
        ret = dlg.exec()
        if not ret:
            return
        fn = dlg.selectedFiles()[0]
        self._dlgPath = os.path.dirname(fn)
        wasDirty = self.isDirty()
        self._xoverFile.saveAs(fn)
        if wasDirty and self._checkModulesInserted():
            configSnippet = self._xoverFile.xoverInput().generatePulseaudioConfig()
            self._removeDefaultPaSnippet()
            self._insertDefaultPaSnippet(configSnippet)
        self.updateStatusBar()


    def onModulesInsert(self):
        """ insert crossover modules """
        if self._xoverFile is None:
            dlg = QMessageBox()
            dlg.setStyleSheet(self.styleSheet())
            dlg.warning(self, "No file loaded.", "You need to load or create a file first!")
            return
        if self._xoverFile.hasUnconnectedObjects():
            dlg = QMessageBox()
            dlg.setStyleSheet(self.styleSheet())
            dlg.warning(self,
                        "Unconnected Objects!",
                        "You have unconnected objects in your graph, all of them have to be connected before module insertion is possible!")
            return
        if self.isDirty():
            dlg = QMessageBox()
            dlg.setStyleSheet(self.styleSheet())
            ret = dlg.question(self, "Save before insert!",
                               "The filter modules can only be inserted once the file was saved.\n"
                               "Do you want to save now?",
                               QMessageBox.Yes | QMessageBox.No)
            if ret == QMessageBox.No:
                return
            self.onFileSave()
        oldDefaultSink = pulse_get_default_sink()
        oldVolume = pulse_get_sink_volume(oldDefaultSink)
        self._consoleDialog.showAutoClose()
        self._consoleDialog.line()
        # set old default sink to zero volume
        pulse_set_sink_volume(oldDefaultSink, 0)
        sleep(0.1)
        # set all output sink volumes to 0
        for output in self._xoverFile.allOutputs():
            pulse_set_sink_volume(output.outputSinkName(), 0)
        self._unloadModules()
        self._removeDefaultPaSnippet()
        self._consoleDialog.hint("\nINSERTING NEW PAXOVERRACK MODULES...")
        configSnippet = self._xoverFile.xoverInput().generatePulseaudioConfig()
        error = False
        for cmd in configSnippetToCommandList(configSnippet):
            result = re.search("sink_name=PaXoverRack.([A-Za-z0-9._]+)", cmd)
            if result is None:
                continue
            sinkName = result.group(1)
            ret = os.system(cmd)
            if ret == 0:
                self._consoleDialog.success("inserted module for '%s'" % sinkName)
            else:
                self._consoleDialog.error("ERROR inserting module for '%s'" % sinkName)
                error = True
                break
        if error:
            self._unloadModules()
            self._moveSinkInputsTo(oldDefaultSink)
            sleep(0.1)
            pulse_set_sink_volume(oldDefaultSink, oldVolume)
            os.system("pacmd set-default-sink %s" % oldDefaultSink)
            return
        # set default sink
        pulse_set_sink_volume("PaXoverRack.Input", 0)
        cmd = "pacmd set-default-sink PaXoverRack.Input"
        ret = os.system(cmd)
        if ret == 0:
            self._consoleDialog.success("set default_sink to PaXoverRack.Input")
        else:
            self._consoleDialog.error("error setting default_sink to PaXoverRack.Input")
        # move streams to new default sink
        self._moveSinkInputsTo("PaXoverRack.Input")
        # restore old volume on new default sink
        if oldVolume is not None:
            sleep(0.1)
            pulse_set_sink_volume("PaXoverRack.Input", oldVolume)
        # set volume of all filter sinks to 100%
        for filter_sink_name in self._xoverFile.filterSinkNames():
            pulse_set_sink_volume("PaXoverRack.%s" % filter_sink_name, 1.0)
        # set volume of output sinks to 100%
        for output_sink_name in self._xoverFile.outputSinkNames():
            pulse_set_sink_volume(output_sink_name, 1.0)
        self._insertDefaultPaSnippet(configSnippet)
        self._modulesInserted = True
        self._xoverFile.resetMmaps()
        self._updateFileMenuEntries()
        self.updateStatusBar()


    def onModulesRemove(self):
        """ remove crossover modules """
        # choose new default sink
        hw_sinks = pulse_get_hw_sinks()
        hw_sink_descriptions = map(lambda x: x["description"], hw_sinks)
        idx = 0
        for i, hw_sink_dict in enumerate(hw_sinks):
            if hw_sink_dict['name'] == self._last_pulseaudio_default_sink:
                idx = i
                break
        dlg = QInputDialog()
        dlg.setStyleSheet(self.styleSheet())
        hw_sink_desc, ok = dlg.getItem(self,
                                       "Choose pulseaudio default sink...",
                                       "Master sinks:",
                                       hw_sink_descriptions,
                                       idx,
                                       False)
        if not ok:
            return
        hw_sink = hw_sinks[0]['name']
        for hw_sink_dict in hw_sinks:
            if hw_sink_dict['description'] == hw_sink_desc:
                hw_sink = hw_sink_dict['name']
                break
        self._last_pulseaudio_default_sink = hw_sink
        self._consoleDialog.showAutoClose()
        self._consoleDialog.line()
        # try to set new sink volume to volume of PaXoverRack.Input
        oldVolume = pulse_get_sink_volume("PaXoverRack.Input")
        if oldVolume is not None:
            pulse_set_sink_volume(hw_sink, oldVolume)
        # move streams to new default sink
        self._moveSinkInputsTo(hw_sink)
        self._unloadModules()
        os.system("pacmd set-default-sink %s" % hw_sink)
        self._modulesInserted = False
        if self._xoverFile is not None:
            self._xoverFile.resetMmaps()
        self._updateFileMenuEntries()
        self._removeDefaultPaSnippet()


    @staticmethod
    def onOnlineHelp():
        url = QUrl('https://t-5.eu/hp/Software/Pulseaudio%20Crossover%20Rack/OnlineHelp/')
        QDesktopServices().openUrl(url)


    @staticmethod
    def onResetDefaultPa():
        """ reset (remove) ~/.config/pulse/default.pa """
        os.unlink(os.path.expanduser("~/.config/pulse/default.pa"))


    def onResizeWindow(self):
        self.resize(1024, 768)


    @staticmethod
    def onRestartPulseaudio():
        """ forcibly restart the pulseaudio server """
        os.system("pulseaudio -k; sleep 1; killall -KILL pulseaudio; pulseaudio -D")


    @staticmethod
    def onShowAboutDialog():
        """ show about dialog """
        dlg = AboutDialog()
        dlg.exec_()


    def onShowConsole(self):
        self._consoleDialog.showNoAutoClose()


    def onShowFrdDialogFor(self):
        self.sender().xoverFilter.widget().onFrdDialog()


    def onShowGlobalFrdDialog(self):
        self._globalFrdDialog.show()


    def onShowMeasurementWindow(self):
        self._frMeasurementDialog.show()


    def onShowTaMeasurementWindow(self):
        self._taMeasurementDialog.show()


    def onZoomSliderChanged(self):
        """ update zoom factor """
        self._zoomFactor = ZOOM_STEPS[self.sliderZoom.value()]
        self.labelZoom.setText(("Zoom: %03d%%" % (self._zoomFactor * 100.0)).replace(" 0", "   "))
        self._drawArea.repaint()
        for widget in self._drawArea.widgets():
            widget.repaint()


    def openedFileName(self):
        if self._xoverFile is None:
            return ""
        return self._xoverFile.fileName()


    def settings(self):
        return self._settings


    def updateStatusBar(self):
        self._timerUpdateStatusbar.stop()
        self._rereadHwStatus = True
        self._timerUpdateStatusbar.setInterval(10)
        self._timerUpdateStatusbar.start()


    def xoverFileAddObject(self, obj):
        self._xoverFile.addObject(obj)


    def zoomFactor(self):
        return self._zoomFactor


    def _checkForInsertedFile(self):
        """ check ~/.config/default.pa for inserted file and load it if found """
        try:
            f = open(os.path.expanduser("~/.config/pulse/default.pa"))
            content = f.read()
            f.close()
        except (PermissionError, FileNotFoundError):
            content = ""
        if DEFAULT_PA_LINE_FIRST in content:
            match = re.search("### Loaded file: (.*)\n", content)
            if match:
                filename = match.group(1)
                if os.path.isfile(filename):
                    self._openFile(filename)


    def _checkModulesInserted(self):
        """ check if modules for currently loaded file are inserted into default.pa """
        self._modulesInserted = False
        if self._xoverFile is None:
            return False
        # check for file uuid
        line2 = DEFAULT_PA_LINE2_FMT % self._xoverFile.uuid()
        toSearch = "%s\n%s\n" % (DEFAULT_PA_LINE_FIRST, line2)
        try:
            f = open(os.path.expanduser("~/.config/pulse/default.pa"))
            content = f.read()
            f.close()
        except (PermissionError, FileNotFoundError):
            content = ""
        if not toSearch in content:
            return False
        # check if output modules are actually inserted
        insertedModules = cmdRunAndRead("pacmd list modules")
        for output in self._xoverFile.xoverOutputs():
            if not ("sink_name=%s" % output.outputSinkName()) in insertedModules:
                return False
            if not ("sink_name=PaXoverRack.%s" % output.sinkName()) in insertedModules:
                return False
        self._modulesInserted = True
        self._xoverFile.resetMmaps()
        self._updateFileMenuEntries()
        self.updateStatusBar()
        return True


    def _connectSignals(self):
        ## menu actions
        self.actionAbout.triggered.connect(self.onShowAboutDialog)
        self.actionClose.triggered.connect(self.onFileClose)
        self.actionExit.triggered.connect(self.onExit)
        self.actionInsertModules.triggered.connect(self.onModulesInsert)
        self.actionNew.triggered.connect(self.onFileNew)
        self.actionOnlineHelp.triggered.connect(self.onOnlineHelp)
        self.actionOpen.triggered.connect(self.onFileOpen)
        self.actionReloadPulseaudioStatus.triggered.connect(self.updateStatusBar)
        self.actionRemoveModules.triggered.connect(self.onModulesRemove)
        self.actionResetDefaultPa.triggered.connect(self.onResetDefaultPa)
        self.actionResize.triggered.connect(self.onResizeWindow)
        self.actionRestartPulseaudio.triggered.connect(self.onRestartPulseaudio)
        self.actionSave.triggered.connect(self.onFileSave)
        self.actionSaveAs.triggered.connect(self.onFileSaveAs)
        self.actionShowConsole.triggered.connect(self.onShowConsole)
        self.actionShowGlobalFrdDialog.triggered.connect(self.onShowGlobalFrdDialog)
        self.actionShowMeasurementWindow.triggered.connect(self.onShowMeasurementWindow)
        self.actionTimeAlignmentMeasuerements.triggered.connect(self.onShowTaMeasurementWindow)
        self.comboBoxAddFilter.currentIndexChanged.connect(self.onAddFilter)
        oldShowEvent = self.menuFilters.showEvent
        def aboutToShowFiltersMenu(self_):
            self._reconstructShowFrdDialogForMenu()
            oldShowEvent(self_)
        self.menuFilters.showEvent = aboutToShowFiltersMenu
        ## toolbar widgets
        self.toolButtonAbout.clicked.connect(self.onShowAboutDialog)
        self.toolButtonClose.clicked.connect(self.onFileClose)
        self.toolButtonExit.clicked.connect(self.onExit)
        self.toolButtonInsert.clicked.connect(self.onModulesInsert)
        self.toolButtonNew.clicked.connect(self.onFileNew)
        self.toolButtonOpen.clicked.connect(self.onFileOpen)
        self.toolButtonRemove.clicked.connect(self.onModulesRemove)
        self.toolButtonSave.clicked.connect(self.onFileSave)
        self.toolButtonSaveAs.clicked.connect(self.onFileSaveAs)
        self.sliderZoom.valueChanged.connect(self.onZoomSliderChanged)


    def _importCheck(self, modulename):
        try:
            __import__(modulename)
            if modulename == "scipy":
                from scipy import signal
        except ImportError:
            dlg = QMessageBox()
            dlg.setStyleSheet(self.styleSheet())
            msg = "WARNING:\n"
            if modulename == "scipy":
                msg += "  Incompatible scipy/numpy version detected.\n"
                msg += "  Should we try to install the correct versions for you?\n"
            else:
                msg += "  Could not find python module '%s'.\n" % modulename
                msg += "  Should we try to install it for you?\n"
            msg += "  (This may take a few seconds or minutes...)"
            result = dlg.warning(self, "Dependencies warning", msg, QMessageBox.Yes | QMessageBox.No)
            if result == QMessageBox.Yes:
                break_system_packages = sys.version_info[1] >= 11 and "--break-system-packages" or ""
                ret = os.system("pip3 install %s %s" % (break_system_packages, modulename))
                if ret != 0:
                    msg = "ERROR: module '%s' not found.\n" % modulename
                    msg += "  Manual installation: 'pip3 install %s'" % modulename
                    dlg.critical(self, "ERROR loading dependencies!", msg)
                    print(msg, file=sys.stderr)
                    sys.exit(1)
                else:
                    msg = "SUCCESS:\n"
                    msg += "  The module '%s' was successfully installed.\n" % modulename
                    msg += "  The program will be restarted now..."
                    dlg.information(self, "Need to restart...", msg, QMessageBox.Ok)
                    fd, fn = mkstemp()
                    os.close(fd)
                    os.chmod(fn, 0o700)
                    f = open(fn, "w")
                    f.write("#!/bin/sh\nsleep 2\n/usr/bin/pulseaudio-crossover-rack\nrm -f %s\n" % fn)
                    f.close()
                    os.system("%s &" % fn)
                    sys.exit(0)
            else:
                sys.exit(1)


    def _insertDefaultPaSnippet(self, configSnippet):
        # read ~/.config/pulse/default.pa
        try:
            f = open(os.path.expanduser("~/.config/pulse/default.pa"))
            content = f.read()
            f.close()
        except FileNotFoundError:
            # fallback to reading /etc/pulse/default.pa in case of FileNotFoundError
            try:
                f = open(os.path.expanduser("/etc/pulse/default.pa"))
                content = f.read()
                f.close()
            except (FileNotFoundError, PermissionError):
                self._consoleDialog.error("ERROR: Could not read /etc/pulse/default.pa")
                return
        # try to write a backup file to ~/.config/pulse/default.pa.backup_by_paxoverrack
        try:
            f = open(os.path.expanduser("~/.config/pulse/default.pa.backup_by_paxoverrack"), "w")
            f.write(content)
            f.close()
        except (PermissionError, FileNotFoundError):
            self._consoleDialog.error("ERROR: Could not write ~/.config/pulse/default.pa.backup_by_paxoverrack")
            return
        self._consoleDialog.success("wrote backup of ~/.config/pulse/default.pa")
        # append config snippet to current content of default.pa
        if content and content[-1] != "\n":
            content += "\n"
        if content and content[-2:] != "\n\n":
            content += "\n"
        content += DEFAULT_PA_LINE_FIRST
        content += "\n"
        content += DEFAULT_PA_LINE2_FMT % self._xoverFile.uuid()
        content += "\n"
        content += DEFAULT_PA_LINE3_FMT % self._xoverFile.fileName()
        content += "\n"
        content += ".nofail\n"
        content += configSnippet
        content += ".fail\n"
        content += DEFAULT_PA_LINE_LAST
        content += "\n"
        # try to write a new config to ~/.config/pulse/default.pa
        try:
            f = open(os.path.expanduser("~/.config/pulse/default.pa"), "w")
            f.write(content)
            f.close()
        except (PermissionError, FileNotFoundError):
            self._consoleDialog.error("ERROR: Could not write ~/.config/pulse/default.pa")
            return
        self._consoleDialog.success("wrote ~/.config/pulse/default.pa")


    def _moveSinkInputsTo(self, sink_name):
        cmd = "pacmd list-sink-inputs"
        ret = cmdRunAndRead(cmd)
        sinkInputsToBeMoved = []
        idx = -1
        for line in ret.split("\n"):
            m = re.search("index: ([0-9]+)", line)
            if m:
                idx = int(m.group(1))
            if "driver: <protocol-native.c>" in line and idx > -1:
                sinkInputsToBeMoved.append(idx)
        for idx in sinkInputsToBeMoved:
            ret = os.system("pacmd move-sink-input %s %s" % (idx, sink_name))
            if ret == 0:
                self._consoleDialog.success("moved stream %s to %s" % (idx, sink_name))
            else:
                self._consoleDialog.error("error moving sream %s to %s" % (idx, sink_name))


    def _openFile(self, fn):
        self._modulesInserted = False
        try:
            for dlg in list(self._frdDialogs):
                dlg.close()
                dlg.deleteLater()
            self._globalFrdDialog.close()
            self._globalFrdDialog.reload()
            self._frdDialogs = []
            self._xoverFile = XoverFile(fn)
            if self._xoverFile.savedVersion() > CURRENT_DATAMODEL_VERSION:
                msg = "An error occurred while trying to load file '%s'\n" % fn
                msg += "Version mismatch, file version is %s, program version is %s.\n" % (
                    self._xoverFile.savedVersion(),
                    CURRENT_DATAMODEL_VERSION)
                msg += "Please update to the latest program version!"
                dlg = QMessageBox()
                dlg.setStyleSheet(self.styleSheet())
                dlg.critical(self, 'ERROR: could not load file', msg)
                self._xoverFile = None
                self._drawArea.clear()
            else:
                self._xoverFile.restoreWidgets(self._drawArea)
                self._dlgPath = os.path.dirname(fn)
                self._checkModulesInserted()
        except (FileStorageError, TypeError):
            msg = "An error occurred while trying to load file %s" % fn
            dlg = QMessageBox()
            dlg.setStyleSheet(self.styleSheet())
            dlg.critical(self, 'ERROR: could not load file', msg)
            print_exc(sys.stderr)
            self._xoverFile = None
        self._updateFileMenuEntries()
        self.updateStatusBar()


    def _readIniSettings(self):
        """ read ini settings """
        s = self._settings
        self._last_pulseaudio_default_sink = s.value("main/lastPulseaudioDefaultSink", "")
        # zoom factor
        try:
            zf = float(s.value("main/zoomFactor", "1"))
        except ValueError:
            zf = 1.0
        if zf in ZOOM_STEPS:
            self._zoomFactor = zf
            self.sliderZoom.setValue(ZOOM_STEPS.index(zf))
        else:
            self._zoomFactor = 1.0
        # main window geometry
        geo = s.value("main/mainWindowGeo")
        if geo is not None:
            self.restoreGeometry(geo)
        state = s.value("main/mainWindowState")
        if state is not None:
            self.restoreState(state)
        # console dialog geometry and settings
        try:
            x, y, w, h = (int(s.value("consoleDialog/x", None)),
                          int(s.value("consoleDialog/y", None)),
                          int(s.value("consoleDialog/width", None)),
                          int(s.value("consoleDialog/height", None)))
        except (TypeError, ValueError):
            x, y, w, h = None, None, None, None
        if None not in (x, y, w, h):
            self._consoleDialog.setGeometry(x, y, w, h)
        try:
            autoClose = bool(int(s.value("consoleDialog/autoClose", None)))
        except (TypeError, ValueError):
            autoClose = False
        self._consoleDialog.setAutoClose(autoClose)
        try:
            autoCloseSeconds = int(s.value("consoleDialog/autoCloseSeconds", None))
        except (TypeError, ValueError):
            autoCloseSeconds = 10
        self._consoleDialog.setAutoCloseSeconds(autoCloseSeconds)
        # open dialog path
        self._dlgPath = s.value("main/dlgPath", os.path.expanduser("~"))
        if not os.path.isdir(self._dlgPath):
            self._dlgPath = os.path.expanduser("~")


    def _reconstructShowFrdDialogForMenu(self):
        """ clear and rebuild the submenu for the 'Show FRD Dialog for...' action """
        self.menuShowFrdDialogFor.clear()
        if self._xoverFile is None:
            return
        for flt in self._xoverFile.allFilters(sort=True):
            action = QAction(flt.sinkName(), self.menuShowFrdDialogFor)
            action.xoverFilter = flt
            # noinspection PyUnresolvedReferences
            action.triggered.connect(self.onShowFrdDialogFor)
            self.menuShowFrdDialogFor.addAction(action)


    def _removeDefaultPaSnippet(self):
        """ remove configuration snippet from ~/.config/pulse/default.pa """
        self._consoleDialog.hint("\nREMOVING OLD PAXOVERRACK CONFIG...")
        # read ~/.config/pulse/default.pa
        try:
            f = open(os.path.expanduser("~/.config/pulse/default.pa"))
            content = f.read()
            f.close()
        except FileNotFoundError:
            # fallback to reading /etc/pulse/default.pa in case of FileNotFoundError
            try:
                f = open(os.path.expanduser("/etc/pulse/default.pa"))
                content = f.read()
                f.close()
            except (FileNotFoundError, PermissionError):
                self._consoleDialog.error("ERROR: Could not read /etc/pulse/default.pa")
                return
        # try to write a backup file to ~/.config/pulse/default.pa.backup_by_paxoverrack
        try:
            f = open(os.path.expanduser("~/.config/pulse/default.pa.backup_by_paxoverrack"), "w")
            f.write(content)
            f.close()
        except (PermissionError, FileNotFoundError):
            self._consoleDialog.error("ERROR: Could not write ~/.config/pulse/default.pa.backup_by_paxoverrack")
            return
        self._consoleDialog.success("wrote backup of ~/.config/pulse/default.pa")
        # try to write a new config to ~/.config/pulse/default.pa
        pattern = "%s\n.*%s\n" % (DEFAULT_PA_LINE_FIRST, DEFAULT_PA_LINE_LAST)
        content = re.sub(pattern, "", content, flags=re.DOTALL)
        try:
            f = open(os.path.expanduser("~/.config/pulse/default.pa"), "w")
            f.write(content)
            f.close()
        except (PermissionError, FileNotFoundError):
            self._consoleDialog.error("ERROR: Could not write ~/.config/pulse/default.pa")
            return
        self._consoleDialog.success("wrote ~/.config/pulse/default.pa")


    def _setupAddFilterComboBox(self):
        self.comboBoxAddFilter.addItem("Add...")
        for name in FILTER_NAMES:
            self.comboBoxAddFilter.addItem(name)


    def _setupPltFig(self, w, h):
        self._pltLine1, self._pltAx1, self._pltLine2, self._pltAx2 = \
            setupPltFigure(w, h, self._mplCanvas, self._mplFigure, -42, 2, -180, 180)
        self._mplFigure.subplots_adjust(left=0.13, right=0.86, top=0.97, bottom=0.13)


    # noinspection PyUnresolvedReferences
    def _setupStatusBar(self):
        self._statusbarFileName = QLabel()
        self._statusbarFileName.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self._statusbarInserted = QLabel()
        self._statusbarInserted.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self._statusbarBackendStatus = QLabel()
        self._statusbarBackendStatus.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self._statusbarOutputStatus = QLabel()
        self._statusbarOutputStatus.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        # sizes and font sizes
        font = QFont("sans-serif", 10)
        self._statusbarFileName.setFont(font)
        self._statusbarInserted.setFont(font)
        self._statusbarInserted.setFixedSize(100, 15)
        self._statusbarBackendStatus.setFont(font)
        self._statusbarBackendStatus.setFixedSize(170, 15)
        self._statusbarOutputStatus.setFont(font)
        self._statusbarOutputStatus.setFixedSize(160, 15)
        # add widgets to status bar
        sb = self.statusBar
        sb.addWidget(self._statusbarFileName)
        sb.addWidget(self._statusbarInserted)
        sb.addWidget(self._statusbarBackendStatus)
        sb.addWidget(self._statusbarOutputStatus)


    def _timerApplyFiltersHandler(self):
        if self._xoverFile is None or not self._modulesInserted:
            return
        self._xoverFile.applyFilterParams()


    def _unloadModules(self):
        """ unload all ladspa_sink modules with 'PaXoverRack.' prefix """
        self._consoleDialog.hint("\nREMOVING PAXOVERRACK MODULES...")
        ret = cmdRunAndRead("LC_ALL=c pacmd list modules")
        foundModuleNumber = 0
        modulesToRemove = []
        for line in ret.split("\n"):
            if re.search(" {4}index: [0-9]+", line):
                tmp_foundModuleNumber = int(line.replace("    index: ", ""))
                if tmp_foundModuleNumber < foundModuleNumber:
                    break
                foundModuleNumber = tmp_foundModuleNumber
            elif "sink_name=PaXoverRack." in line:
                m = re.search(r"sink_name=PaXoverRack\.([A-Za-z0-9._]+)", line)
                if not ".monitor" in line:
                    modulesToRemove.append( (foundModuleNumber, m.group(1)) )
        for idx, moduleName in reversed(modulesToRemove):
            retcode = os.system("pacmd unload-module %s" % idx)
            if retcode == 0:
                self._consoleDialog.success("Unloaded module '%s'." % moduleName)
            else:
                self._consoleDialog.error("Error unloading module '%s'." % moduleName)
        if len(modulesToRemove) == 0:
            self._consoleDialog.warning("No modules found for unloading.")
        self.updateStatusBar()


    def _updateFileMenuEntries(self):
        """ update save/saveas/close file menu entries """
        disabled = self._xoverFile is None
        self.actionClose.setDisabled(disabled)
        self.actionSave.setDisabled(disabled)
        self.actionSaveAs.setDisabled(disabled)
        self.toolButtonClose.setDisabled(disabled)
        self.toolButtonSave.setDisabled(disabled)
        self.toolButtonSaveAs.setDisabled(disabled)
        self.toolButtonInsert.setDisabled(disabled or self._modulesInserted)
        self.toolButtonRemove.setDisabled(disabled or not self._modulesInserted)


    def _updateStatusBarHandler(self):
        self._timerUpdateStatusbar.setInterval(1000)
        if self._xoverFile is None:
            fn = "N/A"
        else:
            fn = self.openedFileName()
            if self.isDirty():
                fn = fn + " [unsaved]"
            if fn == "":
                fn = "[unsaved]"
        self._statusbarFileName.setText(" File: %s" % fn)
        self._statusbarInserted.setText("| Inserted: %s" % (self._modulesInserted and "Yes" or "No"))
        firstOutputSink = "YouWillNeverFindMe"
        if self._xoverFile is not None:
            outputSinkNames = list(self._xoverFile.outputSinkNames())
            if len(outputSinkNames) > 0:
                firstOutputSink = outputSinkNames[0]
        status = pulse_get_status(firstOutputSink, self._rereadHwStatus)
        if self._rereadHwStatus:
            self._rereadHwStatus = False
            self._timerUpdateStatusbar.setInterval(1000)
        self._statusbarBackendStatus.setText("| " + status[0])
        self._statusbarOutputStatus.setText("| " + status[1])
        # resize file label
        self._statusbarFileName.setFixedWidth(self.width() - 450)


    def _writeIniSettings(self):
        """ write ini settings """
        s = self._settings
        s.setValue("main/dlgPath", self._dlgPath)
        s.setValue("main/mainWindowGeo", self.saveGeometry())
        s.setValue("main/mainWindowState", self.saveState())
        s.setValue("main/zoomFactor", self._zoomFactor)
        s.setValue("main/lastPulseaudioDefaultSink", self._last_pulseaudio_default_sink)
