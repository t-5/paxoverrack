import numpy
from PyQt5.QtCore import QUrl, Qt
from PyQt5.QtGui import QDesktopServices
from PyQt5.QtWidgets import QDialog, QLabel
from designer_qt5.Ui_MeasurementDialog import Ui_MeasurementDialog
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from qt5_t5darkstyle import darkstyle_css

from helpers.constants import GRAPH_FREQUENCIES, MPLFIGURE_BOTTOMMARGIN, MPLFIGURE_LEFTRIGHTMARGIN, MPLFIGURE_TOPMARGIN
from helpers.functions import GRAPH_SR


class MeasurementDialog(QDialog, Ui_MeasurementDialog):

    LINE_COLORS = {
        0: '#00ff00',
        1: '#00ffff',
        2: '#ff6666',
        3: '#ffff00',
        4: '#ff00ff',
        5: '#6666ff',
    }


    def __init__(self, mainWindow):
        super(MeasurementDialog, self).__init__()
        self.setupUi(self)
        self.setWindowFlags(Qt.WindowCloseButtonHint | Qt.WindowMinimizeButtonHint | Qt.WindowMaximizeButtonHint)
        self.setStyleSheet(darkstyle_css())
        self._mainWindow = mainWindow
        # setup tabs with diagram canvases
        self._nodata = numpy.full(len(GRAPH_FREQUENCIES), -1000000)
        self._setupMplFrCanvas()
        self._setupMplWaterfallCanvas()
        # connect actions
        self.toolButtonExit.clicked.connect(self.onClose)
        self.toolButtonHelp.clicked.connect(self.onHelp)
        # settings
        self._settings = mainWindow.settings()
        self._readIniSettings()


    def closeEvent(self, evt):
        self._writeIniSettings()
        super().closeEvent(evt)


    def onClose(self):
        self.close()


    @staticmethod
    def onHelp():
        url = QUrl('https://t-5.eu/hp/Software/Pulseaudio%20Crossover%20Rack/OnlineHelp/#frd_measurements')
        QDesktopServices().openUrl(url)


    def resizeEvent(self, evt):
        w = self._mplFrCanvas.width()
        h = self._mplFrCanvas.height()
        l = MPLFIGURE_LEFTRIGHTMARGIN / w
        r = 1 - l
        t = 1 - (MPLFIGURE_TOPMARGIN / h)
        b = MPLFIGURE_BOTTOMMARGIN / h
        self._mplFrFigure.subplots_adjust(left=l, right=r, top=t, bottom=b)
        if evt is not None:
            super().resizeEvent(evt)


    def show(self):
        super().show()
        self.resizeEvent(None)


    def _readIniSettings(self):
        s = self._settings
        # dialog geometry and settings
        try:
            x, y, w, h = (int(s.value("measurementDialog/x", None)),
                          int(s.value("measurementDialog/y", None)),
                          int(s.value("measurementDialog/width", None)),
                          int(s.value("measurementDialog/height", None)))
        except (TypeError, ValueError):
            x, y, w, h = None, None, None, None
        if None not in (x, y, w, h):
            self.setGeometry(x, y, w, h)
        try:
            size1 = int(s.value("measurementDialog/splitterSize1", None))
            size2 = int(s.value("measurementDialog/splitterSize2", None))
            self.splitter.setSizes([size1, size2])
        except (TypeError, ValueError):
            pass


    def _setupMplFrCanvas(self):
        self._nodata = numpy.full(len(GRAPH_FREQUENCIES), -1000000)
        self._mplFrMagLines = []
        self._mplFrPhaseLines = []
        self._mplFrFigure = Figure()
        self._mplFrCanvas = FigureCanvas(self._mplFrFigure)
        self._mplFrFigure.patch.set_facecolor("#333333")
        self._mplFrAx1 = self._mplFrFigure.add_subplot(111)
        self._mplFrAx1.set_zorder(10)
        self._mplFrAx1.patch.set_visible(False)
        self._mplFrAx1.grid(which='major', axis='both', color="#dddddd", linestyle='-')
        self._mplFrAx1.grid(which='minor', axis='x', color="#bbbbbb", linestyle='--')
        self._mplFrAx1.spines['bottom'].set_color('#dddddd')
        self._mplFrAx1.spines['top'].set_color('#dddddd')
        self._mplFrAx1.spines['right'].set_color('#dddddd')
        self._mplFrAx1.spines['left'].set_color('#dddddd')
        self._mplFrAx1.tick_params(axis='x', colors='#dddddd')
        self._mplFrAx1.tick_params(axis='y', colors='#dddddd')
        line, = self._mplFrAx1.plot(GRAPH_FREQUENCIES, self._nodata, self.LINE_COLORS[0], linewidth=2.0, zorder=100)
        self._mplFrMagLines.append(line)
        # noinspection PyTypeChecker
        self._mplFrAx1.axis([10, GRAPH_SR[0] / 2, -50, 10])
        self._mplFrAx1.set_ylabel('Amplitude [dB]', color='#ffffff')
        self._mplFrAx1.set_xscale('log')
        self._mplFrAx2 = self._mplFrAx1.twinx()
        self._mplFrAx2.patch.set_facecolor("#111111")
        self._mplFrAx2.patch.set_visible(True)
        self._mplFrAx2.spines['bottom'].set_color('#dddddd')
        self._mplFrAx2.spines['top'].set_color('#dddddd')
        self._mplFrAx2.spines['right'].set_color('#dddddd')
        self._mplFrAx2.spines['left'].set_color('#dddddd')
        self._mplFrAx2.tick_params(axis='x', colors='#dddddd')
        self._mplFrAx2.tick_params(axis='y', colors='#dddddd')
        line, = self._mplFrAx2.plot(GRAPH_FREQUENCIES, self._nodata, color=self.LINE_COLORS[0], linewidth=1.0, zorder=50, linestyle='--')
        self._mplFrPhaseLines.append(line)
        self._mplFrAx2.set_ylabel('Phase [degrees]', color='#ffffff')
        self._mplFrAx2.grid(False)
        self._mplFrAx2.set_ylim(-200, 200)
        self._mplFrAx2.set_xlim(10, GRAPH_SR[0] / 2)
        self._mplFrFigure.tight_layout(pad=0)
        self._mplFrCanvas.draw()
        for i in range(1, 5):
            line, = self._mplFrAx1.plot(GRAPH_FREQUENCIES, self._nodata, self.LINE_COLORS[i], linewidth=2.0, zorder=100 - i)
            self._mplFrMagLines.append(line)
            line, = self._mplFrAx2.plot(GRAPH_FREQUENCIES, self._nodata, self.LINE_COLORS[i], linewidth=1.0, zorder=50 - i, linestyle='--')
            self._mplFrPhaseLines.append(line)
        self._mplFrCanvas.setMinimumHeight(100)
        self.tabWidget.addTab(self._mplFrCanvas, "Frequency response")


    def _setupMplWaterfallCanvas(self):
        w = QLabel("TBD")
        w.setAlignment(Qt.AlignCenter)
        self.tabWidget.addTab(w, "Waterfall diagramm")


    def _updateGraph(self):
        if self._graphUpdateInProgress:
            return


    def _writeIniSettings(self):
        s = self._settings
        # dialog geometry
        s.setValue("measurementDialog/x", self.x())
        s.setValue("measurementDialog/y", self.y())
        s.setValue("measurementDialog/height", self.height())
        s.setValue("measurementDialog/width", self.width())
        s.setValue("measurementDialog/splitterSize1", self.splitter.sizes()[0])
        s.setValue("measurementDialog/splitterSize2", self.splitter.sizes()[1])
