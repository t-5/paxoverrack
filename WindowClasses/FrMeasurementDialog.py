"""
ATTENTION: this file is completely unfinished and broken atm !!!
"""

import numpy
from time import sleep
from PyQt5.QtCore import QCoreApplication, QUrl
from PyQt5.QtGui import QDesktopServices
from PyQt5.QtWidgets import QMessageBox, QSizePolicy
from designer_qt5.Ui_FrMeasurementDialog import Ui_FrMeasurementDialog
from math import log2, log, pi, sin, exp, sqrt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
try:
    from scipy.signal.windows import hanning
except ImportError:
    hanning = None # import errors are handled in MainWindow.py
try:
    from pyfftw.interfaces.numpy_fft import fft, ifft
except ImportError:
    ifft = fft = None # import errors are handled in MainWindow.py
try:
    from scipy.signal import find_peaks
except ImportError:
    find_peaks = None # import errors are handled in MainWindow.py

from WindowClasses.AbstractMeasurementDialog import AbstractMeasurementDialog
from helpers.constants import MPLFIGURE_BOTTOMMARGIN, MPLFIGURE_LEFTRIGHTMARGIN, \
    MPLFIGURE_TOPMARGIN, SMOOTHING_OCTAVES, TIMING_REFERENCE_FREQUENCIES
from helpers.functions import setupPltFigure, dbToGainFactor, keepPhaseBounded, GRAPH_SR
from helpers.IrFft import IrFft


START_SMOOTH = 0.15 # orig: 0.05
START_SMOOTH_WINDOW = hanning
FINISH_SMOOTH = 0.015 # orig: 0.005
FINISH_SMOOTH_WINDOW = hanning


class FrMeasurementDialog(AbstractMeasurementDialog, Ui_FrMeasurementDialog):

    def __init__(self, mainWindow):
        super().__init__(mainWindow)
        self._mplCanvas = None
        self._mplFigure = None
        self._impulseResponse = None
        self._irFft = None
        self._inhibitWindowSizeUpdate = False
        # fill combo boxes
        for o in SMOOTHING_OCTAVES.keys():
            self.comboBoxSmoothOctaves.addItem(o)
        # connect signals and actions
        self._connectSignals()



    @staticmethod
    def onHelp():
        url = QUrl('https://t-5.eu/hp/Software/Pulseaudio%20Crossover%20Rack/OnlineHelp/#fr_measurements')
        QDesktopServices().openUrl(url)


    def onStartMeasurement(self):
        if len(self._selectedOutputChannelIndexes()) != 2:
            msg = "You have to select exactly two (2) output channels."
            dlg = QMessageBox()
            dlg.critical(self, "ERROR", msg)
            return
        # reset captured data
        self._capturedData = numpy.empty(dtype=numpy.float32, shape=(0,2))
        self._samplesCaptured = 0
        # generate sine sweep signal
        fStart = self.spinBoxStartFrequency.value()
        fStop = self.spinBoxStopFrequency.value()
        duration = self.spinBoxDuration.value()
        l = duration / log(fStop / fStart)
        k = 2 * pi * fStart * l
        nSamples = self.spinBoxDuration.value() * GRAPH_SR[0]
        if nSamples % 2:
            nSamples += 1
        sweep = numpy.zeros(nSamples + GRAPH_SR[0], dtype=nu_captureThmpy.float32) # add one second for sound delay/decay
        gainFactor = dbToGainFactor(self.spinBoxGain.value()) * 0.999 # assure we don't overshoot
        for i in range(0, nSamples):
            t = i / GRAPH_SR[0]
            sweep[i] = sin(k * (exp(t / l) - 1.0)) * gainFactor
        # apply start smoothing window
        winLength = int(START_SMOOTH / sqrt(duration) * nSamples)
        win = START_SMOOTH_WINDOW(winLength * 2 + 1)
        for i in range(0, winLength + 1):
            sweep[i] = sweep[i] * win[i]
        # apply finish smoothing window
        winLength = int(FINISH_SMOOTH / sqrt(duration) * nSamples)
        win = FINISH_SMOOTH_WINDOW(winLength * 2 + 1)
        sweepIdx = nSamples - winLength - 1
        for i in range(0, winLength + 1):
            sweep[sweepIdx + i] = sweep[sweepIdx + i] * win[winLength + i]
        # generate inverse filter sweep
        factor = log2(fStop / fStart) / nSamples
        inverseFilter = numpy.zeros(sweep.size, dtype=numpy.float32)
        for i in range(0, sweep.size):
            inverseFilter[i] = sweep[sweep.size - i - 1]
        for i in range(GRAPH_SR[0], sweep.size):
            inverseFilter[i] = inverseFilter[i] * pow(0.5, factor * (i - GRAPH_SR[0]))
        self._samplesToCapture = sweep.size
        # generate timing reference (48kHz / 4 = 12kHz sine signal)
        referenceFrequency, multi = TIMING_REFERENCE_FREQUENCIES[GRAPH_SR[0]]
        referenceSamples = numpy.arange(10 * multi) / GRAPH_SR[0]
        referenceSine = numpy.sin(2 * numpy.pi * referenceFrequency * referenceSamples) * gainFactor
        referenceZeroPadding = numpy.zeros(nSamples + GRAPH_SR[0] - referenceSine.size, dtype=numpy.float32)
        referenceSignal = numpy.concatenate( (referenceSine, referenceZeroPadding) )
        # transpose the two one channel signals into a dual channel signal
        twoChannelSignal = numpy.array([referenceSignal, sweep]).transpose()
        # setup playback and capture threads
        self._playbackCaptureThread = SoundcardPlaybackThread(
                self._selectedOutputDev(),
                GRAPH_SR[0],
                twoChannelSignal,
                MEASUREMENT_CHUNKSIZE,
                None, # no callback
                channels=self._selectedOutputChannelIndexes())
        # stop level meters
        self._vuCaptureThread.stopAndWait()
        # start capture and playback threads and then wait on them
        self._startThread(self._playbackCaptureThread)
        # wait for captured data to arrive fully (at most 100 * 50ms = 5s)
        waited = 0
        toWait = int(round(self._samplesToCapture / GRAPH_SR[0] / 0.1)) + 10
        while waited <= toWait and self._samplesCaptured < self._samplesToCapture:
            sleep(0.1)
            QCoreApplication.processEvents()
            waited += 1
        # restart level meters
        self._startThread(self._vuCaptureThread)
        # check if all samples were captured
        if self._samplesCaptured < self._samplesToCapture:
            msg = "Not all samples were captured, aborting measurement"
            dlg = QMessageBox()
            dlg.critical(self, "ERROR", msg)
            return
        # split samples to analyze
        capturedSweep, capturedReference = self._capturedData.transpose()
        # find start of captured reference signal and remove starting portions of capturedSweep and inverseFilter
        firstPeakIdx = find_peaks(capturedReference, height=capturedReference.max() * 0.5)[0].min()
        if firstPeakIdx > GRAPH_SR[0] or firstPeakIdx == 0:
            msg = "Could not find start of reference signal in captured data."
            dlg = QMessageBox()
            dlg.critical(self, "ERROR", msg)
            return
        zeroPadding = numpy.zeros(firstPeakIdx - multi // 4 - 1)
        capturedSweep = numpy.concatenate( (capturedSweep[firstPeakIdx - multi // 4 - 1:], zeroPadding) )
        #_, capturedSweep = read("/home/jh/cap.wav")

        # calculate impulse response ###################################################################################
        # transform captured sweep and inverse filter to comples
        complexZeros = numpy.zeros(self._samplesToCapture, dtype=numpy.complex)
        complexFilter = numpy.concatenate( (inverseFilter.astype(numpy.complex), complexZeros) )
        complexSweep = numpy.concatenate( (capturedSweep.astype(numpy.complex), complexZeros) )
        # do the fft of sweep and filter signals
        filterFft = fft(complexFilter)
        sweepFft = fft(complexSweep)
        # multiply fft results
        normFactor = 1 / complexFilter.size
        product = filterFft * sweepFft * normFactor
        # do inverse fft to get the actual impulse response
        impulseResponse = ifft(product)
        # get real impulse response and normalize
        realImpulseResponse = numpy.real(impulseResponse)
        self._impulseResponse = realImpulseResponse / realImpulseResponse.max()
        # DEBUG: write("/home/jh/ir.wav", GRAPH_SR[0], self._impulseResponse)
        # compute amplitude/phase response
        maxLevel = max(-capturedSweep.min(), capturedSweep.max())
        self._irFft = IrFft(self._impulseResponse, maxLevel, fStart, fStop)
        ms = int(self._irFft.windowSize() * 1000)
        self._inhibitWindowSizeUpdate = True
        self.spinBoxWindowSize.setMaximum(ms)
        self.spinBoxWindowSize.setValue(ms)
        self._inhibitWindowSizeUpdate = False
        self._setupMplCanvas(self._irFft.frequencies())
        self.resizeEvent(None)
        self._recalculateFft()


    def resizeEvent(self, evt):
        if self._mplCanvas is None:
            return
        w = self._mplCanvas.width()
        h = self._mplCanvas.height()
        l = MPLFIGURE_LEFTRIGHTMARGIN / w
        r = 1 - l
        t = 1 - (MPLFIGURE_TOPMARGIN / h)
        b = MPLFIGURE_BOTTOMMARGIN / h
        self._mplFigure.subplots_adjust(left=l, right=r, top=t, bottom=b)
        if evt is not None:
            super().resizeEvent(evt)


    def _connectSignals(self):
        super()._connectSignals()
        self.spinBoxWindowSize.valueChanged.connect(self._onWindowSizeChanged)
        self.comboBoxSmoothOctaves.currentIndexChanged.connect(self._recalculateFft)


    def _readIniSettings(self):
        s = self._settings
        # dialog geometry and settings
        try:
            self.restoreGeometry(s.value("frMeasurementDialog/geometry", None))
        except (TypeError, ValueError):
            pass
        # input and output devices and channels
        self.onReloadOutputDevices(
                preselectedName=s.value("frMeasurementDialog/outputDevice", ""),
                preselectedChannels=s.value("frMeasurementDialog/outputChannels", ""))
        self.onReloadInputDevices(
                preselectedName=s.value("frMeasurementDialog/inputDevice", ""),
                preselectedLoopback=s.value("frMeasurementDialog/inputChannelLoopback", ""),
                preselectedMicrophone=s.value("frMeasurementDialog/inputChannelMicrophone", ""))
        self.onInputChannelLoopbackChanged(startCaptureThread=False)
        self.onInputChannelMicrophoneChanged(startCaptureThread=False)
        # sweep settings
        try:
            v = float(s.value("frMeasurementDialog/startFrequency", 20))
        except ValueError:
            v = 20
        self.spinBoxStartFrequency.setValue(v)
        try:
            v = float(s.value("frMeasurementDialog/stopFrequency", 20000))
        except ValueError:
            v = 20000
        self.spinBoxStopFrequency.setValue(v)
        try:
            v = int(s.value("frMeasurementDialog/duration", 10))
        except ValueError:
            v = 10
        self.spinBoxDuration.setValue(v)
        try:
            v = float(s.value("frMeasurementDialog/gain", -12))
        except ValueError:
            v = -12
        self.spinBoxGain.setValue(v)
        try:
            v = float(s.value("frMeasurementDialog/windowSize", 1000))
        except ValueError:
            v = 1000
        self.spinBoxWindowSize.setValue(v)
        v = s.value("frMeasurementDialog/smoothOctaves", "1/12 octaves")
        if v in SMOOTHING_OCTAVES.keys():
            self.comboBoxSmoothOctaves.setCurrentText(v)
        else:
            self.comboBoxSmoothOctaves.setCurrentText("1/12 octaves")


    def _onWindowSizeChanged(self):
        if self._irFft is None or self._inhibitWindowSizeUpdate:
            return
        self._irFft.setWindowSize(self.spinBoxWindowSize.value() / 1000)
        self._recalculateFft()


    def _recalculateFft(self):
        if self._irFft is None:
            return
        smoothFactor = SMOOTHING_OCTAVES[self.comboBoxSmoothOctaves.currentText()]
        self._pltLine1.set_ydata(self._irFft.amplitudes(smoothFactor))
        self._pltLine2.set_ydata(keepPhaseBounded(self._irFft.phases(smoothFactor)))
        self._mplCanvas.draw()


    def _setupMplCanvas(self, w):
        # remove old matplotlib canvas or placeholder from vertical layout
        if self._mplCanvas is None:
            try:
                self.mplCanvasPlaceholder.hide()
                self.verticalLayout.removeWidget(self.mplCanvasPlaceholder)
                self.mplCanvasPlaceholder.deleteLater()
            except RuntimeError:
                pass
        else:
            self._mplCanvas.hide()
            self.verticalLayout.removeWidget(self._mplCanvas)
            self._mplCanvas.deleteLater()
        # setup new matplotlib canvas and figure
        self._mplFigure = Figure()
        self._mplCanvas = FigureCanvas(self._mplFigure)
        self._mplCanvas.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.verticalLayout.insertWidget(1, self._mplCanvas)
        self._nodata = numpy.ones(len(w))
        self._pltLine1, self._pltAx1, self._pltLine2, self._pltAx2 = \
            setupPltFigure(w, self._nodata, self._mplCanvas, self._mplFigure,
                           -65, 5, -210, 210,
                           phaseLineThickness=1.0,
                           graph_sr=42000, fStart=10)


    def _writeIniSettings(self):
        s = self._settings
        # dialog geometry
        if self.isVisible():
            s.setValue("frMeasurementDialog/geometry", self.saveGeometry())
        # input and output devices and channels
        s.setValue("frMeasurementDialog/outputDevice", self.comboBoxOutputDevice.currentText())
        s.setValue("frMeasurementDialog/inputDevice", self.comboBoxInputDevice.currentText())
        s.setValue("frMeasurementDialog/inputChannelLoopback", self.comboBoxInputChannelLoopback.currentText())
        s.setValue("frMeasurementDialog/inputChannelMicrophone", self.comboBoxInputChannelMicrophone.currentText())
        # burst settings
        s.setValue("frMeasurementDialog/startFrequency", str(self.spinBoxStartFrequency.value()))
        s.setValue("frMeasurementDialog/stopFrequency", str(self.spinBoxStopFrequency.value()))
        s.setValue("frMeasurementDialog/duration", str(self.spinBoxDuration.value()))
        s.setValue("frMeasurementDialog/gain", str(self.spinBoxGain.value()))
        s.setValue("frMeasurementDialog/windowSize", str(self.spinBoxWindowSize.value()))
        # fft settings
        s.setValue("frMeasurementDialog/smoothOctaves", self.comboBoxSmoothOctaves.currentText())
