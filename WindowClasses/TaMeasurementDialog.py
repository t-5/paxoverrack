import numpy
from PyQt5.QtWidgets import QMessageBox
try:
    from scipy.signal.windows import gaussian
except ImportError:
    gaussian = None # import errors are handled in MainWindow.py
from PyQt5.QtCore import QUrl, pyqtSignal
from PyQt5.QtGui import QColor, QDesktopServices
from designer_qt5.Ui_TaMeasurementDialog import Ui_TaMeasurementDialog

from CustomWidgets.QOscilloscopeScreen import QOscilloscopeScreen
from WindowClasses.AbstractMeasurementDialog import AbstractMeasurementDialog
from WindowClasses.LevelCheckDialog import LevelCheckDialog
from helpers.constants import MEASUREMENT_BURST_TYPES, SPEED_OF_SOUND, WINDOW_FUNCTIONS
from helpers.functions import dbToGainFactor, GRAPH_SR
from helpers.threads import SoundcardPlaybackCaptureThread


class TaMeasurementDialog(AbstractMeasurementDialog, Ui_TaMeasurementDialog):


    signal_updateMarkerPositions = pyqtSignal(int, int)


    COLORCODE_MARKER1 = '#ffff00'
    COLORCODE_MARKER2 = '#ff6600'
    COLORCODE_LINE1 = '#00ff00'
    COLORCODE_LINE2 = '#00ffff'
    COLOR_MARKER1 = QColor(COLORCODE_MARKER1)
    COLOR_MARKER2 = QColor(COLORCODE_MARKER2)
    COLOR_LINE1 = QColor(COLORCODE_LINE1)
    COLOR_LINE2 = QColor(COLORCODE_LINE2)
    ZOOM_FACTORS = (  1.0,   1.2,   1.5,   2.0,   2.5,   3.0,   4.0,   5.0,  7.5,
                     10.0,  12.0,  15.0,  20.0,  25.0,  30.0,  40.0,  50.0, 75.0,
                    100.0, 120.0, 150.0, 200.0, 250.0, 300.0, 400.0, 500.0)


    def __init__(self, mainWindow):
        super().__init__(mainWindow)
        self.horizontalScrollBar.setStyleSheet("QScrollBar::handle:horizontal { min-width: 20px; }""")
        self.horizontalScrollBar.setMaximum(0)
        self.horizontalScrollBar.setValue(0)
        self.labelStaticMarker1.setStyleSheet("color: %s" % self.COLORCODE_MARKER1)
        self.labelStaticMarker2.setStyleSheet("color: %s" % self.COLORCODE_MARKER2)
        self.labelStaticCurrent.setStyleSheet("color: %s" % self.COLORCODE_LINE1)
        self.labelStaticPrevious.setStyleSheet("color: %s" % self.COLORCODE_LINE2)
        # setup combo Boxes
        self.comboBoxWindowFunction.addItems(WINDOW_FUNCTIONS)
        self.comboBoxBurstType.addItems(MEASUREMENT_BURST_TYPES)
        # insert oscilloscope screen
        self.oscilloscopeScreenPlaceholder.hide()
        self.verticalLayout.removeWidget(self.oscilloscopeScreenPlaceholder)
        self._oscilloscopeScreen = QOscilloscopeScreen(self, self.COLOR_LINE1, self.COLOR_LINE2, self.COLOR_MARKER1,
                                                       self.COLOR_MARKER2, GRAPH_SR[0], self.signal_updateMarkerPositions)
        self.verticalLayout.insertWidget(0, self._oscilloscopeScreen, stretch=1)
        # settings
        self._readIniSettings()
        # setup zoom sliders
        self.sliderHorizontalZoom.setMaximum(len(self.ZOOM_FACTORS) - 1)
        self.sliderVerticalZoom.setMaximum(len(self.ZOOM_FACTORS) - 1)
        # connect signals and actions
        self._connectSignals()
        # internal state
        self._measuring = False


    @staticmethod
    def onHelp():
        url = QUrl('https://t-5.eu/hp/Software/Pulseaudio%20Crossover%20Rack/OnlineHelp/#time_alignment')
        QDesktopServices().openUrl(url)


    def onLevelCheck(self):
        self._vuCaptureThread.stopAndWait()
        dlg = LevelCheckDialog(self, (self.comboBoxInputChannelMicrophone.currentText(),
                                      self.comboBoxInputChannelLoopback.currentText()))
        dlg.exec()
        dlg.deleteLater()
        self._startThread(self._vuCaptureThread)



    def onStartMeasurement(self):
        if self._measuring:
            return
        self._measuring = True
        self.sliderHorizontalZoom.setValue(0)
        # reset captured samples
        self._capturedData = numpy.empty(dtype=numpy.float32, shape=(0,2))
        self._samplesCaptured = 0
        self._samplesPlayed = 0
        # generate sine burst signal
        additionalLength = self.comboBoxBurstType.currentText() == "Peak in the center" and 0.5 or 0
        durationInSamples = round(GRAPH_SR[0] / self.spinBoxFrequency.value() * (self.spinBoxDuration.value() + additionalLength))
        samples = numpy.arange(durationInSamples) / GRAPH_SR[0]
        sine = numpy.sin(2 * numpy.pi * self.spinBoxFrequency.value() * samples) * \
               dbToGainFactor(self.spinBoxGain.value())
        # apply window function
        if self.comboBoxWindowFunction.currentText() == "Kaiser":
            window = numpy.kaiser(durationInSamples, 14)
        elif self.comboBoxWindowFunction.currentText() == "Triangle":
            window = numpy.bartlett(durationInSamples)
        elif self.comboBoxWindowFunction.currentText() == "Gauss":
            window = gaussian(durationInSamples, 14)
        elif self.comboBoxWindowFunction.currentText() == "Blackman":
            window = numpy.blackman(durationInSamples)
        else: # uniform is default
            window = None
        if window is not None:
            sine = sine * window
        # assemble whole signal to be played
        preDelayZeros = numpy.zeros(int(self.spinBoxPreDelay.value() / 1000 * GRAPH_SR[0]))
        postDelayZeros = numpy.zeros(int(self.spinBoxPostDelay.value() / 1000 * GRAPH_SR[0]))
        singleChannelSignal = numpy.concatenate((preDelayZeros, sine, postDelayZeros))
        # transpose into two channel signal
        signal = numpy.array([singleChannelSignal] * 2).transpose().astype(numpy.float32)

        # stop level meters
        self._vuCaptureThread.stopAndWait()
        # start playback/capture
        thread = SoundcardPlaybackCaptureThread(GRAPH_SR[0], signal)
        self._startThread(thread)
        thread.stopAndWait()
        self._capturedData = thread.getCapturedSamples()
        if numpy.isnan(self._capturedData.min()) or thread.failure():
            QMessageBox().critical(self, "ERROR", "Failed to playback/capture samples: %s" % thread.failure())
            self._measuring = False
            return
        self._capturedData = self._transpose(self._capturedData)
        if self._capturedData[1].size == 0:
            QMessageBox().critical(self, "ERROR", "No samples captured")
            self._measuring = False
            return
        # find trigger start on loopback channel and truncate signal before trigger
        firstSampleIndex = numpy.argmax(self._capturedData[1] > self.doubleSpinBoxTriggerThreshold.value())
        if firstSampleIndex == 0:
            QMessageBox().warning(self, "WARNING", "Trigger signal not found.\n\n"
                                        "Either your loopback does not work (wrong channel?) "
                                        "or you set the trigger threshold too high (try using 0.01 - 0.1).\n\n"
                                        "Also your post delay could be too short to capture the actual signal "
                                        "(try at least 200ms then).")
        preDelaySamples = round(self.spinBoxPreDelay.value() / 1000 * GRAPH_SR[0])
        firstSampleIndex = max(0, firstSampleIndex - preDelaySamples)
        self._capturedData = (self._capturedData[0][firstSampleIndex:], None) # loopback data is not needed anymore
        # set samples for oscilloscope screen
        oldSamples = self._oscilloscopeScreen.getSamples1()
        if oldSamples.size < self._capturedData[0].size:
            # need to pad with zeros
            zeros = numpy.zeros(self._capturedData[0].size - oldSamples.size)
            oldSamples = numpy.concatenate((oldSamples, zeros))
        elif oldSamples.size > self._capturedData[0].size:
            # need to truncate oldsamples
            oldSamples = oldSamples[:self._capturedData[0].size]
        self._oscilloscopeScreen.setSamples(self._capturedData[0], oldSamples)
        self._oscilloscopeScreen.setStartSample(0, ignoreError=True)
        self._oscilloscopeScreen.setEndSample(self._capturedData[0].size - 1)
        self._oscilloscopeScreen.update()
        # restart level meters
        self._startThread(self._vuCaptureThread)
        self._measuring = False


    def _connectSignals(self):
        super()._connectSignals()
        self.horizontalScrollBar.valueChanged.connect(self._horizontalScroll)
        # noinspection PyUnresolvedReferences
        self.signal_updateMarkerPositions.connect(self._updateMarkerPositions)
        self.sliderHorizontalZoom.valueChanged.connect(self._horizontalZoomChanged)
        self.sliderVerticalZoom.valueChanged.connect(self._verticalZoomChanged)
        self.toolButtonResetLevels.clicked.connect(self._resetLevels)


    def _enableMeasurementButton(self, enabled):
        self.toolButtonStartMeasurement.blockSignals(not enabled)


    def _horizontalScroll(self):
        sampleSize = self._oscilloscopeScreen.getSampleSize()
        visibleRange = self._oscilloscopeScreen.getEndSample() - self._oscilloscopeScreen.getStartSample()
        scrollableRange = sampleSize - visibleRange
        newStartPosition = int(round(self.horizontalScrollBar.value() / self.horizontalScrollBar.maximum() * \
                                     scrollableRange))
        newEndPosition = min(sampleSize - 1, newStartPosition + visibleRange)
        self._oscilloscopeScreen.setStartEndSample(newStartPosition, newEndPosition)


    def _horizontalZoomChanged(self):
        zf = self.ZOOM_FACTORS[self.sliderHorizontalZoom.value()]
        self.labelHorizontalZoom.setText("x%0.1f" % zf)
        sampleSize = self._oscilloscopeScreen.getSampleSize()
        newSamplesHalfed = int(round(sampleSize / zf / 2))
        mediumSample = int(round((self._oscilloscopeScreen.getStartSample() +
                                  self._oscilloscopeScreen.getEndSample()) / 2))
        newStartSample = mediumSample - newSamplesHalfed
        newEndSample = mediumSample + newSamplesHalfed
        if newStartSample < 0:
            newStartSample = 0
            newEndSample = min(2 * newSamplesHalfed, sampleSize - 1)
            mediumSample = newSamplesHalfed
            self.horizontalScrollBar.setValue(0)
        elif newEndSample > self._oscilloscopeScreen.getSampleSize() - 1:
            newEndSample = self._oscilloscopeScreen.getSampleSize() - 1
            newStartSample = max(0, newEndSample - 2 * newSamplesHalfed)
            mediumSample = newStartSample + newSamplesHalfed
            self.horizontalScrollBar.setValue(1000)
        else:
            max_ = self.horizontalScrollBar.maximum()
            if zf == 1.0:
                newPosition = int(round(max_ / 2))
            else:
                newPosition = max_ * mediumSample / sampleSize
            self.horizontalScrollBar.setValue(int(round(newPosition)))
        self._oscilloscopeScreen.setEndSample(newEndSample)
        self._oscilloscopeScreen.setStartSample(newStartSample)
        self._oscilloscopeScreen.update()
        # update scrollbar
        self.horizontalScrollBar.valueChanged.disconnect()
        if zf == 1.0:
            self.horizontalScrollBar.setMaximum(0)
            self.horizontalScrollBar.setValue(0)

        elif zf < 2.0:
            self.horizontalScrollBar.setMaximum(4)
            self.horizontalScrollBar.setValue(int(round(mediumSample / sampleSize * 4)))
        else:
            maximum = int(round(zf * 10))
            if (maximum % 2) != 0:
                maximum += 1
            self.horizontalScrollBar.setMaximum(maximum)
            self.horizontalScrollBar.setValue(int(round(mediumSample / sampleSize * maximum)))
        self.horizontalScrollBar.valueChanged.connect(self._horizontalScroll)


    def _readIniSettings(self):
        s = self._settings
        # dialog geometry and settings
        try:
            self.restoreGeometry(s.value("taMeasurementDialog/geometry", None))
        except (TypeError, ValueError):
            pass
        # burst settings
        try:
            v = float(s.value("taMeasurementDialog/frequency", 440))
        except ValueError:
            v = 440
        self.spinBoxFrequency.setValue(v)
        try:
            v = int(s.value("taMeasurementDialog/duration", 6))
        except ValueError:
            v = 10
        self.spinBoxDuration.setValue(v)
        try:
            v = float(s.value("taMeasurementDialog/gain", -12))
        except ValueError:
            v = -12
        self.spinBoxGain.setValue(v)
        try:
            v = int(s.value("taMeasurementDialog/preDelay", 50))
        except ValueError:
            v = 100
        self.spinBoxPreDelay.setValue(v)
        try:
            v = int(s.value("taMeasurementDialog/postDelay", 200))
        except ValueError:
            v = 100
        self.spinBoxPostDelay.setValue(v)
        # input channels
        il = s.value("taMeasurementDialog/inputChannelLoopback", "Right")
        im = s.value("taMeasurementDialog/inputChannelMicrophone", "Left")
        self._populateInputChannels(preselectedLoopback=il, preselectedMicrophone=im)
        # trigger threshold
        try:
            v = float(s.value("taMeasurementDialog/triggerThreshold", 0.01))
        except ValueError:
            v = 0.01
        if v < 0.0001:
            v = 0.01
        self.doubleSpinBoxTriggerThreshold.setValue(v)
        # combo boxes
        self.comboBoxBurstType.setCurrentText(s.value("taMeasurementDialog/burstType", "Zero crossing in the center"))
        self.comboBoxWindowFunction.setCurrentText(s.value("taMeasurementDialog/windowFunction", "Blackman"))


    def _resetLevels(self):
        self.levelMeterLoopback.resetPeakLevelMax()
        self.levelMeterMicrophone.resetPeakLevelMax()


    def _updateMarkerPositions(self, marker1, marker2):
        m1 = ((marker1 + 1) / GRAPH_SR[0] * 1000)
        m2 = ((marker2 + 1) / GRAPH_SR[0] * 1000)
        self.labelMarker1.setText("%0.3fms" % m1)
        self.labelMarker2.setText("%0.3fms" % m2)
        dt = abs(m1 - m2)
        self.labelDistance.setText("%0.3fms (%0.1fmm)" % (dt, SPEED_OF_SOUND * dt))


    def _verticalZoomChanged(self):
        zf = self.ZOOM_FACTORS[self.sliderVerticalZoom.value()]
        self._oscilloscopeScreen.setVerticalZoom(zf)
        self.labelVerticalZoom.setText("x%0.1f" % zf)


    def _writeIniSettings(self):
        s = self._settings
        # dialog geometry
        if self.isVisible():
            s.setValue("taMeasurementDialog/geometry", self.saveGeometry())
        # input channels
        s.setValue("taMeasurementDialog/inputChannelLoopback", self.comboBoxInputChannelLoopback.currentText())
        s.setValue("taMeasurementDialog/inputChannelMicrophone", self.comboBoxInputChannelMicrophone.currentText())
        # burst settings
        s.setValue("taMeasurementDialog/frequency", str(self.spinBoxFrequency.value()))
        s.setValue("taMeasurementDialog/duration", str(self.spinBoxDuration.value()))
        s.setValue("taMeasurementDialog/gain", str(self.spinBoxGain.value()))
        s.setValue("taMeasurementDialog/preDelay", str(self.spinBoxPreDelay.value()))
        s.setValue("taMeasurementDialog/postDelay", str(self.spinBoxPostDelay.value()))
        # trigger threshold
        s.setValue("taMeasurementDialog/triggerThreshold", str(self.doubleSpinBoxTriggerThreshold.value()))
        # combo boxes
        s.setValue("taMeasurementDialog/burstType", self.comboBoxBurstType.currentText())
        s.setValue("taMeasurementDialog/windowFunction", self.comboBoxWindowFunction.currentText())
