from PyQt5.QtCore import QSize
from PyQt5.QtGui import QPainter, QPainterPath, QColor
from PyQt5.QtWidgets import QWidget, QMessageBox

from helpers.functions import bezierIntermediatesFor


class DrawArea(QWidget):

    CONNECT_BEZIER_COLOR = QColor(255, 255, 0)

    def __init__(self, parent=None, mainWindow=None):
        super().__init__(parent=parent)
        self._mainWindow = mainWindow
        self.setFixedSize(parent.size())
        self._widgets = []
        self._connectDragPath = None
        self._connectDragStartPos = None
        self._connectDragType = None


    def addWidget(self, widget):
        self._widgets.append(widget)
        self.setFixedSize(self.sizeHint())


    def clear(self):
        for widget in self._widgets:
            widget.deleteLater()
        self.setFixedSize(self.sizeHint())
        self._widgets = []


    def connectDragMove(self, dest):
        if self._connectDragStartPos is None:
            return
        path = QPainterPath()
        destX = dest.x()
        destY = dest.y()
        im1X, im1Y, im2X, im2Y = bezierIntermediatesFor(self._connectDragStartPos, dest)
        # draw path
        path.moveTo(self._connectDragStartPos.x(), self._connectDragStartPos.y())
        path.cubicTo(im1X, im1Y, im2X, im2Y, destX, destY)
        self._connectDragPath = path
        self.repaint()


    def connectDragStart(self, pos, type_):
        self._connectDragStartPos = pos
        self._connectDragType = type_


    def connectDragStop(self, pos, fromWidget):
        for toWidget in self._widgets:
            if toWidget == fromWidget:
                continue
            if toWidget.inputDotRect().contains(pos) and self._connectDragType == "ltr":
                fromWidget.connectWidget(toWidget)
                if self._mainWindow.findCyclicGraph():
                    fromWidget.disconnectWidget(toWidget)
                    self._informAboutCycle()
            if toWidget.outputDotRect().contains(pos) and self._connectDragType == "rtl":
                toWidget.connectWidget(fromWidget)
                if self._mainWindow.findCyclicGraph():
                    toWidget.disconnectWidget(fromWidget)
                    self._informAboutCycle()
        self._connectDragStartPos = None
        self._connectDragPath = None
        self._connectDragType = None
        self.repaint()


    def mainWindow(self):
        return self._mainWindow


    def paintEvent(self, e):
        qp = QPainter()
        qp.begin(self)
        qp.setRenderHint(QPainter.Antialiasing)
        for widget in self._widgets:
            try:
                for path in widget.connectionPaths():
                    qp.drawPath(path)
            except RuntimeError:
                pass # prevent crash after deleting an OutputWidget
        if self._connectDragPath is not None:
            qp.setPen(self.CONNECT_BEZIER_COLOR)
            qp.drawPath(self._connectDragPath)
        qp.end()


    def removeWidget(self, widget):
        if widget in self._widgets:
            self._widgets.remove(widget)
            self.setFixedSize(self.sizeHint())
            widget.deleteLater()


    def repaint(self):
        self.setFixedSize(self.sizeHint())
        super().repaint()


    def sizeHint(self):
        maxX = 0
        maxY = 0
        for widget in self._widgets:
            widget.resizeToZoomedSize()
            maxX = max(maxX, widget.width() + widget.x())
            maxY = max(maxY, widget.height() + widget.y())
        return QSize(maxX + 2, maxY + 2)


    def widgets(self):
        return self._widgets


    def _informAboutCycle(self):
        dlg = QMessageBox()
        dlg.critical(self, "ERROR, Cycle detected!",
                     "You cannot connect two outputs to one input.\n"
                     "This would form a cyclic connection!")
