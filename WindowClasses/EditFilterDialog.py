import io
import math

import numpy
from PyQt5 import QtCore
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QDialog, QLabel, QLineEdit, QMessageBox, QPushButton, QSlider
from designer_qt5.Ui_EditFilterDialog import Ui_EditFilterDialog
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

from DataClasses import XoverFilter
from WindowClasses.ParameterLinkingDialog import ParameterLinkingDialog
from helpers.constants import FILTER_EDIT_DLG_SLIDER_MAX, EDIT_FLOAT_FORMAT
from helpers.functions import GRAPH_SR, setupPltFigure, keepPhaseBounded


class EditFilterDialog(QDialog, Ui_EditFilterDialog):

    def __init__(self, parent, filter_: XoverFilter, newlyCreated=False):
        super().__init__(parent=parent)
        self.setupUi(self)
        self._newlyCreated = newlyCreated
        self._filter = filter_
        # newly created?
        if newlyCreated:
            self.setWindowTitle("Add new %s..." % filter_.filterName())
        else:
            self.setWindowTitle("Edit %s '%s'" % (filter_.filterName(), self._filter.sinkName()))
        # restore window geometry
        self._readIniSettings()
        # add inputs according to filter parameters
        self._sliders = []
        self._lineEdits = []
        self._sliderUpdates = True
        self._lineEditUpdates = True
        labelHeight = 30
        sliderHeight = 30
        topMargin = 5
        leftMargin = 5
        y = 8
        # sink name label
        label = QLabel("Sink name:", self.scrollAreaWidgetContents)
        label.setGeometry(leftMargin, y, 100, labelHeight)
        # sink name line edit
        self._lineEditSinkName = QLineEdit(filter_.sinkName(), self.scrollAreaWidgetContents)
        self._lineEditSinkName.setGeometry(110, y, 357, labelHeight)
        self._lineEditSinkName.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        # add labels, lin edits and sliders for all filter paramteters
        y += labelHeight + topMargin
        for pMap in filter_.parameterMappings():
            # label
            label = QLabel(pMap["description"] + ":", self.scrollAreaWidgetContents)
            label.setGeometry(leftMargin, y, 350, labelHeight)
            # line edit
            lineEdit = QLineEdit("0.0", self.scrollAreaWidgetContents)
            lineEdit.setGeometry(315, y, 150, labelHeight)
            lineEdit.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
            # noinspection PyUnresolvedReferences
            lineEdit.textChanged.connect(self._lineEditTextChanged)
            self._lineEdits.append(lineEdit)
            y += labelHeight
            # slider
            slider = QSlider(Qt.Horizontal, self.scrollAreaWidgetContents)
            slider.setMinimum(0)
            slider.setMaximum(FILTER_EDIT_DLG_SLIDER_MAX)
            slider.setValue(int(round(FILTER_EDIT_DLG_SLIDER_MAX / 2)))
            slider.setGeometry(leftMargin, y, 460, sliderHeight)
            # noinspection PyUnresolvedReferences
            slider.valueChanged.connect(self._sliderValueChanged)
            y += sliderHeight + topMargin
            self._sliders.append(slider)
            if pMap.get("dividerAfter", False):
                label = QLabel("", self.scrollAreaWidgetContents)
                label.setPixmap(QPixmap(":/MainWindow/divider.png"))
                label.setGeometry(leftMargin, y, 460, 11)
                label.setScaledContents(True)
                y += 11 + topMargin
        # retrieve param defaults for new filter or get current params from existing filter
        for i, pMap in enumerate(filter_.parameterMappings()):
            if newlyCreated:
                v = pMap["default"]
                pMap["setter"](v)
            else:
                v = pMap["getter"]()
            self._lineEdits[i].setText(EDIT_FLOAT_FORMAT % v)
        # add buttons
        y += 10
        if newlyCreated:
            cancelButton = QPushButton("Cancel", self.scrollAreaWidgetContents)
            cancelButton.setGeometry(382, y, 80, 35)
            # noinspection PyUnresolvedReferences
            cancelButton.clicked.connect(self.onCancel)
            addButton = QPushButton("Add Filter", self.scrollAreaWidgetContents)
            addButton.setGeometry(272, y, 100, 35)
            # noinspection PyUnresolvedReferences
            addButton.clicked.connect(self.onAdd)
            addButton.setDefault(True)
            # find unique sink name for filter
            oldname = filter_.sinkName()
            name = oldname
            i = 0
            while parent.nameExists(name):
                i += 1
                name = oldname + str(i)
            filter_.setSinkName(name)
            self._lineEditSinkName.setText(name)
        else:
            closeButton = QPushButton("Close", self.scrollAreaWidgetContents)
            closeButton.setGeometry(382, y, 80, 35)
            # noinspection PyUnresolvedReferences
            closeButton.clicked.connect(self.onClose)
            if self._filter.canBeLinked():
                editLinksButton = QPushButton("Edit links", self.scrollAreaWidgetContents)
                editLinksButton.setGeometry(262, y, 100, 35)
                # noinspection PyUnresolvedReferences
                editLinksButton.clicked.connect(self.onEditLinks)
        self.scrollAreaWidgetContents.setFixedHeight(y + 45)
        # setup graphing things
        self.replacementFrame.hide()
        self._mplFigure = Figure()
        self._mplFigureSetup = False
        self._mplCanvas = FigureCanvas(self._mplFigure)
        self._mplCanvas.setParent(self)
        self._mplCanvas.setGeometry(5, 5, 490, 191)
        # setup update ui timer
        self._timerUpdateUi = QtCore.QTimer(self)
        self._timerUpdateUi.setInterval(50)
        # noinspection PyUnresolvedReferences
        self._timerUpdateUi.timeout.connect(self._updateFrGraph)
        self._timerUpdateUi.start()
        filter_.markDirty() # force redraw of fr graph


    def closeEvent(self, e):
        self._timerUpdateUi.stop()
        self._updateFilterFrPng()
        linkedBy = self._filter.linkedBy()
        if linkedBy is not None:
            l = linkedBy.allLinkedFiltersExcept(self._filter)
            for flt in l:
                self._updateFrGraph(flt=flt)
                self._mplCanvas.draw()
                self._updateFilterFrPng(flt)
                flt.repaintWidget()


    def onAdd(self):
        if self._checkAndSaveSinkName() != 0:
            return
        self._updateFilterFrPng()
        self.parent().xoverFileAddObject(self._filter)
        self._filter.restoreWidget(self.parent().drawArea())
        self._writeIniSettings()
        self.close()


    def onCancel(self):
        self._writeIniSettings()
        self.close()


    def onClose(self):
        if self._checkAndSaveSinkName() != 0:
            return
        self._writeIniSettings()
        self.close()


    def onEditLinks(self):
        dlg = ParameterLinkingDialog(self._filter, self.parent())
        dlg.exec()
        dlg.deleteLater()


    def _checkAndSaveSinkName(self):
        name = self._lineEditSinkName.text()
        if not self.parent().isUniqueName(name, exclude=self._filter):
            QMessageBox().critical(self, "ERROR", "The name '%s' is not unique!" % name)
            return -1
        checkResult = self.parent().checkSinkName(name)
        if checkResult:
            msg = "The name '%s' has errors:\n" % name
            QMessageBox().critical(self, "ERROR", msg + checkResult)
            return -2
        self._filter.setSinkName(name)
        return 0


    def reject(self):
        self._updateFilterFrPng()
        self._timerUpdateUi.stop()


    def resizeEvent(self, e):
        self.scrollArea.setGeometry(5, 200, self.width() - 10, self.height() - 200)


    @staticmethod
    def _lineEditFloatToSliderInt(f, min_, max_, scale):
        if scale == "lin":
            return int(round((f - min_) / (max_ - min_) * FILTER_EDIT_DLG_SLIDER_MAX))
        if scale == "log2":
            if f == 0:
                return 1
            ratio = (max_ - min_) / f
            rlog2 = math.log(max_ / min_, 2)
            return int(round((rlog2 - math.log(ratio, 2)) / rlog2 * FILTER_EDIT_DLG_SLIDER_MAX))
        if scale == "log10":
            if f == 0:
                return 1
            ratio = (max_ - min_) / f
            rlog10 = math.log(max_ / min_, 10)
            return int(round((rlog10 - math.log(ratio, 10)) / rlog10 * FILTER_EDIT_DLG_SLIDER_MAX))
        raise ValueError("wrong scale '%s'" % scale)


    def _lineEditTextChanged(self, v):
        if not self._lineEditUpdates:
            return
        # noinspection PyTypeChecker
        idx = self._lineEdits.index(self.sender())
        parameters = self._filter.parameterMappings()[idx]
        try:
            f = max(parameters['min'], min(parameters['max'], float(v)))
        except ValueError:
            return
        i = self._lineEditFloatToSliderInt(f, parameters['min'], parameters['max'], parameters['scale'])
        parameters["setter"](f)
        self._filter.markDirty()
        self._sliderUpdates = False
        self._sliders[idx].setValue(i)
        self._sliderUpdates = True


    def _readIniSettings(self):
        s = self.parent().settings()
        try:
            x, y, w, h = int(s.value("editFilterDialog/windowX", None)), \
                         int(s.value("editFilterDialog/windowY", None)), \
                         int(s.value("editFilterDialog/windowW", None)), \
                         int(s.value("editFilterDialog/windowH", None))
        except (TypeError, ValueError):
            x, y, w, h = (None, None, None, None)
        if None not in (x, y, w, h):
            self.setGeometry(x, y, w, h)


    def _setupPltFig(self, w, h):
        self._pltLine1, self._pltAx1, self._pltLine2, self._pltAx2 = \
            setupPltFigure(w, h, self._mplCanvas, self._mplFigure,
                           self._filter.gainGraphLowerLimit(), self._filter.gainGraphUpperLimit(),
                           self._filter.phaseLowerLimit(), self._filter.phaseUpperLimit())
        self._mplFigure.subplots_adjust(left=0.13, right=0.86, top=0.97, bottom=0.13)


    @staticmethod
    def _sliderIntToLineEditFloat(i, min_, max_, scale):
        if scale == "lin":
            return ((max_ - min_) / FILTER_EDIT_DLG_SLIDER_MAX * i) + min_
        if scale == "log2":
            ratio = i / FILTER_EDIT_DLG_SLIDER_MAX * math.log(max_/min_, 2)
            return math.pow(2, ratio) * min_
        if scale == "log10":
            ratio = i / FILTER_EDIT_DLG_SLIDER_MAX * math.log(max_/min_, 10)
            return math.pow(10, ratio) * min_
        raise ValueError("wrong scale '%s'" % scale)


    def _sliderValueChanged(self, v):
        if not self._sliderUpdates:
            return
        # noinspection PyTypeChecker
        idx = self._sliders.index(self.sender())
        parameters = self._filter.parameterMappings()[idx]
        f = self._sliderIntToLineEditFloat(v, parameters['min'], parameters['max'], parameters['scale'])
        parameters["setter"](f)
        self._filter.markDirty()
        self._lineEditUpdates = False
        self._lineEdits[idx].setText(EDIT_FLOAT_FORMAT % f)
        self._lineEditUpdates = True


    def _updateFilterFrPng(self, flt=None):
        bio = io.BytesIO()
        self._mplCanvas.print_png(bio)
        bio.seek(0)
        if flt is None:
            flt = self._filter
        flt.setFrequencyResponsePng(bio.read())
        flt.updateOutputFrequencyResponsePngs()


    def _updateFrGraph(self, flt=None):
        if flt is None:
            flt = self._filter
        if not flt.isDirty():
            return
        w, h = flt.frequencyResponse()
        w = w * GRAPH_SR[0] / math.pi * 0.5
        if not self._mplFigureSetup:
            self._setupPltFig(w, h)
            self._mplFigureSetup = True
        self._pltLine1.set_ydata(20 * numpy.log10(abs(h)))
        self._pltLine2.set_ydata(keepPhaseBounded(numpy.unwrap(numpy.angle(h)) / math.pi * 180))
        self._mplCanvas.draw()
        flt.markClean()


    def _writeIniSettings(self):
        """ write ini settings """
        s = self.parent().settings()
        s.setValue("editFilterDialog/windowX", self.x())
        s.setValue("editFilterDialog/windowY", self.y())
        s.setValue("editFilterDialog/windowW", self.width())
        s.setValue("editFilterDialog/windowH", self.height())


