import datetime
from time import time

from PyQt5.QtCore import QTimer, Qt
from PyQt5.QtGui import QColor, QTextCursor
from PyQt5.QtWidgets import QDialog
from designer_qt5.Ui_ConsoleDialog import Ui_ConsoleDialog
from qt5_t5darkstyle import darkstyle_css


class ConsoleDialog(QDialog, Ui_ConsoleDialog):

    COLOR_WHITE = QColor(255, 255, 255)
    COLOR_GREEN = QColor(0, 255, 0)
    COLOR_RED = QColor(255, 0, 0)
    COLOR_YELLOW = QColor(255, 255, 0)

    def __init__(self, mainWindow):
        super(ConsoleDialog, self).__init__()
        self.setupUi(self)
        self.setWindowFlags(Qt.WindowCloseButtonHint | Qt.WindowMinimizeButtonHint | Qt.WindowMaximizeButtonHint)
        self._mainWindow = mainWindow
        self._lastUpdate = time()
        self._ignoreAutoClose = False
        self.setStyleSheet(darkstyle_css())
        self.pushButtonClose.clicked.connect(self.onClose)
        self.checkBoxAutoClose.stateChanged.connect(self.onAutoCloseChecked)
        ## setup autoclose timer
        self._timerAutoClose = QTimer(self)
        self._timerAutoClose.setInterval(500)
        # noinspection PyUnresolvedReferences
        self._timerAutoClose.timeout.connect(self._autoCloseHandler)
        self._timerAutoClose.start()



    def closeEvent(self, e):
        s = self._mainWindow.settings()
        s.setValue("consoleDialog/x", self.x())
        s.setValue("consoleDialog/y", self.y())
        s.setValue("consoleDialog/height", self.height())
        s.setValue("consoleDialog/width", self.width())
        s.setValue("consoleDialog/autoClose", int(self.checkBoxAutoClose.isChecked()))
        s.setValue("consoleDialog/autoCloseSeconds", self.spinBoxAutoClose.value())


    def error(self, text):
        self._appendTimePrefix()
        self._appendColoredText(text + "\n", self.COLOR_RED)
        self._lastUpdate = time()


    def hint(self, text):
        self._appendColoredText(text + "\n", self.COLOR_WHITE)
        self._lastUpdate = time()


    def line(self):
        if self.textEditConsole.toPlainText() == "":
            return
        metrics = self.textEditConsole.fontMetrics()
        w = int(self.textEditConsole.width() / metrics.width("-")) - 1
        self._appendColoredText("\n" + "-" * w + "\n", self.COLOR_WHITE)
        self._lastUpdate = time()


    def onAutoCloseChecked(self):
        v = self.checkBoxAutoClose.isChecked()
        self.spinBoxAutoClose.setEnabled(v)
        if v:
            self._ignoreAutoClose = True
            self._timerAutoClose.start()
        else:
            self._timerAutoClose.stop()


    def onClose(self):
        self.close()


    def setAutoClose(self, v):
        self.spinBoxAutoClose.setEnabled(v)
        self.checkBoxAutoClose.setChecked(v)
        if v:
            self._timerAutoClose.start()
        else:
            self._timerAutoClose.stop()


    def setAutoCloseSeconds(self, v):
        self.spinBoxAutoClose.setValue(v)


    def showNoAutoClose(self):
        self._ignoreAutoClose = True
        self.show()


    def showAutoClose(self):
        self._ignoreAutoClose = False
        self.show()


    def success(self, text):
        self._appendTimePrefix()
        self._appendColoredText(text + "\n", self.COLOR_GREEN)
        self._lastUpdate = time()


    def warning(self, text):
        self._appendTimePrefix()
        self._appendColoredText(text + "\n", self.COLOR_YELLOW)
        self._lastUpdate = time()


    def _appendColoredText(self, text, color):
        self.textEditConsole.moveCursor(QTextCursor.End)
        self.textEditConsole.setTextColor(color)
        self.textEditConsole.insertPlainText(text)


    def _appendTimePrefix(self):
        self.textEditConsole.moveCursor(QTextCursor.End)
        self.textEditConsole.setTextColor(self.COLOR_WHITE)
        self.textEditConsole.insertPlainText('[{0:%H:%M:%S.%f}] '.format(datetime.datetime.now()))


    def _autoCloseHandler(self):
        if not self.checkBoxAutoClose.isChecked() or self._ignoreAutoClose:
            return
        if self._lastUpdate < time() - self.spinBoxAutoClose.value():
            self.close()