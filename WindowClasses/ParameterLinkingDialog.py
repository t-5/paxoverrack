from PyQt5.QtCore import QUrl
from PyQt5.QtGui import QDesktopServices
from PyQt5.QtWidgets import QDialog, QInputDialog, QMessageBox
from designer_qt5.Ui_ParameterLinkingDialog import Ui_ParameterLinkingDialog
from qt5_t5darkstyle import darkstyle_css

from DataClasses.XoverFilterLink import XoverFilterLink


class ParameterLinkingDialog(QDialog, Ui_ParameterLinkingDialog):
    def __init__(self, flt, mainWindow):
        super(ParameterLinkingDialog, self).__init__()
        self.setupUi(self)
        self.setStyleSheet(darkstyle_css())
        self._filter = flt
        self._mainWindow = mainWindow
        self._lowHighPassFilters = self._mainWindow.lowHighPassFilters()
        self._dirty = False
        self.setModal(True)
        # connect actions
        self.toolButtonClose.clicked.connect(self.onClose)
        self.pushButtonClose.clicked.connect(self.onClose)
        self.toolButtonAdd.clicked.connect(self.onAdd)
        self.toolButtonDelete.clicked.connect(self.onDelete)
        self.toolButtonHelp.clicked.connect(self.onHelp)
        # initialize listWidget
        self._name2filter = {}
        for flt in self._mainWindow.allLinkableFilters():
            self._name2filter[flt.sinkName()] = flt
        self.initListWidget()


    def exec(self):
        super().exec()
        return self._dirty


    def initListWidget(self):
        self.listWidget.clear()
        link = self._filter.linkedBy()
        if link is not None:
            for flt in link.allLinkedFiltersExcept(self._filter):
                self.listWidget.addItem(flt.sinkName())
                self._name2filter[flt.sinkName()] = flt


    def onAdd(self):
        dlg = QInputDialog(self)
        item, result = dlg.getItem(self, "Choose a filter to link...", "Filter to link:", sorted(self._name2filter.keys()))
        if not result:
            return
        link = self._filter.linkedBy()
        linkTo = self._name2filter[item]
        if item == self._filter.sinkName():
            return
        if linkTo.linkedBy() is not None:
            msg = "The filter '%s' you want to link to is already linked to one or more other filters."\
                  "Do you really want to add to these links?" % item
            reply = QMessageBox().question(self, 'Existing link detected!',
                                           msg,
                                           QMessageBox.No | QMessageBox.Yes,
                                           QMessageBox.Yes)
            if reply != QMessageBox.Yes:
                return
            if link is None:
                link = linkTo.linkedBy()
            else:
                link.merge(linkTo.linkedBy())
        if link is None:
            link = XoverFilterLink()
        self._filter.setLinkedBy(link)
        linkTo.setLinkedBy(link)
        link.addLinkedFilter(linkTo)
        link.addLinkedFilter(self._filter)
        self._dirty = True
        self.initListWidget()


    def onClose(self):
        self.close()


    def onDelete(self):
        item = self.listWidget.currentItem()
        if item is None:
            return
        msg = "Do you want to delete the link to '%s'?" % item.text()
        reply = QMessageBox().question(self, 'Really delete?',
                                       msg,
                                       QMessageBox.No | QMessageBox.Yes,
                                       QMessageBox.Yes)
        if reply == QMessageBox.Yes:
            link = self._filter.linkedBy()
            previousFilters = list(link.allLinkedFilters())
            filterToRemove = self._name2filter[item.text()]
            link.removeLinkedFilter(filterToRemove)
            for flt in previousFilters:
                if flt not in link.allLinkedFilters():
                    flt.setLinkedBy(None)
            self._dirty = True
            self.initListWidget()


    @staticmethod
    def onHelp():
        url = QUrl('https://t-5.eu/hp/Software/Pulseaudio%20Crossover%20Rack/OnlineHelp/#parameter_linking')
        QDesktopServices().openUrl(url)
