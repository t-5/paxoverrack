import math
import os
import time

import numpy
from PyQt5.QtCore import QTimer, QUrl, Qt
from PyQt5.QtGui import QDesktopServices
from PyQt5.QtWidgets import QDialog, QFileDialog, QMessageBox, QSizePolicy
from designer_qt5.Ui_FrdComparisonDialog import Ui_FrdComparisonDialog
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from qt5_t5darkstyle import darkstyle_css
try:
    from scipy.signal import hilbert
except ImportError:
    hilbert = None # import errors are handled in MainWindow.py

from WindowClasses.EditFilterDialog import EditFilterDialog
from helpers.constants import GRAPH_FREQUENCIES, MPLFIGURE_LEFTRIGHTMARGIN, MPLFIGURE_TOPMARGIN, MPLFIGURE_BOTTOMMARGIN
from helpers.functions import GRAPH_SR, dbToGainFactor, keepPhaseBounded, parse_frd_file, setupPltFigure


class FrdComparisonDialog(QDialog, Ui_FrdComparisonDialog):
    def __init__(self, mainWindow, xoverFilter):
        super(FrdComparisonDialog, self).__init__()
        self.setupUi(self)
        self.setWindowFlags(Qt.WindowCloseButtonHint | Qt.WindowMinimizeButtonHint | Qt.WindowMaximizeButtonHint)
        self.setStyleSheet(darkstyle_css())
        self._dlgPath = ""
        self._broadcastLocalChanges = True
        self._editFilterDialog = None
        self._frdFilename = xoverFilter.frdFilename()
        self._frdComplexAdjusted = numpy.ones(len(GRAPH_FREQUENCIES))
        self._frdMagnitudesDb = numpy.zeros(len(GRAPH_FREQUENCIES))
        self._frdPhaseDegrees = numpy.zeros(len(GRAPH_FREQUENCIES))
        self._frequencies = numpy.asarray(GRAPH_FREQUENCIES) / math.pi / 2 * GRAPH_SR[0]
        self._summedComplexes = numpy.zeros(len(GRAPH_FREQUENCIES))
        self._lastGraphUpdate = 0
        self._mainWindow = mainWindow
        self._mplCanvas = None
        self._mplFigure = None
        self._mplCanvasIsSetup = False
        self._nodata = numpy.full(len(GRAPH_FREQUENCIES), -1000000)
        self._summedPhase = numpy.zeros(len(GRAPH_FREQUENCIES))
        self._summedMagnitude = numpy.ones(len(GRAPH_FREQUENCIES))
        self._xoverFilter = xoverFilter
        # settings
        self._settings = mainWindow.settings()
        self._readIniSettings()
        ########self.verticalLayout.insertWidget(2, self._mplCanvas)
        # connect actions
        self.checkBoxIgnoreFrdPhase.stateChanged.connect(self._recalculateComplexResponse)
        self.checkBoxKeepSynced.stateChanged.connect(self.onCheckboxModified)
        self.checkBoxShowSummedFrequencyResponse.stateChanged.connect(self.onCheckboxModified)
        self.checkBoxShowSummedPhaseResponse.stateChanged.connect(self.onCheckboxModified)
        self.checkBoxShowFilterFrequencyResponse.stateChanged.connect(self.onCheckboxModified)
        self.checkBoxShowFrdFrequencyResponse.stateChanged.connect(self.onCheckboxModified)
        self.radioButtonShowLocal.toggled.connect(self.onCheckboxModified)
        self.radioButtonShowAggregated.toggled.connect(self.onCheckboxModified)
        self.spinBoxAdjustPhase.valueChanged.connect(self._recalculateComplexResponse)
        self.spinBoxAdjustResponse.valueChanged.connect(self._recalculateComplexResponse)
        self.toolButtonClose.clicked.connect(self.onClose)
        self.toolButtonEditFilter.clicked.connect(self.onEditFilter)
        self.toolButtonHelp.clicked.connect(self.onHelp)
        self.toolButtonOpen.clicked.connect(self.onOpen)
        # setup update ui timer
        self._graphUpdateInProgress = False
        self._timerUpdateUi = QTimer(self)
        self._timerUpdateUi.setInterval(100)
        # noinspection PyUnresolvedReferences
        self._timerUpdateUi.timeout.connect(self._updateGraph)
        self._timerUpdateUi.start()
        if self._frdFilename != "":
            self._readFrdFile()
        self.setWindowFlags(Qt.Window |
                            Qt.CustomizeWindowHint |
                            Qt.WindowTitleHint |
                            Qt.WindowCloseButtonHint |
                            Qt.WindowSystemMenuHint)


    def closeEvent(self, evt):
        if self._editFilterDialog is not None:
            self._editFilterDialog.close()
            self._editFilterDialog.deleteLater()
        self._writeIniSettings()
        self._mainWindow.delFrdDialog(self)
        self._xoverFilter.widget().resetFrdDialog()
        super().closeEvent(evt)


    def frequencies(self):
        return self._frequencies


    def lastGraphUpdate(self):
        return self._lastGraphUpdate


    def onCheckboxModified(self):
        if not self._broadcastLocalChanges:
            return
        keepSynced = self.checkBoxKeepSynced.isChecked()
        for dlg in self._mainWindow.frdDialogs():
            if dlg is self:
                continue
            dlg.setKeepAdjustResponseSynced(keepSynced)
        self._lastGraphUpdate = 0

    def onClose(self):
        self.close()


    def onEditFilter(self):
        if self._editFilterDialog is None:
            self._editFilterDialog = EditFilterDialog(self._mainWindow, self._xoverFilter)
            self._editFilterDialog.setModal(False)
        self._editFilterDialog.show()
        self._editFilterDialog.raise_()
        self._editFilterDialog.activateWindow()
        self._mainWindow.markDirty()


    @staticmethod
    def onHelp():
        url = QUrl('https://t-5.eu/hp/Software/Pulseaudio%20Crossover%20Rack/OnlineHelp/#frd_comparison')
        QDesktopServices().openUrl(url)


    def onOpen(self):
        dlg = QFileDialog()
        dlg.setStyleSheet(self.styleSheet())
        fn, ignore = dlg.getOpenFileName(self, 'Open a .FRD file...', self._dlgPath, 'FRD (*.frd);;TXT (*.txt);;CAL (*.cal);;')
        if fn == "":
            return
        self._frdFilename = fn
        self._xoverFilter.setFrdFilename(fn)
        self._mainWindow.markDirty()
        self._dlgPath = os.path.dirname(fn)
        self._readFrdFile()


    def resizeEvent(self, evt):
        if self._mplCanvas is None:
            return
        w = self._mplCanvas.width()
        h = self._mplCanvas.height()
        l = MPLFIGURE_LEFTRIGHTMARGIN / w
        r = 1 - l
        t = 1 - (MPLFIGURE_TOPMARGIN / h)
        b = MPLFIGURE_BOTTOMMARGIN / h
        self._mplFigure.subplots_adjust(left=l, right=r, top=t, bottom=b)
        if evt is not None:
            super().resizeEvent(evt)


    def setAdjustResponseByDb(self, db):
        self._broadcastLocalChanges = False
        self.spinBoxAdjustResponse.setValue(db)
        self._broadcastLocalChanges = True


    def setKeepAdjustResponseSynced(self, b):
        self._broadcastLocalChanges = False
        self.checkBoxKeepSynced.setChecked(b)
        self._broadcastLocalChanges = True


    def summedComplexes(self):
        return self._summedComplexes


    def xoverFilter(self):
        return self._xoverFilter


    def _calculateFrdComplex(self):
        self._frdComplexAdjusted = numpy.full(len(self._frequencies), 1j)
        if self.checkBoxIgnoreFrdPhase.isChecked():
            phaseRadList = -numpy.imag(hilbert(numpy.log(10 ** (self._frdMagnitudesDb / 20))))
        else:
            phaseRadList = self._frdPhaseDegrees / 180 * math.pi
        phaseRadList = phaseRadList + (self.spinBoxAdjustPhase.value() / 180 * math.pi)
        adjustByDb = self.spinBoxAdjustResponse.value()
        for idx, db in enumerate(self._frdMagnitudesDb):
            complex_ = dbToGainFactor(self._frdMagnitudesDb[idx] + adjustByDb) * math.e ** (1j * phaseRadList[idx])
            self._frdComplexAdjusted[idx] = complex_
        self._frdComplexAdjusted = numpy.asarray(self._frdComplexAdjusted)


    def _readFrdFile(self):
        self.labelFrdFile.setText(self._frdFilename)
        frdContents, errors = parse_frd_file(self._frdFilename)
        if errors:
            dlg = QMessageBox()
            dlg.setStyleSheet(self.styleSheet())
            msg = "ERROR:\n%s" % errors
            dlg.critical(self, "Errors reading FRD file:", msg)
            return
        # break out contents of frd file into separate variables
        self._frequencies = numpy.asarray(list(map(lambda x: x[0], frdContents)))
        self._frdMagnitudesDb = numpy.asarray(list(map(lambda x: x[1], frdContents)))
        self._frdPhaseDegrees = numpy.asarray(list(map(lambda x: x[2], frdContents)))
        # adjust frd magnitudes to max 0db
        # noinspection PyArgumentList
        self.spinBoxAdjustResponse.setValue(self._frdMagnitudesDb.max() * -1.0)
        # calculate complex responses
        self._calculateFrdComplex()
        # reset graph setup
        self._mplCanvasIsSetup = False
        self._lastGraphUpdate = 0


    def _readIniSettings(self):
        s = self._settings
        # dialog geometry and settings
        try:
            self.restoreGeometry(s.value("frdDialog/geometry", None))
        except (TypeError, ValueError):
            pass
        # dlgPath
        self._dlgPath = self._settings.value("frdDialog/dlgPath", os.path.expanduser("~"))
        # checkbox and radiobutton settings
        shownFilterResponse = s.value("frdDialog/shownFilterResponse", "local")
        if shownFilterResponse not in ("local", "aggregated"):
            shownFilterResponse = "local"
        if shownFilterResponse == "local":
            self.radioButtonShowLocal.setChecked(True)
        else:
            self.radioButtonShowAggregated.setChecked(True)
        self.checkBoxKeepSynced.setChecked(
            s.value("frdDialog/keepSynced", "true") in (True, "true"))
        self.checkBoxShowFilterFrequencyResponse.setChecked(
            s.value("frdDialog/showFilterFrequencyResponse", "true") in (True, "true"))
        self.checkBoxShowFrdFrequencyResponse.setChecked(
            s.value("frdDialog/showFrdFrequencyResponse", "true") in (True, "true"))
        self.checkBoxShowSummedFrequencyResponse.setChecked(
            s.value("frdDialog/showSummedFrequencyResponse", "true") in (True, "true"))
        self.checkBoxShowSummedPhaseResponse.setChecked(
            s.value("frdDialog/showSummedPhaseResponse", "true") in (True, "true"))


    def _recalculateComplexResponse(self):
        if self.checkBoxKeepSynced.isChecked() and self._broadcastLocalChanges:
            for dlg in self._mainWindow.frdDialogs():
                if dlg is self:
                    continue
                dlg.setAdjustResponseByDb(self.spinBoxAdjustResponse.value())
        self._calculateFrdComplex()
        self._lastGraphUpdate = 0


    def _setupMplCanvas(self, w):
        # remove old matplotlib canvas or placeholder from vertical layout
        if self._mplCanvas is None:
            try:
                self.widgetReplacedByFrGraph.hide()
                self.verticalLayout.removeWidget(self.widgetReplacedByFrGraph)
                self.widgetReplacedByFrGraph.deleteLater()
            except RuntimeError:
                pass
        else:
            self._mplCanvas.hide()
            self.verticalLayout.removeWidget(self._mplCanvas)
            self._mplCanvas.deleteLater()
        # setup new matplotlib canvas and figure
        self._mplFigure = Figure()
        self._mplCanvas = FigureCanvas(self._mplFigure)
        self._mplCanvas.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.verticalLayout.insertWidget(3, self._mplCanvas)
        self._pltLine1, self._pltAx1, self._pltLine2, self._pltAx2 = \
            setupPltFigure(w, self._nodata, self._mplCanvas, self._mplFigure,
                           -30, 10, -200, 200, phaseLineThickness=1.0)
        self._pltLine3, = self._pltAx1.plot(w, self._nodata, '#0000ff', linewidth=2.0, zorder=5)
        self._pltLine4, = self._pltAx1.plot(w, self._nodata, '#00ffff', linewidth=2.0, zorder=5)
        self._mplCanvas.setMinimumHeight(100)
        self._mplFigure.subplots_adjust(left=0.06, right=0.94, top=0.98, bottom=0.07)


    def _updateGraph(self):
        if self._graphUpdateInProgress:
            return
        if self.isHidden():
            return
        if self._lastGraphUpdate >= self._xoverFilter.lastModified():
            return
        self._graphUpdateInProgress = True
        self.setWindowTitle("%s - PaXoverRack FRD Dialog" % self._xoverFilter.sinkName())
        # show local or aggregated frequency response?
        if self.radioButtonShowLocal.isChecked():
            w, self._filterComplex = self._xoverFilter.frequencyResponse(frequencies=self._frequencies * 2 * math.pi / GRAPH_SR[0])
        else:
            w, self._filterComplex = self._xoverFilter.aggregatedFrequencyResponse(frequencies=self._frequencies * 2 * math.pi / GRAPH_SR[0])
        try:
            self._summedComplexes = self._frdComplexAdjusted * self._filterComplex
        except ValueError: # occurrs, if no frd file read yet
            self._summedComplexes = self._filterComplex
        # do initial graph setup?
        if not self._mplCanvasIsSetup:
            # prepare new "nodata" array, as length of frequencies might have changed
            self._nodata = numpy.full(len(self._frequencies), -1000000)
            # setup the new matplotlib canvas
            self._setupMplCanvas(self._frequencies)
            self._mplCanvasIsSetup = True
        # fill y data, filter frequency response
        if self.checkBoxShowFilterFrequencyResponse.isChecked():
            self._pltLine1.set_ydata(20 * numpy.log10(abs(self._filterComplex)))
        else:
            self._pltLine1.set_ydata(self._nodata)
        # fill y data, summed phase response
        if self.checkBoxShowSummedPhaseResponse.isChecked():
            self._pltLine2.set_ydata(keepPhaseBounded(numpy.angle(self._summedComplexes)) / math.pi * 180)
        else:
            self._pltLine2.set_ydata(self._nodata)
        # fill y data, filter frequency response
        if self.checkBoxShowFrdFrequencyResponse.isChecked():
            self._pltLine3.set_ydata(20 * numpy.log10(abs(self._frdComplexAdjusted)))
        else:
            self._pltLine3.set_ydata(self._nodata)
        # fill y data, filter frequency response
        if self.checkBoxShowSummedFrequencyResponse.isChecked():
            self._pltLine4.set_ydata(20 * numpy.log10(abs(self._summedComplexes)))
        else:
            self._pltLine4.set_ydata(self._nodata)
        self._mplCanvas.draw()
        self._lastGraphUpdate = time.time()
        self._graphUpdateInProgress = False


    def _writeIniSettings(self):
        s = self._settings
        # dialog geometry and settings
        if self.isVisible():
            s.setValue("frdDialog/geometry", self.saveGeometry())
        # dlgPath
        s.setValue("frdDialog/dlgPath", self._dlgPath)
        # checkbox and radiobutton settings
        s.setValue("frdDialog/keepSynced", self.checkBoxKeepSynced.isChecked())
        s.setValue("frdDialog/shownFilterResponse", self.radioButtonShowLocal.isChecked() and "local" or "aggregated")
        s.setValue("frdDialog/showFilterFrequencyResponse", self.checkBoxShowFilterFrequencyResponse.isChecked())
        s.setValue("frdDialog/showFrdFrequencyResponse", self.checkBoxShowFrdFrequencyResponse.isChecked())
        s.setValue("frdDialog/showSummedFrequencyResponse", self.checkBoxShowSummedFrequencyResponse.isChecked())
        s.setValue("frdDialog/showSummedPhaseResponse", self.checkBoxShowSummedPhaseResponse.isChecked())
