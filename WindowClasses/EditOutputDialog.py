from PyQt5.QtWidgets import QDialog, QMessageBox
from designer_qt5.Ui_EditOutputDialog import Ui_EditOutputDialog

from DataClasses import XoverOutput
from helpers.functions import pulse_get_sink_channels, pulse_get_hw_sinks


class EditOutputDialog(QDialog, Ui_EditOutputDialog):
    def __init__(self, parent, output: XoverOutput, newlyCreated=False):
        super().__init__(parent=parent)
        self.setupUi(self)
        self._output = output
        self._newlyCreated = newlyCreated
        # get hw output sinks and populate combo box
        self._hw_sinks = pulse_get_hw_sinks()
        for sinkDict in self._hw_sinks:
            self.comboBoxOutputSink.addItem(sinkDict["description"])
        for i in range(0, len(self._hw_sinks)):
            if self._output.outputSinkName() == self._hw_sinks[i]["name"]:
                self.comboBoxOutputSink.setCurrentIndex(i)
                break
        # newly created?
        if newlyCreated:
            self.setWindowTitle("Add new Output...")
            self.lineEditSinkName.setFocus()
            # find unique sink name for output
            oldname = "Output"
            name = oldname
            i = 0
            while parent.nameExists(name):
                i += 1
                name = oldname + str(i)
            output.setSinkName(name)
            self.lineEditSinkName.setText(name)
        else:
            self.setWindowTitle("Edit Output '%s'" % self._output.sinkName())
            self.lineEditSinkName.setText(self._output.sinkName())
            self.checkBoxMono.setChecked(self._output.mono())
            self.checkBoxSwapChannels.setChecked(self._output.swapChannels())
        # finally setup event listeners
        self.comboBoxOutputSink.currentIndexChanged.connect(self.onOutputSinkChanged)
        self.onOutputSinkChanged()


    def accept(self):
        name = self.lineEditSinkName.text()
        if not self.parent().isUniqueName(name, exclude=self._output):
            QMessageBox().critical(self, "ERROR", "The name '%s' is not unique!" % name)
            return -1
        checkResult = self.parent().checkSinkName(name)
        if checkResult:
            msg = "The name '%s' has errors:\n" % name
            QMessageBox().critical(self, "ERROR", msg + checkResult)
            return -2
        channels = self._selectedChannels()
        if len(channels) == 1 and not self.checkBoxMono.isChecked():
            msg = "You have selected ony one channel, do you want to downmix to mono?"
            ret = QMessageBox().question(self, "Downmix to mono?", msg, QMessageBox.Yes | QMessageBox.No)
            if ret != QMessageBox.Yes:
                return -3
            self.checkBoxMono.setChecked(True)
        elif len(channels) not in (1, 2):
            msg = "You have to select one (1) or two (2) output channels!"
            QMessageBox().critical(self, "ERROR", msg)
            return -4
        self._output.setSinkName(name)
        outputSinkDescription = self.comboBoxOutputSink.currentText()
        outputSinkName = ""
        for sinkDict in self._hw_sinks:
            if sinkDict["description"] == outputSinkDescription:
                outputSinkName = sinkDict["name"]
                break
        self._output.setOutputSinkDescription(outputSinkDescription)
        self._output.setOutputSinkName(outputSinkName)
        self._output.setOutputSinkChannels(channels)
        if self._newlyCreated:
            self._output.restoreWidget(self.parent().drawArea())
            self.parent().xoverFileAddObject(self._output)
        self._output.setMono(self.checkBoxMono.isChecked())
        self._output.setSwapChannels(self.checkBoxSwapChannels.isChecked())
        self.close()


    def onOutputSinkChanged(self):
        outputSinkName = ""
        for sinkDict in self._hw_sinks:
            if sinkDict["description"] == self.comboBoxOutputSink.currentText():
                outputSinkName = sinkDict["name"]
                break
        channels = pulse_get_sink_channels(outputSinkName)
        self.listWidgetOutputChannels.clear()
        self.listWidgetOutputChannels.addItems(channels)
        try:
            ch1 = self._output.outputSinkChannels()[0]
        except IndexError:
            ch1 = None
        try:
            ch2 = self._output.outputSinkChannels()[1]
        except IndexError:
            ch2 = None
        foundChannel = False
        if ch1 in channels:
            self.listWidgetOutputChannels.item(channels.index(ch1)).setSelected(True)
            foundChannel = True
        if ch2 in channels:
            self.listWidgetOutputChannels.item(channels.index(ch2)).setSelected(True)
            foundChannel = True
        if not foundChannel:
            for idx in range(0, min(self.listWidgetOutputChannels.count(), 2)):
                self.listWidgetOutputChannels.item(idx).setSelected(True)


    def _selectedChannels(self):
        channels = []
        i = 0
        item = 1
        while item is not None:
            item = self.listWidgetOutputChannels.item(i)
            if item and item.isSelected():
                channels.append(item.text())
            i += 1
        return channels