import numpy
from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtWidgets import QDialog
from qt5_t5darkstyle import darkstyle_css
from helpers.constants import INPUT_CHANNEL_MAP
from helpers.functions import gainFactorToDb, GRAPH_SR
from helpers.threads import AbstractSoundcardThread, SoundcardPeakDetectorThread


class AbstractMeasurementDialog(QDialog):


    signal_processCapturedSamples = pyqtSignal(numpy.ndarray)
    signal_updateVuMeters = pyqtSignal(numpy.ndarray)
    signal_removeThread = pyqtSignal(AbstractSoundcardThread)


    def __init__(self, mainWindow):
        super().__init__()
        self.setupUi(self)
        self.setWindowFlags(Qt.WindowCloseButtonHint | Qt.WindowMinimizeButtonHint | Qt.WindowMaximizeButtonHint)
        self.setStyleSheet(darkstyle_css())
        self._mainWindow = mainWindow
        self._settings = mainWindow.settings()
        # capture & playback threads
        self._threadList = [] # holds references to stopped capture threads until their finished() signal arrives
        self._playbackCaptureThread = None
        self._vuCaptureThread = None
        # capture data container, fill with 1s of silence for startup
        self._capturedData = numpy.array([numpy.zeros(GRAPH_SR[0])] * 2).transpose()
        self._samplesCaptured = 0
        self._samplesPlayed = 0


    def closeEvent(self, evt):
        if self._vuCaptureThread is not None:
            self._vuCaptureThread.stopAndWait()
        self._writeIniSettings()
        super().closeEvent(evt)


    def hideEvent(self, event):
        if self._vuCaptureThread is not None:
            self._vuCaptureThread.stopAndWait()


    def onClose(self):
        self.close()


    @staticmethod
    def onHelp():
        raise NotImplementedError


    def onLevelCheck(self):
        raise NotImplementedError


    def onStartMeasurement(self):
        raise NotImplementedError


    def settings(self):
        return self._mainWindow.settings()


    def showEvent(self, event):
        self._readIniSettings()
        self._vuCaptureThread = SoundcardPeakDetectorThread(GRAPH_SR[0], self.signal_updateVuMeters)
        self._startThread(self._vuCaptureThread)


    def _connectSignals(self):
        # noinspection PyUnresolvedReferences
        self.signal_processCapturedSamples.connect(self._processCapturedSamples)
        # noinspection PyUnresolvedReferences
        self.signal_updateVuMeters.connect(self._updateVuMeters)
        self.toolButtonExit.clicked.connect(self.onClose)
        self.toolButtonHelp.clicked.connect(self.onHelp)
        self.toolButtonLevelCheck.clicked.connect(self.onLevelCheck)
        self.toolButtonStartMeasurement.clicked.connect(self.onStartMeasurement)


    def _populateInputChannels(self, *_, preselectedLoopback=None, preselectedMicrophone=None):
        if preselectedLoopback is None:
            preselectedLoopback = self.comboBoxInputChannelLoopback.currentText()
        if preselectedMicrophone is None:
            preselectedMicrophone = self.comboBoxInputChannelMicrophone.currentText()
        self.comboBoxInputChannelLoopback.clear()
        self.comboBoxInputChannelMicrophone.clear()
        for name, num in INPUT_CHANNEL_MAP.items():
            self.comboBoxInputChannelLoopback.addItem(name, userData=num)
            if preselectedLoopback == name:
                self.comboBoxInputChannelLoopback.setCurrentIndex(num)
            self.comboBoxInputChannelMicrophone.addItem(name, userData=num)
            if preselectedMicrophone == name:
                self.comboBoxInputChannelMicrophone.setCurrentIndex(num)


    def _processCapturedSamples(self, data):
        self._samplesCaptured = len(data)
        self._capturedData = self._transpose(data)


    def _removeThread(self):
        thread = self.sender()
        while thread in self._threadList:
            self._threadList.remove(thread)


    def _startThread(self, thread):
        self._threadList.append(thread)
        thread.setFinishedSignal(self.signal_removeThread)
        thread.start()


    def _transpose(self, captureData):
        transposed = captureData.transpose()
        im = INPUT_CHANNEL_MAP.get(self.comboBoxInputChannelMicrophone.currentText(), 0)
        il = INPUT_CHANNEL_MAP.get(self.comboBoxInputChannelLoopback.currentText(), 1)
        return transposed[im], transposed[il]


    def _updateVuMeters(self, captureData):
        microphone, loopback = self._transpose(captureData)
        if microphone.size:
            self.levelMeterMicrophone.levelChanged(gainFactorToDb(max(abs(microphone))))
        if loopback.size:
            self.levelMeterLoopback.levelChanged(gainFactorToDb(max(abs(loopback))))
