pulseaudio-crossover-rack (1.89) stable; urgency=medium
  *  feature:
     - fix module installation via pip3 for python versions >= 3.11
 -- Jürgen Herrmann <t-5@t-5.eu>  Sun, 18 Aug 2023 18:11:50 +0200
pulseaudio-crossover-rack (1.88) stable; urgency=medium
  *  feature:
     - fix module installation via pip3 for python versions >= 3.12
 -- Jürgen Herrmann <t-5@t-5.eu>  Sun, 18 Aug 2023 15:42:42 +0200
pulseaudio-crossover-rack (1.87) stable; urgency=medium
  *  bugfix:
     - detect buffer underrun when recording in TaMeasurementDialog
 -- Jürgen Herrmann <t-5@t-5.eu>  Sat, 13 May 2023 21:57:55 +0200
pulseaudio-crossover-rack (1.86) stable; urgency=high
  *  bugfix:
     - require libatlas-base-dev and libfftw3-dev
 -- Jürgen Herrmann <t-5@t-5.eu>  Sun, 02 Apr 2023 22:38:55 +0200
pulseaudio-crossover-rack (1.83) stable; urgency=high
  *  bugfix:
     - fixed float vs int bugs in time alignment measurement dialog
 -- Jürgen Herrmann <t-5@t-5.eu>  Fri, 20 Jan 2023 01:19:28 +0200
pulseaudio-crossover-rack (1.82) stable; urgency=high
  *  bugfix:
     - fixed bug discovered on manjaro with python 3.10
 -- Jürgen Herrmann <t-5@t-5.eu>  Fri, 19 May 2022 21:06:26 +0200
pulseaudio-crossover-rack (1.81) stable; urgency=high
  *  bugfix:
     - fixed several locations where float values were given to PyQT
       calls and floats were expected.
 -- Jürgen Herrmann <t-5@t-5.eu>  Fri, 19 May 2022 21:06:26 +0200
pulseaudio-crossover-rack (1.80) stable; urgency=high
  *  bugfix:
     - fixed a typo that prevented the insertion of
       "Second Order Butterworth Lowpass" filters
 -- Jürgen Herrmann <t-5@t-5.eu>  Sun, 17 Apr 2022 22:38:01 +0200
pulseaudio-crossover-rack (1.78) stable; urgency=high
  *  feature:
     - make the "modules inserted" check much more terse
 -- Jürgen Herrmann <t-5@t-5.eu>  Wed, 13 Apr 2022 21:01:01 +0200
pulseaudio-crossover-rack (1.77) stable; urgency=low
  *  feature:
     - if no file is given on the command line search for loaded filename
       in ~/.config/pulse/default.pa and load it if file still exists.
 -- Jürgen Herrmann <t-5@t-5.eu>  Wed, 09 Apr 2022 19:35:06 +0200
pulseaudio-crossover-rack (1.76) stable; urgency=high
  *  fixed version check for scipy/signal and installation via pip3
 -- Jürgen Herrmann <t-5@t-5.eu>  Wed, 06 Apr 2022 17:48:45 +0200
pulseaudio-crossover-rack (1.75) stable; urgency=high
  *  fixed version check for scipy and installation via pip if version < 1.19
 -- Jürgen Herrmann <t-5@t-5.eu>  Wed, 06 Apr 2022 17:43:37 +0200
pulseaudio-crossover-rack (1.74) stable; urgency=high
  *  fixed version check for scipy and installation via pip if version < 1.19
 -- Jürgen Herrmann <t-5@t-5.eu>  Wed, 06 Apr 2022 17:24:10 +0200
pulseaudio-crossover-rack (1.73) stable; urgency=high
  *  fixed version check for scipy and installation via pip if version < 1.19
 -- Jürgen Herrmann <t-5@t-5.eu>  Wed, 06 Apr 2022 17:17:22 +0200
pulseaudio-crossover-rack (1.72) stable; urgency=high
  *  added version check for scipy and installation via pip if version < 1.19
 -- Jürgen Herrmann <t-5@t-5.eu>  Wed, 06 Apr 2022 17:01:05 +0200
pulseaudio-crossover-rack (1.71) stable; urgency=high
  *  added import check/install for pyfftw via pip3
 -- Jürgen Herrmann <t-5@t-5.eu>  Wed, 06 Apr 2022 17:01:05 +0200
pulseaudio-crossover-rack (1.70) stable; urgency=medium
  *  removed dependency on python3-pyfftw
 -- Jürgen Herrmann <t-5@t-5.eu>  Wed, 06 Apr 2022 16:58:23 +0200
pulseaudio-crossover-rack (1.69) stable; urgency=medium
  *  fixed help url
 -- Jürgen Herrmann <t-5@t-5.eu>  Wed, 06 Apr 2022 21:06:47 +0200
pulseaudio-crossover-rack (1.68) stable; urgency=low
  *  unimportant release that fixes the shown version number
 -- Jürgen Herrmann <t-5@t-5.eu>  Wed, 06 Apr 2022 20:50:07 +0200
pulseaudio-crossover-rack (1.67) stable; urgency=high
  *  bugfix:
     - added missing dependency libportaudio2
 -- Jürgen Herrmann <t-5@t-5.eu>  Wed, 05 Apr 2022 23:56:10 +0200
pulseaudio-crossover-rack (1.66) stable; urgency=high
  *  bugfix:
     - added missing dependency python3-qt5-levelmeter
 -- Jürgen Herrmann <t-5@t-5.eu>  Wed, 05 Apr 2022 23:47:22 +0200
pulseaudio-crossover-rack (1.65) stable; urgency=medium
  *  bugfix:
     - remove unnecessary import from __future__ to reestablish compatibility
       with python < 3.7
  * feature:
     - added time alignment measurement dialog
 -- Jürgen Herrmann <t-5@t-5.eu>  Wed, 05 Apr 2022 23:33:52 +0200
pulseaudio-crossover-rack (1.64) stable; urgency=medium
  *  bugfix:
     - fix redraw bug after deleting OutputWidgets that also prevented saving
     - fix bug that prevented saving because of missing method in XoverOutput
  * feature:
     - remove pretty much senseless nag dialog when saving a file asking if
       you also want to persist changes to the default.pa file when the xover
       modules were inserted already. Why wouldn't you?
 -- Jürgen Herrmann <t-5@t-5.eu>  Wed, 09 Mar 2022 01:03:38 +0200
pulseaudio-crossover-rack (1.63) stable; urgency=medium
  *  bugfix:
     - fix redraw error in global frd dialog when there is no frd dialog
     - fix window flags for frd dialog windows
 -- Jürgen Herrmann <t-5@t-5.eu>  Mon, 21 Feb 2022 22:56:21 +0200
pulseaudio-crossover-rack (1.62) stable; urgency=low
  *  bugfix:
     - remove broken ICC color profiles from pngs to prevent startup warning
 -- Jürgen Herrmann <t-5@t-5.eu>  Mon, 21 Feb 2022 22:31:49 +0200
pulseaudio-crossover-rack (1.61) stable; urgency=medium
  *  bugfix:
     - deleted filter objects did hang around in the database if
       they were linked to another filter.
 -- Jürgen Herrmann <t-5@t-5.eu>  Sat, 05 Feb 2022 20:10:18 +0200
pulseaudio-crossover-rack (1.60) stable; urgency=medium
  *  bugfix:
     - set sink volumes to 0 in appropriate places while (re)inserting
       modules to prevent loud glitches while playing music.
 -- Jürgen Herrmann <t-5@t-5.eu>  Tue, 01 Feb 2022 22:21:38 +0200
pulseaudio-crossover-rack (1.59) stable; urgency=medium
  *  bugfix:
     - added set-default-sink statement to default.pa, needed by newer
       pulseaudio versions (>= 13.99 afaik)
 -- Jürgen Herrmann <t-5@t-5.eu>  Mon, 31 Jan 2022 02:19:30 +0200
pulseaudio-crossover-rack (1.58) stable; urgency=medium
  *  bugfix:
     - fixed dialog that asks for unsaved file at main window close time
  * features:
     - set sink volumes to zero when (re)inserting modules
     - prevent insertion when there are unconnected objects
 -- Jürgen Herrmann <t-5@t-5.eu>  Mon, 31 Jan 2022 01:33:04 +0200
pulseaudio-crossover-rack (1.57) stable; urgency=medium
  *  bugfix:
     - fixed argparse options and help text
 -- Jürgen Herrmann <t-5@t-5.eu>  Thu, 29 Oct 2020 10:36:04 +0200
pulseaudio-crossover-rack (1.56) stable; urgency=low
  *  bugfix/feature:
     - enable using two sound cards with identical sink descriptions
 -- Jürgen Herrmann <t-5@t-5.eu>  Thu, 29 Oct 2020 10:36:04 +0200
pulseaudio-crossover-rack (1.55) stable; urgency=low
  *  bugfix/feature:
     - enable using two sound cards with identical sink descriptions
 -- Jürgen Herrmann <t-5@t-5.eu>  Thu, 29 Oct 2020 10:36:04 +0200
pulseaudio-crossover-rack (1.54) stable; urgency=low
  *  bugfix/feature:
     - enable using two sound cards with identical sink descriptions
 -- Jürgen Herrmann <t-5@t-5.eu>  Thu, 29 Oct 2020 10:22:04 +0200
pulseaudio-crossover-rack (1.53) stable; urgency=low
  *  bugfix:
     - properly parse module names while unloading
 -- Jürgen Herrmann <t-5@t-5.eu>  Fri, 20 Oct 2020 21:14:53 +0200
pulseaudio-crossover-rack (1.52) stable; urgency=low
  *  compatibility fix:
     - as pactl just crashes on ubuntu/mint > 20, use pacmd instead.
 -- Jürgen Herrmann <t-5@t-5.eu>  Tue, 20 Oct 2020 21:01:01 +0200
pulseaudio-crossover-rack (1.51) stable; urgency=low
  *  feature:
     - added --apply flag
 -- Jürgen Herrmann <t-5@t-5.eu>  Fri, 22 May 2019 02:56:59 +0200
pulseaudio-crossover-rack (1.47) stable; urgency=low
  *  bugfix:
     - added dependency on python3-wheel
 -- Jürgen Herrmann <t-5@t-5.eu>  Sat, 03 Aug 2019 07:03:42 +0200
pulseaudio-crossover-rack (1.46) stable; urgency=high
  *  bugfix:
     - fixed output coloring scheme for good now
 -- Jürgen Herrmann <t-5@t-5.eu>  Sat, 03 Aug 2019 07:03:42 +0200
pulseaudio-crossover-rack (1.45) stable; urgency=high
  *  bugfix:
     - accidentally released the measurement_support branch. reverted. DOH! :/
 -- Jürgen Herrmann <t-5@t-5.eu>  Sat, 03 Aug 2019 05:03:42 +0200
pulseaudio-crossover-rack (1.44) stable; urgency=medium
  *  bugfix:
     - Allow uuid.SafeUUID in ZODB files. This fixes a bug which was observed
       first time on ubuntu 19.04/python 3.7.
 -- Jürgen Herrmann <t-5@t-5.eu>  Sat, 03 Aug 2019 05:00:42 +0200
pulseaudio-crossover-rack (1.43) stable; urgency=low
  *  bugfix:
     - only show "Edit links"-button in EditFilterDialog when not newly created.
 -- Jürgen Herrmann <t-5@t-5.eu>  Sat, 03 Aug 2019 01:40:50 +0200
pulseaudio-crossover-rack (1.42) stable; urgency=low
  *  bugfix:
     - removed unnecessary debug statement
 -- Jürgen Herrmann <t-5@t-5.eu>  Sun, 21 Jul 2019 23:12:50 +0200
pulseaudio-crossover-rack (1.41) stable; urgency=low
  *  bugfix:
     - frequency response gfx of outputs was shown incorrectly sized when opened
       in FrequencyResponseDialog
 -- Jürgen Herrmann <t-5@t-5.eu>  Sat, 13 Jul 2019 04:21:50 +0200
pulseaudio-crossover-rack (1.40) stable; urgency=low
  *  bugfix:
     - frequency response gfx of outputs was shown incorrectly sized when opened
       in FrequencyResponseDialog
 -- Jürgen Herrmann <t-5@t-5.eu>  Sat, 13 Jul 2019 04:13:50 +0200
pulseaudio-crossover-rack (1.39) stable; urgency=low
  *  bugfix:
     - unguarded import of PersistentList in XoverFilterLink prevented automatic
       installation of ZODB via pip
 -- Jürgen Herrmann <t-5@t-5.eu>  Thu, 04 Jul 2019 10:23:50 +0200
pulseaudio-crossover-rack (1.38) stable; urgency=low
  *  bugfix:
     - on modules insert set all filter sink volumes to 100%
       (had a bug on my laptop where a splitter sink somehow was set to -47.x dB)
 -- Jürgen Herrmann <t-5@t-5.eu>  Tue, 11 Jun 2019 00:03:50 +0200
pulseaudio-crossover-rack (1.37) stable; urgency=low
  *  bugfix:
     - actually read stored open file dialog path from config file (wasn't used up to now)
 -- Jürgen Herrmann <t-5@t-5.eu>  Fri, 12 Mar 2019 20:26:18 +0200
pulseaudio-crossover-rack (1.34) stable; urgency=low
  *  features:
     - added version check when opening files
 -- Jürgen Herrmann <t-5@t-5.eu>  Fri, 12 Mar 2019 17:26:39 +0200
pulseaudio-crossover-rack (1.33) stable; urgency=low
  *  features:
     - added Linkwitz Transform Filter
     - made filter linking possible from to many filters (required data model change)
 -- Jürgen Herrmann <t-5@t-5.eu>  Fri, 12 Mar 2019 16:47:05 +0200
pulseaudio-crossover-rack (1.29) stable; urgency=low
  *  bugfix:
     - output with swapped channels was not editable
 -- Jürgen Herrmann <t-5@t-5.eu> 2019-03-27
pulseaudio-crossover-rack (1.28) stable; urgency=low
  *  features:
     - allow swapping of left/right channels on outputs
 -- Jürgen Herrmann <t-5@t-5.eu> 2019-03-27
pulseaudio-crossover-rack (1.27) stable; urgency=low
  *  features:
     - allow adding a filter by just choosing it from the combobox and get rid of the "+" button
 -- Jürgen Herrmann <t-5@t-5.eu> 2019-03-26
pulseaudio-crossover-rack (1.26) stable; urgency=low
   *  bugfixes:
    - fixed 100% cpu usage bug when showing output's frequency response
-- Jürgen Herrmann <t-5@t-5.eu> 2019-03-19
pulseaudio-crossover-rack (1.25) stable; urgency=low
  *  bugfixes:
     - fixed some redraw bugs of matplotlib canvas
     - unified calling subprocesses and the handling of console encodings
 -- Jürgen Herrmann <t-5@t-5.eu> 2019-03-19
pulseaudio-crossover-rack (1.22) stable; urgency=low
  *  features:
     - shrink minimum height of main window
 -- Jürgen Herrmann <t-5@t-5.eu> 2019-02-27
pulseaudio-crossover-rack (1.21) stable; urgency=low
  *  features:
     - EditOutputDialog
       - find unique name for added output
     - EditFilterDialog
       - find unique name for added filter
       - make the add filter button the default action for the dialog
 -- Jürgen Herrmann <t-5@t-5.eu> 2019-01-27
pulseaudio-crossover-rack (1.20) stable; urgency=high
  *  security fixes:
     - Only allow certain specific classes while unpickling zodb stuff.
       Before the fix loading .paxor files from untrusted sources was
       not advisable as it opened the possibility of arbitrary code
       execution! This was brought to my attention a few days ago by a posting
       on the ZODB-dev mailing list.
  *  bugfixes:
     - Do not crash when soundcard reports < 2 outputs.
  *  features:
     - Made it possible to output downmixed mono audio to a single soundcard
       channel only.
     - using keepPhaseBounded() on all phase response graphs
 -- Jürgen Herrmann <t-5@t-5.eu> 2019-01-27
pulseaudio-crossover-rack (1.18) stable; urgency=low
  *  bugfix:
     - reverted dependency on pulseaudio>=11.0 to make it work on debian
       stable and raspbian again
 -- Jürgen Herrmann <t-5@t-5.eu> 2019-01-18
pulseaudio-crossover-rack (1.17) stable; urgency=medium
  *  bugfixes:
       - resized edit fields for filter parameters to accomodate new precision
 -- Jürgen Herrmann <t-5@t-5.eu> 2019-01-15
pulseaudio-crossover-rack (1.15) stable; urgency=medium
  *  features:
     - added checkbox to ignore phase values in FRD files and replace them
       by calculated phase responses (minimum phase model, hilbert transform)
     - added checkbox to sync up the dB values by which FRD responses get
       shifted towards 0dB
     - added spinBox to manually shift phase responses. this enables exploration
       of different phase shifts needed to gain proper summation at crossover
       frequencies. phase shifts then have to be converted to delays in the
       crossover filter chain.
  *  bugfixes:
     - switched internal data structures completely to complex representation
       of magnitude/phase responses. In the process a summation error was
       detected and fixed
 -- Jürgen Herrmann <t-5@t-5.eu> 2019-01-15
pulseaudio-crossover-rack (1.14) stable; urgency=medium
  *  bugfixes:
     - removed more debug statements
 -- Jürgen Herrmann <t-5@t-5.eu> 2019-01-14
pulseaudio-crossover-rack (1.13) stable; urgency=low
  *  features:
     - delays now show the offset in millimeters
  *  bugfixes:
     - removed formatter with 3 decimal places for float values (otherwise a
       precise delay value for example would be truncated)
     - removed some debug statements
     - reverted delay biquad filter coefficients to short form for
       performance reasons
 -- Jürgen Herrmann <t-5@t-5.eu> 2019-01-14
pulseaudio-crossover-rack (1.12) stable; urgency=low
  *  features:
     - added second order low-/highpass filters with variable Q
     - added dialog to load FRD frequency response files and look at the speaker
       response, both standalone and combined with the filter response
     - added dialog to show summed up responses of loaded FRD+filter responses
       plus their sums
       ATTENTION: This dialog does not yet account for phase shifts introduced
                  by delay filters, this is kind of complicated :/
     - changed all lower frequency parameter limits to 2Hz and all
       upper limits to 20kHz
     - made connection curves look nicer by using antialiasing
     - added dividers in EditFilterDialog to group parameters of ParamEq
     - added inverting filter
  *  bugfixes:
     - insert/remove modules threw an error when no file was loaded
 -- Jürgen Herrmann <t-5@t-5.eu> 2019-01-13
pulseaudio-crossover-rack (1.11) stable; urgency=medium
  *  bugfix: added pulseaudio-utils to the dependencies
 -- Jürgen Herrmann <t-5@t-5.eu> 2019-01-05
pulseaudio-crossover-rack (1.10) stable; urgency=low
  *  bugfix: changed matplotlib.backends.backend_qt4agg to backend_qt5agg
     which prevented running on a RPi3
 -- Jürgen Herrmann <t-5@t-5.eu> 2019-01-04
pulseaudio-crossover-rack (1.9) stable; urgency=low
  *  bugfixes:
     - there was a bug in displaying the frequency response of
       third order filters.
 -- Jürgen Herrmann <t-5@t-5.eu> 2018-12-20
pulseaudio-crossover-rack (1.8) stable; urgency=low
  *  bugfixes:
     - show correct number of active bands in parametric eq widget
     - fixed wrong ladspa plugin names for first order filters
     - made console window's textedit readonly
     - changed GRAPH_FREQS slightly to cover the bass region again
       even when sampling rates are very high
     - phase responses should be shown correctly for all low/high
       pass filters now
  *  features:
     - added second order and third order butterworth filters
     - removed feature to "auto-drag" added filter widgets
  *  internal:
     - added extra subclasses for low/high pass filters
 -- Jürgen Herrmann <t-5@t-5.eu> 2018-12-19
pulseaudio-crossover-rack (1.7) stable; urgency=low
  *  bugfixes:
     - reimplemented filter coefficients and frequency response calculation
       algorithms to match real LR-2 characteristics
     - fixed bug in filter add functionality where widget was None if cancel
       button was pressed
 -- Jürgen Herrmann <t-5@t-5.eu> 2018-11-29
pulseaudio-crossover-rack (1.6) stable; urgency=medium
  *  bugfix:
     there was a bug introduced in 1.5 which prevented opening files if
     no file was open yet.
 -- Jürgen Herrmann <t-5@t-5.eu> 2018-11-28
pulseaudio-crossover-rack (1.5) stable; urgency=low
  *  refactoring:
     moved color information for output channel markers from
     XoverOutput to the OutputWidget class where they belong.
  *  bugfix:
     Fixed wrong mmap filename hint in XoverSubsampleDelay which prevented
     realtime parametrization to work. Depends on a bugfix released in
     ladspa-t5-plugins version 1.2
 -- Jürgen Herrmann <t-5@t-5.eu> 2018-11-27
pulseaudio-crossover-rack (1.4) stable; urgency=low
  *  StatusBar:
     smaller font
     fixed sizes for dsp status, output status and inserted
     dsp status:
       show sr/format of pulseaudio server
     output status:
       show nothing if no output exists
       show sr/format of first output sink
  *  Fixed bug in MainWindow (closing _xoverFile to soon in onFileOpen)
  *  small code cleanup
 -- Jürgen Herrmann <t-5@t-5.eu> 2018-11-24
pulseaudio-crossover-rack (1.2) stable; urgency=low
  *  Packaging:
     added changelog for the first time
  *  XoverOutput:
     add mono summing (first feature request from diyaudio.com :)
  *  Widgets:
     when adding, start in drag&drop mode in the center of the window
  *  Widgets
     add color markings to output widgets according to surround color scheme
  *  MainWindow
     if filters are inserted and a file is saved, ask if default.pa should be rewritten
 -- Jürgen Herrmann <t-5@t-5.eu> 2018-11-22
