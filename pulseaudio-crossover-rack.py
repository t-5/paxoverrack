#!/usr/bin/python3

"""
PaXoverRack
===========
Pulseaudio Multi-Way Speaker Crossover Configurator

Main python (startup) file

GIT: https://gitlab.com/t-5/PaXoverRack.git
"""

from PyQt5 import QtWidgets
from qt5_t5darkstyle import darkstyle_css
import sys
from WindowClasses.MainWindow import MainWindow
#import yappi



def main():
    app = QtWidgets.QApplication(sys.argv)
    form = MainWindow()
    form.setStyleSheet(darkstyle_css())
    form.show()
    app.exec_()


if __name__ == '__main__':
#    yappi.set_clock_type("cpu")
#    yappi.start(builtins=True)
    main()
#    stats = yappi.get_func_stats()
#    stats.save("/home/jh/callgrind.out", type="callgrind")
