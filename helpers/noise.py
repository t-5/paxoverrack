"""
Adapted from https://github.com/python-acoustics/python-acoustics/blob/master/acoustics/generator.py

Copyright Copyright (c) 2013, Python Acoustics

License: https://github.com/python-acoustics/python-acoustics/blob/master/LICENSE


Noise-Generators
****************

Different types of noise are available. The following table lists the color
of noise and how the power and power density change per octave.

====== ===== =============
Color  Power Power density
====== ===== =============
White  +3 dB  0 dB
Pink    0 dB -3 dB
Blue   +6 dB +3 dB
Brown  -3 dB -6 dB
Violet +9 dB +6 dB
====== ===== =============

The colored noise is created by generating pseudo-random numbers using
:func:`np.random.randn` and then multiplying these with a curve tyical for the color.
Afterwards, an inverse DFT is performed using :func:`np.fft.irfft`.
Finally, the noise is normalized using :func:`normalize`.
"""

import numpy as np

try:
    from pyfftw.interfaces.numpy_fft import rfft, irfft # Performs much better than numpy's fftpack
except ImportError:
    rfft = irfft = None # import errors are handled in MainWindow.py


def ms(x):
    """Mean value of signal `x` squared.
    :param x: Dynamic quantity.
    :returns: Mean squared of `x`.
    """
    return (np.abs(x)**2.0).mean()


def normalize(y, x=None):
    """normalize power in y to a (standard normal) white noise signal.
    Optionally normalize to power in signal `x`.
    #The mean power of a Gaussian with :math:`\\mu=0` and :math:`\\sigma=1` is 1.
    """
    if x is not None:
        x = ms(x)
    else:
        x = 1.0
    return y * np.sqrt(x / ms(y))


def noise(N, color='white', state=None):
    """Noise generator.

    :param N: Amount of samples.
    :param color: Color of noise.
    :param state: State of PRNG.
    :type state: :class:`np.random.RandomState`

    """
    try:
        return _noise_generators[color](N, state)
    except KeyError:
        raise ValueError("Incorrect color.")


def white(N, state=None):
    """
    White noise.

    :param N: Amount of samples.
    :param state: State of PRNG.
    :type state: :class:`np.random.RandomState`

    White noise has a constant power density. It's narrowband spectrum is therefore flat.
    The power in white noise will increase by a factor of two for each octave band,
    and therefore increases with 3 dB per octave.
    """
    state = np.random.RandomState() if state is None else state
    # noinspection PyArgumentList
    return state.randn(N)


def pink(N, state=None):
    """
    Pink noise.

    :param N: Amount of samples.
    :param state: State of PRNG.
    :type state: :class:`np.random.RandomState`

    Pink noise has equal power in bands that are proportionally wide.
    Power density decreases with 3 dB per octave.

    """
    state = np.random.RandomState() if state is None else state
    uneven = N%2
    # noinspection PyArgumentList
    X = state.randn(N//2+1+uneven) + 1j * state.randn(N//2+1+uneven)
    S = np.sqrt(np.arange(len(X))+1.) # +1 to avoid divide by zero
    y = (irfft(X/S)).real
    if uneven:
        y = y[:-1]
    return normalize(y)


def blue(N, state=None):
    """
    Blue noise.

    :param N: Amount of samples.
    :param state: State of PRNG.
    :type state: :class:`np.random.RandomState`

    Power increases with 6 dB per octave.
    Power density increases with 3 dB per octave.

    """
    state = np.random.RandomState() if state is None else state
    uneven = N%2
    # noinspection PyArgumentList,PyArgumentList
    X = state.randn(N//2+1+uneven) + 1j * state.randn(N//2+1+uneven)
    S = np.sqrt(np.arange(len(X)))# Filter
    y = (irfft(X*S)).real
    if uneven:
        y = y[:-1]
    return normalize(y)


def brown(N, state=None):
    """
    Violet noise.

    :param N: Amount of samples.
    :param state: State of PRNG.
    :type state: :class:`np.random.RandomState`

    Power decreases with -3 dB per octave.
    Power density decreases with 6 dB per octave.

    """
    state = np.random.RandomState() if state is None else state
    uneven = N%2
    # noinspection PyArgumentList,PyArgumentList
    X = state.randn(N//2+1+uneven) + 1j * state.randn(N//2+1+uneven)
    S = (np.arange(len(X))+1) # Filter
    y = (irfft(X/S)).real
    if uneven:
        y = y[:-1]
    return normalize(y)


def violet(N, state=None):
    """
    Violet noise. Power increases with 6 dB per octave.

    :param N: Amount of samples.
    :param state: State of PRNG.
    :type state: :class:`np.random.RandomState`

    Power increases with +9 dB per octave.
    Power density increases with +6 dB per octave.

    """
    state = np.random.RandomState() if state is None else state
    uneven = N%2
    # noinspection PyArgumentList,PyArgumentList
    X = state.randn(N//2+1+uneven) + 1j * state.randn(N//2+1+uneven)
    S = (np.arange(len(X))) # Filter
    y = (irfft(X*S)).real
    if uneven:
        y = y[:-1]
    return normalize(y)


_noise_generators = {
    'white'  : white,
    'pink'   : pink,
    'blue'   : blue,
    'brown'  : brown,
    'violet' : violet,
    }


def noise_generator(N=44100, color='white', state=None):
    """Noise generator.

    :param N: Amount of unique samples to generate.
    :param color: Color of noise.
    :param state: Random generator state

    Generate `N` amount of unique samples and cycle over these samples.
    """
    return noise(N, color, state)



__all__ = ['noise', 'white', 'pink', 'blue', 'brown', 'violet', 'noise_generator']
