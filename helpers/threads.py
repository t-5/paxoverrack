import numpy
try:
    import sounddevice
    # noinspection PyProtectedMember
    sounddevice._terminate() # immediately terminate sounddevice module, must be reinitilaized at usage time
                             # this will prevent unused pulseaudio connections from hanging around indefinitely
except ImportError:
    sounddevice = None # import errors are handled in MainWindow.py
from helpers.functions import GRAPH_SR
from threading import Thread
from time import sleep, time


class AbstractSoundcardThread:
    """ implements a restartable thread """
    def __init__(self):
        super().__init__()
        self._failure = None
        self._running = False
        self._thread = None
        self._finishedSignal = None


    def failure(self):
        return self._failure


    def run(self):
        raise NotImplementedError


    def setFinishedSignal(self, signal):
        self._finishedSignal = signal


    def start(self):
        self._running = True
        self._thread = Thread(target=self.run)
        self._thread.start()


    def stop(self):
        self._running = False
        self._thread = None


    def stopAndWait(self):
        if not self._running:
            return
        self._running = False
        self._thread.join()
        self._thread = None
        if self._finishedSignal:
            self._finishedSignal.emit(self)


class SoundcardPeakDetectorThread(AbstractSoundcardThread):
    """ Soundcard Peak Detector Thread """
    def __init__(self, samplerate, callbackSignal, channels=(0,1)):
        """
        :param samplerate: samplerate to use
        :param callbackSignal: signal to call with the captured data
        """
        super().__init__()
        self._samplerate = samplerate
        self._callbackSignal = callbackSignal
        self._channels = channels
        # internal state
        self._running = False
        self._recordedSamples = 0
        self._stream = None


    def run(self):
        """ do the capturing as long as the running flag is set """
        # noinspection PyProtectedMember
        sounddevice._initialize()
        lastupdate = 0
        samples = numpy.ndarray((0,2))
        self._failure = None
        try:
            self._stream = sounddevice.InputStream(device="default", samplerate=GRAPH_SR[0], channels=2)
            self._stream.start()
            while self._running:
                if self._running:
                    sleep(0.001)
                    samples = numpy.concatenate((samples, self._stream.read(self._stream.read_available)[0]))
                    if time() > lastupdate + 0.05:
                        self._callbackSignal.emit(samples)
                        samples = numpy.ndarray((0,2))
                        lastupdate = time()
                else:
                    self._stream.stop()
                    self._stream.close()
        except sounddevice.PortAudioError as e:
            try:
                self._stream.stop()
                self._stream.close()
            except sounddevice.PortAudioError:
                pass
            self._failure = e
        # noinspection PyProtectedMember
        sounddevice._terminate()


class SoundcardPlaybackThread(AbstractSoundcardThread):
    """ Soundcard Peak Detector Thread """
    def __init__(self, samplerate, signal, channels=(0,1)):
        """
        :param samplerate: samplerate to use
        :param signal: data to play
        """
        super().__init__()
        self._samplerate = samplerate
        self._signal = signal
        self._channels = channels
        # internal state
        self._running = False


    def run(self):
        """ do the capturing as long as the running flag is set """
        # noinspection PyProtectedMember
        sounddevice._initialize()
        self._failure = None
        try:
            sounddevice.play(self._signal, self._samplerate, blocking=True)
            sounddevice.stop()
            sounddevice.wait()
            self._running = False
        except sounddevice.PortAudioError as e:
            sounddevice.stop()
            sounddevice.wait(ignore_errors=True)
            self._failure = e
        self._running = False
        # noinspection PyProtectedMember
        sounddevice._terminate()


class SoundcardPlaybackCaptureThread(AbstractSoundcardThread):
    """ Soundcard Peak Detector Thread """
    def __init__(self, samplerate, signal, channels=(0,1), finishedCallback=None):
        """
        :param samplerate: samplerate to use
        :param signal: data to play
        """
        super().__init__()
        self._samplerate = samplerate
        self._signal = signal
        self._channels = channels
        self._finishedCallback = finishedCallback
        # internal state
        self._running = False
        self._capturedSamples = numpy.ndarray((2,0))


    def getCapturedSamples(self):
        return self._capturedSamples


    def run(self):
        """ do the capturing as long as the running flag is set """
        # noinspection PyProtectedMember
        sounddevice._initialize()
        self._failure = None
        try:
            self._capturedSamples = sounddevice.playrec(self._signal, self._samplerate, channels=2, blocking=True)
            if self._finishedCallback is not None:
                self._finishedCallback.emit(self._capturedSamples)
            sounddevice.stop()
            sounddevice.wait()
        except sounddevice.PortAudioError as e:
            self._failure = e
            sounddevice.stop()
            sounddevice.wait(ignore_errors=True)
        self._running = False
        # noinspection PyProtectedMember
        sounddevice._terminate()
