import numpy
from math import log, exp, pi
from numpy import hanning

try:
    from pyfftw.interfaces.numpy_fft import fft
except ImportError:
    fft = None # import errors are handled in MainWindow.py

from helpers.functions import gainFactorToDb, GRAPH_SR


class IrFft(object):

    N_FREQUENCIES = 4096

    def __init__(self, ir, maxLevel, fStart, fStop):
        self._ir = ir
        self._maxLevel = maxLevel
        self._fStart = fStart
        self._fStop = fStop
        self._windowSize = None
        # internal instance variables
        self._fftResult = None
        self._trimmedIr = None
        self._peakIdx = None
        # trim IR, perform the FFT on it and calculate log spaced frequencies
        self._trimIr(detectWindowSize=True)
        self._doFFT()
        self._frequencies = self._logFreqs()


    def amplitudes(self, smoothFactor):
        adjustBy = gainFactorToDb(self._maxLevel)
        fftResultLength = self._fftResult.size
        linAmps = numpy.zeros(fftResultLength)
        for i in range(0, fftResultLength):
            linAmps[i] = gainFactorToDb(abs(self._fftResult[i])) + adjustBy
        logAmps = self._spaceAmpsToFreqs(linAmps)
        if smoothFactor is None:
            return logAmps
        return self._smoothForLog(logAmps, smoothFactor)


    def frequencies(self):
        return self._frequencies


    def phases(self, smoothFactor):
        linPhase = numpy.zeros(self._fftResult.size)
        factor = 180.0 / pi
        for i in range(0, self._fftResult.size):
            linPhase[i] = numpy.angle(self._fftResult[i]) * factor
        logPhase = self._spaceAmpsToFreqs(linPhase)
        if smoothFactor is None:
            return logPhase
        return self._fastSmooth(logPhase, smoothFactor)


    def setWindowSize(self, ws):
        self._windowSize = ws
        self._trimIr()
        self._doFFT()


    def windowSize(self):
        return self._windowSize


    def _doFFT(self):
        self._fftResult = self._trimmedIr / self._trimmedIr.max()
        fftLength = self._trimmedIr.size
        if fftLength % GRAPH_SR[0]:
            fftLength += GRAPH_SR[0] - (fftLength % GRAPH_SR[0])
        zeroPadding = numpy.zeros(fftLength - self._trimmedIr.size)
        ir = numpy.concatenate( (self._trimmedIr, zeroPadding) )
        secs = int(fftLength / GRAPH_SR[0])
        fftResults = []
        # split into one second slices and do fft on them
        slicedComplex = numpy.zeros(GRAPH_SR[0], dtype=numpy.complex)
        for sec in range(0, secs):
            slicedIr = ir[sec * GRAPH_SR[0]:(sec + 1) * GRAPH_SR[0]]
            for i in range(0, GRAPH_SR[0]):
                slicedComplex[i] = complex(slicedIr[i], 0)
            fftResults.append(fft(slicedComplex))
        # sum up fft results
        for i in range(1, len(fftResults)):
            fftResults[0] = fftResults[0] + fftResults[i]
        # take first half of summed result
        self._fftResult = fftResults[0][0:int(fftResults[0].size/2)]


    def _fastSmooth(self, amps, smoothFactor):
        smoothed = numpy.zeros(self.N_FREQUENCIES)
        rightFactor = pow(2.0, smoothFactor / 2.0)
        leftFactor = 1.0 / rightFactor
        for i in range(0, self.N_FREQUENCIES):
            left = int(leftFactor * i + 0.5)
            if left < 0:
                left = 0
            right = int(rightFactor * i + 0.5)
            if right > self.N_FREQUENCIES - 1:
                right = self.N_FREQUENCIES - 1
            tmp = 0.0
            increment = 1
            if right - left > 2048:
                increment = int((right - left) / 2048)
            count = 0
            for j in range(left, right + 1, increment):
                tmp += amps[j]
                count += 1
            smoothed[i]  = tmp / count
        return smoothed


    def _logFreqs(self):
            freqs = numpy.zeros(self.N_FREQUENCIES)
            ratio = log(self._fStop / self._fStart) / self.N_FREQUENCIES
            for i in range(0, self.N_FREQUENCIES):
                freqs[i] = exp(ratio * i) * self._fStart
            return freqs


    def _smoothForLog(self, amps, smoothFactor):
        smoothed = numpy.zeros(self.N_FREQUENCIES)
        freqFactor = pow(2.0, smoothFactor / 2.0)
        stepRatio = self._frequencies[1] / self._frequencies[0]
        delta = 0
        ratio = 1.0
        while ratio < freqFactor:
            ratio *= stepRatio
            delta += 1
        for i in range(0, self.N_FREQUENCIES):
            deltaInUse = delta
            left = i - deltaInUse
            if left < 0:
                deltaInUse = i
                left = 0
            right = i + deltaInUse
            if right > self.N_FREQUENCIES - 1:
                deltaInUse = self.N_FREQUENCIES - 1 - i
                right = self.N_FREQUENCIES - 1
            tmp = 0
            for j in range(left, right+1):
                tmp += amps[j]
            smoothed[i]  = tmp / ((deltaInUse << 1) + 1)
        return smoothed


    def _spaceAmpsToFreqs(self, amps):
        out = numpy.zeros(self.N_FREQUENCIES)
        for i in range(0, self.N_FREQUENCIES):
            freq = self._frequencies[i]
            d1 = int(freq)
            d2 = d1 + 1
            if d2 >= amps.size:
                out[i] = amps[d1]
            else:
                out[i] = amps[d1] + (amps[d2] - amps[d1]) * (freq - d1)
        return out


    def _trimIr(self, detectWindowSize=False):
        ir = numpy.copy(self._ir)
        # find IR peak position
        tmp = -2
        for i in range(0, ir.size):
            if ir[i] > tmp:
                self._peakIdx = i
                tmp = ir[i]
        # check if peak position looks somewhat reasonable :)
        if self._peakIdx < ir.size * 0.4 or self._peakIdx > ir.size * 0.6:
            print("DEBUG: very strange peak position %s/%s" % (self._peakIdx, ir.size))
        # apply left window and find second harmonic's IR peak
        dt = ir.size * 0.2 * log(2) / log(self._fStop / self._fStart)
        leftShift = int(dt * GRAPH_SR[0] + 0.5)
        if leftShift > self._peakIdx:
            leftShift = self._peakIdx
        left = self._peakIdx - leftShift
        win = hanning(leftShift * 2 + 1)
        for i in range(0, leftShift+1):
            ir[left + i] = ir[left + i] * win[i]
        # apply right window
        if detectWindowSize:
            self._windowSize = (ir.size - self._peakIdx - 1) / GRAPH_SR[0]
        rightShift = int(self._windowSize * GRAPH_SR[0] + 0.5)
        right = self._peakIdx + rightShift
        if right > ir.size:
            print("DEBUG: window too wide!")
        win = hanning(rightShift * 2 + 1)
        for i in range(0, rightShift + 1):
            ir[self._peakIdx + i] = ir[self._peakIdx + i] * win[i + rightShift]
        self._trimmedIr = ir[left:right+1]
