from math import cos, pi, sin

from DataClasses.XoverLowHighPassWithQ import XoverLowHighPassWithQ
from helpers.functions import GRAPH_SR, prettyFrequency


class XoverSoHighpassWithQ(XoverLowHighPassWithQ):
    """
    Second Order High-Pass Filter With Q
    """
    def __init__(self, sink_name="", gain=0, fequency=1000, q=1):
        super().__init__(sink_name, gain, fequency, q)


    @staticmethod
    def filterName():
        return "Second Order Highpass with Q"


    @staticmethod
    def frequencyResponseCalculations():
        return 1


    def info(self):
        return "Gain: %0.1f dB | Cutoff: %s | Q: %0.2f" % (self._gain, prettyFrequency(self._frequency), self._q)


    @staticmethod
    def ladspaPluginName():
        return "so_highpass_q"


    @staticmethod
    def mmapFilenamePart():
        return "SoHighpassWithQ"


    def _coeffsBA(self):
        """ return 12db/oct high pass biquad coeffs for given frequency """
        w0 = 2 * pi * self._frequency / GRAPH_SR[0]
        alpha = sin(w0) / 2 / self._q
        cs = cos(w0)
        norm = 1 / (1 + alpha)
        b0 = (1 + cs) / 2 * norm
        b1 = -1.0 * (1 + cs) * norm
        b2 = b0
        a1 = -2 * cs * norm
        a2 = (1 - alpha) * norm
        return (b0, b1, b2), (1, a1, a2)
