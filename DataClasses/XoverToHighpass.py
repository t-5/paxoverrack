from math import pi

import numpy

from DataClasses.XoverHighPass import XoverHighPass
from helpers.functions import GRAPH_SR


class XoverToHighpass(XoverHighPass):
    """
    Crosover Third Order Butterworth High-Pass Filter Class
    """
    def __init(self, sink_name="", gain=0, fequency=1000):
        super().__init__(sink_name, gain, fequency)


    @staticmethod
    def filterName():
        return "Third Order Butterworth Highpass"


    @staticmethod
    def frequencyResponseCalculations():
        return 1


    @staticmethod
    def ladspaPluginName():
        return "to_highpass"


    @staticmethod
    def mmapFilenamePart():
        return "ToHighpass"


    def _coeffsBA(self):
        """ return 18db/oct high pass biquad coeffs for given frequency """
        # this code is intentionally broken out into pieces to make it
        # simpler to keep it in sync with the C implementation used in
        # ladspa-t5-plugins
        Wn = 2 * self._frequency / GRAPH_SR[0]
        p = []
        for m in (-2, 0, 2):
            p.append(-numpy.exp(1j * pi * m / 6))
        p = numpy.array(p)
        warped = 4 * numpy.tan(pi * Wn / 2)
        z_analog = numpy.zeros(3)
        z_digital = numpy.ones(3)
        p_analog = warped / p
        k_analog = 1
        p_digital = (4 + p_analog) / (4 - p_analog)
        k_digital = k_analog * numpy.real(numpy.prod(4 - z_analog) / numpy.prod(4 - p_analog))
        b = k_digital * numpy.poly(z_digital)
        a = numpy.poly(p_digital)
        return b, a
