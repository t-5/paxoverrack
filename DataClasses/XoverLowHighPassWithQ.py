import struct

from DataClasses.XoverLowHighPass import XoverLowHighPass
from helpers.functions import prettyFrequency


class XoverLowHighPassWithQ(XoverLowHighPass):
    """
    Crossover Low/High Pass With Q FBase Class
    """

    def __init__(self, sink_name="", gain=0, frequency=1000, q=1):
        super().__init__(sink_name, gain, frequency)
        self._q = q
        
        
    def controlValuesText(self):
        """ return control values for ladspa plugin """
        return "%.5f,%.5f,%.5f,%.5f" % (self._frequency, self._q, self._gain, self.numId())


    @staticmethod
    def filterName():
        raise NotImplementedError


    @staticmethod
    def frequencyResponseCalculations():
        raise NotImplementedError


    @staticmethod
    def gainGraphUpperLimit():
        return 15


    def info(self):
        return "Gain: %0.1f dB | Cutoff: %s | Q: %0.2f" % (self._gain, prettyFrequency(self._frequency), self._q)


    @staticmethod
    def ladspaPluginName():
        raise NotImplementedError


    @staticmethod
    def mmapFilenamePart():
        raise NotImplementedError


    def parameterMappings(self):
        if getattr(self, "_v_cached_parameterMappings", None) is None:
            ret = super().parameterMappings()[:]
            ret.append({
                "description": "Q",
                "getter": self.q,
                "setter": self.setQ,
                "min": 0.1,
                "default": 1,
                "max": 10,
                "scale": "log10",
            })
            self._v_cached_parameterMappings = ret
        return self._v_cached_parameterMappings


    def q(self):
        return self._q


    def setQ(self, q):
        self._q = q


    def _coeffsBA(self):
        raise NotImplementedError


    def _filterParamsAsBytes(self):
        """ return packed bytes object for writing to mmap """
        return struct.pack("ffff",
                           1.0, # changed flag
                           self._frequency,
                           self._q,
                           self._gain)
