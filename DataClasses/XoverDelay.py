import struct

try:
    from scipy import signal
except ImportError:
    signal = None # import errors are handled in MainWindow.py

from DataClasses.XoverFilter import XoverFilter
from helpers.constants import GRAPH_FREQUENCIES, SPEED_OF_SOUND
from helpers.functions import GRAPH_SR


class XoverDelay(XoverFilter):
    """
    Delay Base Class
    """

    def __init__(self, sink_name="", gain=0, delay=0):
        super().__init__(sink_name, gain)
        self._delay = delay
        
        
    def controlValuesText(self):
        """ return control values for ladspa plugin """
        return "%.5f,%.5f,%.5f" % (self._gain, self._delay, self.numId())


    def delay(self):
        return self._delay


    def delaySamples(self):
        return self._delay / 1000 * GRAPH_SR[0]


    @staticmethod
    def filterName():
        raise NotImplementedError


    def frequencyResponse(self, frequencies=GRAPH_FREQUENCIES):
        """ return w, h frequency response components for filter """
        b, a = self._coeffsBA()
        w, h = signal.freqz(b, a, worN=frequencies, whole=True)
        return w, h * self.gainFactor()


    @staticmethod
    def gainGraphLowerLimit():
        return -15


    @staticmethod
    def gainGraphUpperLimit():
        return 1


    def info(self):
        return "Gain: %0.1f dB | Dly: %0.5fms (%0.1fmm)" % (self._gain, self._delay, SPEED_OF_SOUND * self._delay)


    @staticmethod
    def ladspaPluginName():
        raise NotImplementedError


    @staticmethod
    def mmapFilenamePart():
        raise NotImplementedError


    def setDelay(self, delay):
        self._delay = delay
        self._v_dirty = True
        self._v_needMmapUpdate = True


    def parameterMappings(self):
        if getattr(self, "_v_cached_parameterMappings", None) is None:
            ret = super().parameterMappings()[:]
            ret.append({
                "description": "Delay [ms]",
                "getter": self.delay,
                "setter": self.setDelay,
                "min": 0,
                "default": 0,
                "max": 1000,
                "scale": "lin",
            })
            self._v_cached_parameterMappings = ret
        return self._v_cached_parameterMappings


    def _coeffsBA(self):
        raise NotImplementedError


    def _filterParamsAsBytes(self):
        """ return packed bytes object for writing to mmap """
        return struct.pack("fff",
                           1.0,  # changed flag
                           self._delay,
                           self._gain)
