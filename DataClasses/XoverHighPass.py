from DataClasses.XoverLowPass import XoverLowHighPass


class XoverHighPass(XoverLowHighPass):
    """
    Crosover High Pass Filter Base Class
    """

    def __init__(self, sink_name="", gain=0, frequency=1000):
        super().__init__(sink_name, gain, frequency)

    @staticmethod
    def filterName():
        raise NotImplementedError


    @staticmethod
    def frequencyResponseCalculations():
        raise NotImplementedError


    @staticmethod
    def ladspaPluginName():
        raise NotImplementedError


    @staticmethod
    def mmapFilenamePart():
        raise NotImplementedError


    def _coeffsBA(self):
        raise NotImplementedError
