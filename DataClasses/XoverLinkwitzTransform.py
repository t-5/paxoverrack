try:
    from scipy import signal
except ImportError:
    signal = None # import errors are handled in MainWindow.py
import struct

from math import pi, tan

from DataClasses.XoverFilter import XoverFilter
from helpers.constants import GRAPH_FREQUENCIES
from helpers.functions import GRAPH_SR, prettyFrequency


class XoverLinkwitzTransform(XoverFilter):
    """
    Second Order Low-Pass Filter With Q
    """
    def __init__(self, sink_name="", gain=0, frequency=1000, q=0.7071067, f0=1000, q0=0.7071067):
        super().__init__(sink_name, gain)
        self._frequency = frequency
        self._q = q
        self._f0 = f0
        self._q0 = q0
        self._linkedBy = None


    @staticmethod
    def canBeLinked():
        return True


    def controlValuesText(self):
        """ return control values for ladspa plugin """
        return "%.5f,%.5f,%.5f,%.5f,%.5f,%.5f" % (
            self._frequency, self._q, self._f0, self._q0, self._gain, self.numId())


    @staticmethod
    def filterName():
        return "Linkwitz Transform"


    def frequency(self):
        return self._frequency


    def f0(self):
        return self._f0


    def frequencyResponse(self, frequencies=GRAPH_FREQUENCIES):
        """ return w, h frequency response components for filter """
        b, a = self._coeffsBA()
        w, h = signal.freqz(b, a, worN=frequencies, whole=True)
        return w, h * self.gainFactor()


    @staticmethod
    def gainGraphLowerLimit():
        return -30


    @staticmethod
    def gainGraphUpperLimit():
        return 20


    def info(self):
        return "G:%0.1f dB|F:%s|Q:%0.2f|F0:%s|Q0:%0.2f" % (self._gain,
                                                           prettyFrequency(self._frequency), self._q,
                                                           prettyFrequency(self._f0), self._q0)


    @staticmethod
    def ladspaPluginName():
        return "linkwitz_transform"


    def linkedBy(self):
        return self._linkedBy


    @staticmethod
    def mmapFilenamePart():
        return "LinkwitzTransform"


    def parameterMappings(self):
        if getattr(self, "_v_cached_parameterMappings", None) is None:
            ret = super().parameterMappings()[:]
            ret.append({
                "description": "Target Frequency [Hz]",
                "getter": self.frequency,
                "setter": self.setFrequency,
                "min": 2,
                "default": 1000,
                "max": 20000,
                "scale": "log2",
            })
            ret.append({
                "description": "Target Q",
                "getter": self.q,
                "setter": self.setQ,
                "min": 0.1,
                "default": 0.7071067,
                "max": 10,
                "scale": "log10",
            })
            ret.append({
                "description": "Original Frequency [Hz]",
                "getter": self.f0,
                "setter": self.setF0,
                "min": 2,
                "default": 1000,
                "max": 20000,
                "scale": "log2",
            })
            ret.append({
                "description": "Original Q",
                "getter": self.q0,
                "setter": self.setQ0,
                "min": 0.1,
                "default": 0.7071067,
                "max": 10,
                "scale": "log10",
            })
            self._v_cached_parameterMappings = ret
        return self._v_cached_parameterMappings


    def q(self):
        return self._q


    def q0(self):
        return self._q0


    def setFrequency(self, frequency):
        self._frequency = frequency
        self._v_dirty = True
        self._v_needMmapUpdate = True
        if self._linkedBy is not None:
            self._linkedBy.broadCastFrequencyExceptTo(self, frequency)


    def setFrequencyNoLinkUpdate(self, frequency):
        self._frequency = frequency
        self._v_dirty = True
        self._v_needMmapUpdate = True


    def setF0(self, v):
        self._f0 = v


    def setLinkedBy(self, linkedBy):
        self._linkedBy = linkedBy
        self._v_dirty = True


    def setQ(self, v):
        self._q = v


    def setQ0(self, v):
        self._q0 = v


    def _coeffsBA(self):
        """ return linkwitz transform coefficients """
        d0i = pow(2 * pi * self._f0, 2)
        d1i = (2 * pi * self._f0) / self._q0
        d2i = 1
        c0i = pow(2 * pi * self._frequency, 2)
        c1i = (2 * pi * self._frequency) / self._q
        c2i = 1
        gn = 2 * pi / (tan(pi / GRAPH_SR[0]))
        cci = c0i + gn * c1i + gn * gn * c2i

        a1 = (2 * (c0i - gn * gn * c2i) / cci)
        a2 = ((c0i - gn * c1i + gn * gn * c2i) / cci)
        b0 = (d0i + gn * d1i + gn * gn * d2i) / cci
        b1 = 2 * (d0i - (gn * gn * d2i)) / cci
        b2 = (d0i - gn * d1i + gn * gn * d2i) / cci

        return (b0, b1, b2), (1, a1, a2)


    def _filterParamsAsBytes(self):
        """ return packed bytes object for writing to mmap """
        return struct.pack("ffffff",
                           1.0, # changed flag
                           self._frequency,
                           self._q,
                           self._f0,
                           self._q0,
                           self._gain)
