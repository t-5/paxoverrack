import struct

try:
    from scipy import signal
except ImportError:
    signal = None # import errors are handled in MainWindow.py

from DataClasses.XoverFilter import XoverFilter
from helpers.constants import GRAPH_FREQUENCIES


class XoverInverter(XoverFilter):
    """
    Inverter Filter Class
    """

    def __init__(self, sink_name="", gain=0):
        super().__init__(sink_name, gain)

        
    def controlValuesText(self):
        """ return control values for ladspa plugin """
        return "%.5f,%.5f" % (self._gain, self.numId())


    @staticmethod
    def filterName():
        return "Inverter"


    def frequencyResponse(self, frequencies=GRAPH_FREQUENCIES):
        """ return w, h frequency response components for filter """
        b, a = self._coeffsBA()
        w, h = signal.freqz(b, a, worN=frequencies, whole=True)
        return w, h * self.gainFactor()


    @staticmethod
    def gainGraphLowerLimit():
        return -15


    @staticmethod
    def gainGraphUpperLimit():
        return 1


    def info(self):
        return "Gain: %0.1f dB" % self._gain


    @staticmethod
    def ladspaPluginName():
        return "inverter"


    @staticmethod
    def mmapFilenamePart():
        return "Inverter"


    @staticmethod
    def _coeffsBA():
        """ return linear frequency response but inverted """
        return (-1.0,), (1.0,)


    def _filterParamsAsBytes(self):
        """ return packed bytes object for writing to mmap """
        return struct.pack("fff",
                           1.0,  # changed flag
                           self._gain)
