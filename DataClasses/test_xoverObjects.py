from unittest import TestCase


class TestXoverObjects(TestCase):
    maxDiff = None

    def test_simpleStereoInOut(self):
        """
        setup a direct connection from a stereo input sink to the defined output sinks/channels
        """
        from DataClasses.XoverInput import XoverInput
        from DataClasses.XoverOutput import XoverOutput
        input_ = XoverInput()
        output = XoverOutput("InternalOutput", "HwTestSink", "", ("rear-left", "rear-right"))
        input_.connect(output)
        config = input_.generatePulseaudioConfig()
        expected = """# InternalOutput sink, connect to HwTestSink(rear-left,rear-right):
load-module module-remap-sink sink_name=PaXoverRack.InternalOutput sink_properties=device.description=PaXoverRack.InternalOutput remix=no master=HwTestSink channels=2 master_channel_map=rear-left,rear-right channel_map=front-left,front-right
# Input sink, connect to InternalOutput:
load-module module-remap-sink sink_name=PaXoverRack.Input sink_properties=device.description=PaXoverRack.Input remix=no master=PaXoverRack.InternalOutput channels=2 master_channel_map=front-left,front-right channel_map=front-left,front-right
"""
        self.assertEqual(config, expected)


    def test_simpleStereoInLr4FilterOut(self):
        """
        insert a Lr4Lowpass between stereo input sink and a stereo output sink
        """
        from helpers.functions import testingIdGeneratorReset
        from DataClasses.XoverInput import XoverInput
        from DataClasses.XoverOutput import XoverOutput
        from DataClasses.XoverLr4Lowpass import XoverLr4Lowpass
        testingIdGeneratorReset()
        input_ = XoverInput()
        output = XoverOutput("InternalOutput", "HwTestSink", "", ("rear-left", "rear-right"))
        lowpass = XoverLr4Lowpass("LowPass", 0.567, 1234)
        input_.connect(lowpass)
        lowpass.connect(output)
        config = input_.generatePulseaudioConfig()
        expected = """# InternalOutput sink, connect to HwTestSink(rear-left,rear-right):
load-module module-remap-sink sink_name=PaXoverRack.InternalOutput sink_properties=device.description=PaXoverRack.InternalOutput remix=no master=HwTestSink channels=2 master_channel_map=rear-left,rear-right channel_map=front-left,front-right
# LowPass sink/filter, connect to InternalOutput:
load-module module-ladspa-sink sink_name=PaXoverRack.LowPass sink_properties=device.description=PaXoverRack.LowPass sink_master=PaXoverRack.InternalOutput plugin=t5_lr4_lowpass label=lr4_lowpass control=1234.00000,0.56700,1.00000
# Input sink, connect to LowPass:
load-module module-remap-sink sink_name=PaXoverRack.Input sink_properties=device.description=PaXoverRack.Input remix=no master=PaXoverRack.LowPass channels=2 master_channel_map=front-left,front-right channel_map=front-left,front-right
"""
        self.assertEqual(config, expected)


    def test_inTwoLr4FiltersChainedOut(self):
        """
        insert a Lr4Lowpass and a Lr4Highpass between stereo input sink and a stereo output sink
        """
        from helpers.functions import testingIdGeneratorReset
        testingIdGeneratorReset()
        from DataClasses.XoverInput import XoverInput
        from DataClasses.XoverOutput import XoverOutput
        from DataClasses.XoverLr4Highpass import XoverLr4Highpass
        from DataClasses.XoverLr4Lowpass import XoverLr4Lowpass
        input_ = XoverInput()
        output = XoverOutput("InternalOutput", "HwTestSink", "", ("rear-left", "rear-right"))
        lowpass = XoverLr4Lowpass("LowPass", 0.567, 1234)
        highpass = XoverLr4Highpass("HighPass", -0.567, 987)
        input_.connect(lowpass)
        lowpass.connect(highpass)
        highpass.connect(output)
        config = input_.generatePulseaudioConfig()
        expected = """# InternalOutput sink, connect to HwTestSink(rear-left,rear-right):
load-module module-remap-sink sink_name=PaXoverRack.InternalOutput sink_properties=device.description=PaXoverRack.InternalOutput remix=no master=HwTestSink channels=2 master_channel_map=rear-left,rear-right channel_map=front-left,front-right
# HighPass sink/filter, connect to InternalOutput:
load-module module-ladspa-sink sink_name=PaXoverRack.HighPass sink_properties=device.description=PaXoverRack.HighPass sink_master=PaXoverRack.InternalOutput plugin=t5_lr4_highpass label=lr4_highpass control=987.00000,-0.56700,1.00000
# LowPass sink/filter, connect to HighPass:
load-module module-ladspa-sink sink_name=PaXoverRack.LowPass sink_properties=device.description=PaXoverRack.LowPass sink_master=PaXoverRack.HighPass plugin=t5_lr4_lowpass label=lr4_lowpass control=1234.00000,0.56700,2.00000
# Input sink, connect to LowPass:
load-module module-remap-sink sink_name=PaXoverRack.Input sink_properties=device.description=PaXoverRack.Input remix=no master=PaXoverRack.LowPass channels=2 master_channel_map=front-left,front-right channel_map=front-left,front-right
"""
        self.assertEqual(config, expected)


    def test_stereoInOutToQuadOut(self):
        """
        setup a split connection from a stereo input sink to two stereo outputs
        """
        from DataClasses.XoverInput import XoverInput
        from DataClasses.XoverOutput import XoverOutput
        input_ = XoverInput()
        output1 = XoverOutput("InternalOutput1", "HwTestSink1", "", ("front-left", "front-right"))
        output2 = XoverOutput("InternalOutput2", "HwTestSink1", "", ("rear-left", "rear-right"))
        input_.connect(output1)
        input_.connect(output2)
        config = input_.generatePulseaudioConfig()
        expected = """# InternalOutput1 sink, connect to HwTestSink1(front-left,front-right):
load-module module-remap-sink sink_name=PaXoverRack.InternalOutput1 sink_properties=device.description=PaXoverRack.InternalOutput1 remix=no master=HwTestSink1 channels=2 master_channel_map=front-left,front-right channel_map=front-left,front-right
# InternalOutput2 sink, connect to HwTestSink1(rear-left,rear-right):
load-module module-remap-sink sink_name=PaXoverRack.InternalOutput2 sink_properties=device.description=PaXoverRack.InternalOutput2 remix=no master=HwTestSink1 channels=2 master_channel_map=rear-left,rear-right channel_map=front-left,front-right
# Input sink, split to InternalOutput1,InternalOutput2:
load-module module-combine-sink sink_name=PaXoverRack.Input sink_properties=device.description=PaXoverRack.Input channels=2 channel_map=front-left,front-right slaves=PaXoverRack.InternalOutput1,PaXoverRack.InternalOutput2
"""
        self.assertEqual(config, expected)


    def test_stereoInSplitLpHpQuadOut(self):
        """
        setup a split connection from a stereo input sink to two filters and two stereo outputs
        """
        from helpers.functions import testingIdGeneratorReset
        testingIdGeneratorReset()
        from DataClasses.XoverInput import XoverInput
        from DataClasses.XoverOutput import XoverOutput
        from DataClasses.XoverLr4Highpass import XoverLr4Highpass
        from DataClasses.XoverLr4Lowpass import XoverLr4Lowpass
        input_ = XoverInput()
        output1 = XoverOutput("InternalOutput1", "HwTestSink1", "", ("front-left", "front-right"))
        output2 = XoverOutput("InternalOutput2", "HwTestSink1", "", ("rear-left", "rear-right"))
        lowpass = XoverLr4Lowpass("LowPass", 0.567, 1234)
        highpass = XoverLr4Highpass("HighPass", -0.567, 987)
        input_.connect(lowpass)
        input_.connect(highpass)
        lowpass.connect(output1)
        highpass.connect(output2)
        config = input_.generatePulseaudioConfig()
        expected = """# InternalOutput1 sink, connect to HwTestSink1(front-left,front-right):
load-module module-remap-sink sink_name=PaXoverRack.InternalOutput1 sink_properties=device.description=PaXoverRack.InternalOutput1 remix=no master=HwTestSink1 channels=2 master_channel_map=front-left,front-right channel_map=front-left,front-right
# LowPass sink/filter, connect to InternalOutput1:
load-module module-ladspa-sink sink_name=PaXoverRack.LowPass sink_properties=device.description=PaXoverRack.LowPass sink_master=PaXoverRack.InternalOutput1 plugin=t5_lr4_lowpass label=lr4_lowpass control=1234.00000,0.56700,1.00000
# InternalOutput2 sink, connect to HwTestSink1(rear-left,rear-right):
load-module module-remap-sink sink_name=PaXoverRack.InternalOutput2 sink_properties=device.description=PaXoverRack.InternalOutput2 remix=no master=HwTestSink1 channels=2 master_channel_map=rear-left,rear-right channel_map=front-left,front-right
# HighPass sink/filter, connect to InternalOutput2:
load-module module-ladspa-sink sink_name=PaXoverRack.HighPass sink_properties=device.description=PaXoverRack.HighPass sink_master=PaXoverRack.InternalOutput2 plugin=t5_lr4_highpass label=lr4_highpass control=987.00000,-0.56700,2.00000
# Input sink, split to LowPass,HighPass:
load-module module-combine-sink sink_name=PaXoverRack.Input sink_properties=device.description=PaXoverRack.Input channels=2 channel_map=front-left,front-right slaves=PaXoverRack.LowPass,PaXoverRack.HighPass
"""
        self.assertEqual(config, expected)


    def test_fullyFledged3wayXover(self):
        """
               .- SubLowPass -- SubEq ----------------------- HwTestSink1(front-left,front.right)
               |
        Input -+- MidHighPass -+- MidLowPass -- MidEq ------- HwTestSink1(rear-left,rear.right)
                               |
                               `- TweetHighPass -- TweetEq -- HwTestSink2(front-left,front.right)
        """
        from helpers.functions import testingIdGeneratorReset
        testingIdGeneratorReset()
        from DataClasses.XoverInput import XoverInput
        from DataClasses.XoverOutput import XoverOutput
        from DataClasses.XoverLr4Highpass import XoverLr4Highpass
        from DataClasses.XoverLr4Lowpass import XoverLr4Lowpass
        from DataClasses.XoverParamEq import XoverParamEq
        input_ = XoverInput()
        output1 = XoverOutput("InternalOutput1", "HwTestSink1", "", ("front-left", "front-right"))
        output2 = XoverOutput("InternalOutput2", "HwTestSink1", "", ("rear-left", "rear-right"))
        output3 = XoverOutput("InternalOutput3", "HwTestSink2", "", ("front-left", "front-right"))
        subLowPass = XoverLr4Lowpass("SubLowPass", 0, 150)
        midHighPass = XoverLr4Highpass("MidHighPass", 0, 150)
        input_.connect(subLowPass)
        input_.connect(midHighPass)
        midLowPass = XoverLr4Lowpass("MidLowPass", 0, 2500)
        tweetHighPass = XoverLr4Highpass("TweetHighPass", 0, 2500)
        midHighPass.connect(midLowPass)
        midHighPass.connect(tweetHighPass)
        subEq = XoverParamEq("SubEq", 0, 40, 8, 0.707, 300, 0, 1, 1000, 0, 1, 3000, 0, 1, 10000, 0, 1)
        midEq = XoverParamEq("MidEq", 0, 30, 0, 1, 300, 0, 1, 1122, -3, 0.5, 3000, 0, 1, 10000, 0, 1)
        tweetEq = XoverParamEq("TweetEq", 0, 30, 0, 1, 300, 0, 1, 1000, 0, 1, 5500, -2, 0.707, 10000, 3, 0.5)
        subLowPass.connect(subEq)
        midLowPass.connect(midEq)
        tweetHighPass.connect(tweetEq)
        subEq.connect(output1)
        midEq.connect(output2)
        tweetEq.connect(output3)
        config = input_.generatePulseaudioConfig()
        expected = """# InternalOutput1 sink, connect to HwTestSink1(front-left,front-right):
load-module module-remap-sink sink_name=PaXoverRack.InternalOutput1 sink_properties=device.description=PaXoverRack.InternalOutput1 remix=no master=HwTestSink1 channels=2 master_channel_map=front-left,front-right channel_map=front-left,front-right
# SubEq sink/filter, connect to InternalOutput1:
load-module module-ladspa-sink sink_name=PaXoverRack.SubEq sink_properties=device.description=PaXoverRack.SubEq sink_master=PaXoverRack.InternalOutput1 plugin=t5_3band_parameq_with_shelves label=3band_parameq_with_shelves control=40.00000,8.00000,0.70700,300.00000,0.00000,1.00000,1000.00000,0.00000,1.00000,3000.00000,0.00000,1.00000,10000.00000,0.00000,1.00000,0.00000,1.00000
# SubLowPass sink/filter, connect to SubEq:
load-module module-ladspa-sink sink_name=PaXoverRack.SubLowPass sink_properties=device.description=PaXoverRack.SubLowPass sink_master=PaXoverRack.SubEq plugin=t5_lr4_lowpass label=lr4_lowpass control=150.00000,0.00000,2.00000
# InternalOutput2 sink, connect to HwTestSink1(rear-left,rear-right):
load-module module-remap-sink sink_name=PaXoverRack.InternalOutput2 sink_properties=device.description=PaXoverRack.InternalOutput2 remix=no master=HwTestSink1 channels=2 master_channel_map=rear-left,rear-right channel_map=front-left,front-right
# MidEq sink/filter, connect to InternalOutput2:
load-module module-ladspa-sink sink_name=PaXoverRack.MidEq sink_properties=device.description=PaXoverRack.MidEq sink_master=PaXoverRack.InternalOutput2 plugin=t5_3band_parameq_with_shelves label=3band_parameq_with_shelves control=30.00000,0.00000,1.00000,300.00000,0.00000,1.00000,1122.00000,-3.00000,0.50000,3000.00000,0.00000,1.00000,10000.00000,0.00000,1.00000,0.00000,3.00000
# MidLowPass sink/filter, connect to MidEq:
load-module module-ladspa-sink sink_name=PaXoverRack.MidLowPass sink_properties=device.description=PaXoverRack.MidLowPass sink_master=PaXoverRack.MidEq plugin=t5_lr4_lowpass label=lr4_lowpass control=2500.00000,0.00000,4.00000
# InternalOutput3 sink, connect to HwTestSink2(front-left,front-right):
load-module module-remap-sink sink_name=PaXoverRack.InternalOutput3 sink_properties=device.description=PaXoverRack.InternalOutput3 remix=no master=HwTestSink2 channels=2 master_channel_map=front-left,front-right channel_map=front-left,front-right
# TweetEq sink/filter, connect to InternalOutput3:
load-module module-ladspa-sink sink_name=PaXoverRack.TweetEq sink_properties=device.description=PaXoverRack.TweetEq sink_master=PaXoverRack.InternalOutput3 plugin=t5_3band_parameq_with_shelves label=3band_parameq_with_shelves control=30.00000,0.00000,1.00000,300.00000,0.00000,1.00000,1000.00000,0.00000,1.00000,5500.00000,-2.00000,0.70700,10000.00000,3.00000,0.50000,0.00000,5.00000
# TweetHighPass sink/filter, connect to TweetEq:
load-module module-ladspa-sink sink_name=PaXoverRack.TweetHighPass sink_properties=device.description=PaXoverRack.TweetHighPass sink_master=PaXoverRack.TweetEq plugin=t5_lr4_highpass label=lr4_highpass control=2500.00000,0.00000,6.00000
# MidHighPass sink/filter, split to MidLowPass,TweetHighPass:
load-module module-combine-sink sink_name=PaXoverRack.MidHighPass.Splitter sink_properties=device.description=PaXoverRack.MidHighPass.Splitter channels=2 channel_map=front-left,front-right slaves=PaXoverRack.MidLowPass,PaXoverRack.TweetHighPass
load-module module-ladspa-sink sink_name=PaXoverRack.MidHighPass sink_properties=device.description=PaXoverRack.MidHighPass sink_master=PaXoverRack.MidHighPass.Splitter plugin=t5_lr4_highpass label=lr4_highpass control=150.00000,0.00000,7.00000
# Input sink, split to SubLowPass,MidHighPass:
load-module module-combine-sink sink_name=PaXoverRack.Input sink_properties=device.description=PaXoverRack.Input channels=2 channel_map=front-left,front-right slaves=PaXoverRack.SubLowPass,PaXoverRack.MidHighPass
"""
        self.assertEqual(config, expected)
