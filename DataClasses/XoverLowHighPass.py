import struct

try:
    from scipy import signal
except ImportError:
    signal = None # import errors are handled in MainWindow.py

from DataClasses.XoverFilter import XoverFilter
from helpers.constants import GRAPH_FREQUENCIES
from helpers.functions import prettyFrequency


class XoverLowHighPass(XoverFilter):
    """
    Crosover Low/High Pass Filter Base Class
    """

    @staticmethod
    def filterName():
        pass

    def __init__(self, sink_name="", gain=0, frequency=1000):
        super().__init__(sink_name, gain)
        self._frequency = frequency
        self._linkedBy = None


    @staticmethod
    def canBeLinked():
        return True


    def controlValuesText(self):
        """ return control values for ladspa plugin """
        return "%.5f,%.5f,%.5f" % (self._frequency, self._gain, self.numId())


    def frequency(self):
        return self._frequency


    def frequencyResponse(self, frequencies=GRAPH_FREQUENCIES):
        """ return w, h frequency response components for filter """
        hs = []
        b, a = self._coeffsBA()
        w, h = signal.freqz(b, a, worN=frequencies, whole=True)
        hs.append(h)
        hs.append(h)
        # maybe do filter calculation twice, as a lr-4 filter is two
        # 12dB/oct filters stacked on top of each other
        for i in range(1, self.frequencyResponseCalculations()):
            for j in range(0, len(hs[0])):
                hs[0][j] = hs[i][j] * hs[0][j]
        return w, hs[0] * self.gainFactor()


    @staticmethod
    def frequencyResponseCalculations():
        raise NotImplementedError


    @staticmethod
    def gainGraphLowerLimit():
        return -30


    @staticmethod
    def gainGraphUpperLimit():
        return 2


    def info(self):
        return "Gain: %0.1f dB | Cutoff: %s" % (self._gain, prettyFrequency(self._frequency))


    def linkedBy(self):
        return self._linkedBy


    @staticmethod
    def ladspaPluginName():
        raise NotImplementedError


    @staticmethod
    def mmapFilenamePart():
        raise NotImplementedError


    def parameterMappings(self):
        if getattr(self, "_v_cached_parameterMappings", None) is None:
            ret = super().parameterMappings()[:]
            ret.append({
                "description": "Cutoff Frequency [Hz]",
                "getter": self.frequency,
                "setter": self.setFrequency,
                "min": 2,
                "default": 1000,
                "max": 20000,
                "scale": "log2",
            })
            self._v_cached_parameterMappings = ret
        return self._v_cached_parameterMappings


    def setFrequency(self, frequency):
        self._frequency = frequency
        self._v_dirty = True
        self._v_needMmapUpdate = True
        if self._linkedBy is not None:
            self._linkedBy.broadCastFrequencyExceptTo(self, frequency)


    def setFrequencyNoLinkUpdate(self, frequency):
        self._frequency = frequency
        self._v_dirty = True
        self._v_needMmapUpdate = True


    def setLinkedBy(self, linkedBy):
        self._linkedBy = linkedBy
        self._v_dirty = True


    def _coeffsBA(self):
        raise NotImplementedError


    def _filterParamsAsBytes(self):
        """ return packed bytes object for writing to mmap """
        return struct.pack("fff",
                           1.0, # changed flag
                           self._frequency,
                           self._gain)
