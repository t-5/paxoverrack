import numpy

from PyQt5.QtGui import QImage, QPixmap

from helpers.constants import GRAPH_FREQUENCIES

try:
    from persistent.list import PersistentList
except ImportError:
    PersistentList = None # import errors will be handled in MainWindow

from CustomWidgets.OutputWidget import OutputWidget
from DataClasses.XoverObject import XoverObject


class XoverOutput(XoverObject):
    """
    Crosover Output Class
    """

    def __init__(self, sink_name, output_sink_name, output_sink_description, output_sink_channels):
        super().__init__()
        if sink_name == "":
            sink_name = self.tempSinkName()
        self._sink_name = sink_name
        self._output_sink_name = output_sink_name
        self._output_sink_description = output_sink_description
        self._output_sink_channels = output_sink_channels
        self._swapChannels = False
        self._frequencyResponsePng = b""
        self._v_frequencyResponsePixmap = None
        self._v_widget = None # volatile instance variable for holding the qt widget
        self._inputs = PersistentList()


    def connectReverse(self, input_):
        if input_ not in self._inputs:
            self._inputs.append(input_)


    def disconnectAll(self):
        for input_ in self._inputs[:]:
            input_.disconnect(self)


    def disconnectReverse(self, input_):
        if input_ in self._inputs:
            self._inputs.remove(input_)


    def frequencyResponse(self):
        if len(self._inputs) == 0:
            return GRAPH_FREQUENCIES, numpy.ones(len(GRAPH_FREQUENCIES))
        return self._inputs[0].aggregatedFrequencyResponse()


    def frequencyResponsePixmap(self):
        if getattr(self, "_v_frequencyResponsePixmap", None) is None:
            img = QImage.fromData(self._frequencyResponsePng)
            self._v_frequencyResponsePixmap = QPixmap(img)
        return self._v_frequencyResponsePixmap


    def generatePulseaudioConfig(self):
        ret = "# %s sink, connect to %s(%s):\n" % (self.sinkName(), self.outputSinkName(), self.outputSinkChannelsText())
        ret += "load-module module-remap-sink sink_name=PaXoverRack.%s sink_properties=device.description=PaXoverRack.%s " \
            "remix=no master=%s channels=%d master_channel_map=%s channel_map=%s\n" % \
            (self.sinkName(), self.sinkName(), self.outputSinkName(), self.numChannels(),
             self.outputSinkChannelsText(), self.sinkChannelsText())
        return ret


    def hasInputConnections(self):
        return len(self._inputs) > 0


    def info(self):
        return ""


    def inputs(self):
        return self._inputs


    def markDirty(self):
        pass


    @staticmethod
    def outputs():
        return []


    def outputSinkChannels(self):
        ret = self._output_sink_channels
        if self._swapChannels:
            ret = list(reversed(ret))
        return ret


    def outputSinkChannelsText(self):
        if len(self._output_sink_channels) == 1:
            return "%s,%s" % (self._output_sink_channels[0], self._output_sink_channels[0])
        return ",".join(self.outputSinkChannels())


    def outputSinkDescription(self):
        return self._output_sink_description


    def outputSinkName(self):
        return self._output_sink_name


    def restoreWidget(self, drawArea):
        """ restore widget for all connected xover objects """
        self._v_widget = OutputWidget(drawArea, self, self._widgetX, self._widgetY)
        self._v_widget.show()


    def setFrequencyResponsePng(self, png):
        self._frequencyResponsePng = png
        self._v_frequencyResponsePixmap = None


    def setOutputSinkChannels(self, output_sink_channels):
        self._output_sink_channels = output_sink_channels


    def setOutputSinkDescription(self, output_sink_description):
        self._output_sink_description = output_sink_description


    def setOutputSinkName(self, output_sink_name):
        self._output_sink_name = output_sink_name


    def setSinkName(self, sink_name):
        self._sink_name = sink_name


    def setSwapChannels(self, b):
        self._swapChannels = b


    def sinkName(self):
        return self._sink_name


    def sinkNameWithPrefix(self):
        return "PaXoverRack.%s" % self._sink_name


    def swapChannels(self):
        return self._swapChannels


    def updateOutputFrequencyResponsePngs(self):
        w, h = self.frequencyResponse()
        png = self._v_widget.mainWindow().frequencyResponseGraphPngFor(w, h)
        self._frequencyResponsePng = png
        self._v_frequencyResponsePixmap = None
        self._v_widget.updateFrequencyResponseLabel()
        self._v_widget.repaint()


    def walkAndFindReachable(self):
        return [self]


    def widget(self):
        return self._v_widget
