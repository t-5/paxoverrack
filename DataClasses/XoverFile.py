import os
import shutil
import uuid
from time import time
from tempfile import mkstemp

from DataClasses.XoverFilterLink import XoverFilterLink
from DataClasses.XoverLinkwitzTransform import XoverLinkwitzTransform
from helpers.constants import CURRENT_DATAMODEL_VERSION

try:
    import transaction
    from ZODB import FileStorage, DB
    from ZODB.broken import find_global
    from ZODB.serialize import referencesf
    from persistent.list import PersistentList
except ImportError:
    # import errors will be handled in MainWindow
    find_global = transaction =  FileStorage = referencesf = PersistentList = None
    DB = object

from DataClasses.XoverFilter import XoverFilter
from DataClasses.XoverInput import XoverInput
from DataClasses.XoverLowHighPass import XoverLowHighPass
from DataClasses.XoverOutput import XoverOutput


ALLOWED_MODULE_GLOBALS = (
    ("DataClasses.XoverFilterLink", "XoverFilterLink"),
    ("DataClasses.XoverFoHighpass", "XoverFoHighpass"),
    ("DataClasses.XoverFoLowpass", "XoverFoLowpass"),
    ("DataClasses.XoverHighPass", "XoverHighPass"),
    ("DataClasses.XoverInput", "XoverInput"),
    ("DataClasses.XoverInverter", "XoverInverter"),
    ("DataClasses.XoverLinkwitzTransform", "XoverLinkwitzTransform"),
    ("DataClasses.XoverLr2Highpass", "XoverLr2Highpass"),
    ("DataClasses.XoverLr2Lowpass", "XoverLr2Lowpass"),
    ("DataClasses.XoverLr4Highpass", "XoverLr4Highpass"),
    ("DataClasses.XoverLr4Lowpass", "XoverLr4Lowpass"),
    ("DataClasses.XoverOutput", "XoverOutput"),
    ("DataClasses.XoverParamEq", "XoverParamEq"),
    ("DataClasses.XoverSampleDelay", "XoverSampleDelay"),
    ("DataClasses.XoverSoHighpass", "XoverSoHighpass"),
    ("DataClasses.XoverSoHighpassWithQ", "XoverSoHighpassWithQ"),
    ("DataClasses.XoverSoLowpass", "XoverSoLowpass"),
    ("DataClasses.XoverSoLowpassWithQ", "XoverSoLowpassWithQ"),
    ("DataClasses.XoverSubsampleDelay", "XoverSubsampleDelay"),
    ("DataClasses.XoverToHighpass", "XoverToHighpass"),
    ("DataClasses.XoverToLowpass", "XoverToLowpass"),
    ("persistent.list", "PersistentList"),
    ("persistent.mapping", "PersistentMapping"),
    ("uuid", "SafeUUID"),
    ("uuid", "UUID"),
)


class XoverDB(DB):
    """
    derived from ZODB.DB.DB - overrides classFactory() to only allow
    import of very specific classes
    """
    def classFactory(self, connection, modulename, globalname):
        if (modulename, globalname) not in ALLOWED_MODULE_GLOBALS:
            raise TypeError("Not allowed to import global %r from module %r" % (globalname, modulename))
        return super().classFactory(connection, modulename, globalname)


class XoverFile(object):
    """
    Crossover configuration file class, acts as a wrapper around
    the ZODB FilesStorage contained in it and handles copying to
    and from tempfiles in the background
    """
    def __init__(self, filename):
        """
        initializes a new XoverFile instance
        :param filename: give the filename of the FileStorage file,
                         if filename is empty, only init temp files.
        """
        self._filename = filename
        self._dirty = False
        fh, self._tempname = mkstemp(prefix="PaXoverRack_", suffix=".fs")
        os.close(fh)
        if filename != "":
            shutil.copyfile(self._filename, self._tempname)
        self._zodb_storage = FileStorage.FileStorage(self._tempname)
        self._zodb_db = XoverDB(self._zodb_storage)
        self._zodb_connection = self._zodb_db.open()
        self._zodb_root = self._zodb_connection.root()
        self._bootstrap()
        self.resetMmaps()


    def addObject(self, obj):
        self._zodb_root["allObjects"].append(obj)
        self.markDirty()


    def allFilters(self, sort=False):
        ret = list(filter(lambda x: issubclass(x.__class__, XoverFilter),
                          self._zodb_root["allObjects"]))
        if sort:
            ret.sort(key=XoverFilter.sinkName)
        return ret


    def allLinkableFilters(self, sort=False):
        ret = list(filter(lambda x: issubclass(x.__class__, XoverFilter) and x.canBeLinked(),
                          self._zodb_root["allObjects"]))
        if sort:
            ret.sort(key=XoverFilter.sinkName)
        return ret


    def allOutputs(self, sort=False):
        ret = list(filter(lambda x: issubclass(x.__class__, XoverOutput),
                          self._zodb_root["allObjects"]))
        if sort:
            ret.sort(key=XoverOutput.sinkName)
        return ret


    def applyFilterParams(self):
        """ apply filter params via mmaps """
        for obj in self._zodb_root["allObjects"]:
            obj.applyFilterParams()


    def close(self):
        """ close db connection and remove temporary db files """
        transaction.abort()
        self._zodb_connection.close()
        self._zodb_root = None
        # cleanup db files in TMP dir
        for fn in (self._tempname,
                   "%s.index" % self._tempname,
                   "%s.index_tmp" % self._tempname,
                   "%s.lock" % self._tempname,
                   "%s.old" % self._tempname,
                   "%s.pack" % self._tempname,
                   "%s.tmp" % self._tempname):
            try:
               os.unlink(fn)
            except OSError:
                pass


    def dbRoot(self):
        """ return zodb root """
        return self._zodb_root


    def delObject(self, obj):
        if obj in self._zodb_root["allObjects"]:
            self._zodb_root["allObjects"].remove(obj)
        self.markDirty()


    def fileName(self):
        """ return filename """
        return self._filename


    def filterSinkNames(self):
        return map(lambda x: x.sinkName(),
                   filter(lambda x: issubclass(x.__class__,  XoverFilter),
                          self._zodb_root["allObjects"]))


    def findCyclicGraph(self):
        """
        if there is a cyclic connection in the graoh of XoverObjects,
        this method returns True
        """
        inp = self._zodb_root["xoverInput"]
        reachable = inp.walkAndFindReachable()
        uniqReachable = list(set(reachable))
        return len(reachable) != len(uniqReachable)


    def hasUnconnectedObjects(self):
        if not self.xoverInput().hasOutputConnections():
            return True
        for output in self.xoverOutputs():
            if not output.hasInputConnections():
                return True
        for filter_ in self.xoverFilters():
            if not filter_.hasInputConnections():
                return True
            if not filter_.hasOutputConnections():
                return True
        return False


    def isDirty(self):
        """ return dirty flag """
        return self._dirty


    def isUniqueName(self, name, exclude=None):
        for obj in self._zodb_root["allObjects"]:
            if obj == exclude:
                continue
            if obj.sinkName() == name:
                return False
        return True


    def lowHighPassFilters(self):
        # also include Linkwitz Transform filters here!
        return list(filter(lambda x: issubclass(x.__class__, XoverLowHighPass) or \
                                     issubclass(x.__class__, XoverLinkwitzTransform),
                           self._zodb_root["allObjects"]))


    def markClean(self):
        self._dirty = False


    def markDirty(self):
        self._dirty = True


    def nameExists(self, name):
        for obj in self._zodb_root["allObjects"]:
            if obj.sinkName() == name:
                return True
        return False


    def outputSinkNames(self):
        return map(lambda x: x.outputSinkName(), self.xoverOutputs())


    def resetMmaps(self):
        """ reset mmaps of all filters """
        for obj in self._zodb_root["allObjects"]:
            obj.resetMmaps()


    def restoreWidgets(self, drawArea):
        drawArea.clear()
        for obj in self._zodb_root["allObjects"]:
            obj.restoreWidget(drawArea)


    def save(self):
        """ save temporary db to original filename """
        self.saveAs(self._filename, needNewUuid=False)


    def saveAs(self, filename, needNewUuid=True):
        """ save temporary db to given filename """
        if needNewUuid:
            self._zodb_root["UUID"] = uuid.uuid4()
        self._zodb_root["saved_version"] = CURRENT_DATAMODEL_VERSION
        transaction.commit()
        self._zodb_storage.pack(time(), referencesf)
        shutil.copyfile(self._tempname, filename)
        self._filename = filename
        self.markClean()


    def savedVersion(self):
        return self._zodb_root["saved_version"]


    def uuid(self):
        return self._zodb_root["UUID"]


    def xoverFilters(self):
        return filter(lambda x: isinstance(x, XoverFilter), self._zodb_root["allObjects"])


    def xoverInput(self):
        return self._zodb_root["xoverInput"]


    def xoverOutputs(self):
        return filter(lambda x: x.__class__ is XoverOutput, self._zodb_root["allObjects"])


    def _bootstrap(self):
        """ setup internal structure of ZODB """
        root = self._zodb_root
        # data model change in v1.34:
        #  * add software version on save and check on open
        if not "saved_version" in root:
            root["saved_version"] = 1033
            self.markDirty()
        if root["saved_version"] < 1037:
            # Here come the data model changes before saving of the data model version was introduced in 1.34 (1034).
            # As we didn't have data model versions available before that, we have to go through all of them...
            # All data model changes > 1037 will be guarded by a similar if statement and will only be executed once.

            # Initial setup of containers and the UUID (these will be executed only once too, because the data model
            # version is set to 1033 on a new file
            if not "UUID" in root:
                root["UUID"] = str(uuid.uuid4())
                self.markDirty()
                transaction.commit()
            if not "allObjects" in root:
                root["allObjects"] = PersistentList()
                self.markDirty()
                transaction.commit()
            if not "xoverInput" in root:
                xoverInput = XoverInput()
                root["xoverInput"] = xoverInput
                self.addObject(xoverInput)
                transaction.commit()
            # data model change in v1.2:
            #  * added XoverObject._mono (bool)
            for flt in root["allObjects"]:
                if getattr(flt, "_mono", None) is None:
                    flt._mono = False
                    self.markDirty()
            # data model change in v1.12:
            #  * added XoverFilter._frdFilename (str)
            for flt in self.allFilters():
                if getattr(flt, "_frdFilename", None) is None:
                    flt._frdFilename = ""
                    self.markDirty()
            # data model change in v1.27:
            #  * added XoverOutput._swapChannels (bool)
            for flt in self.allOutputs():
                if getattr(flt, "_swapChannels", None) is None:
                    flt._swapChannels = False
                    self.markDirty()
            # data model change in v1.31:
            #  * one XoverFilter can now be linked to more than one XoverFilter at a time
            #    -> _linkedTo becomes obsolete
            #    -> _linkedBy now is represented through a XoverFilterLink object
            for flt in self.allFilters():
                linkedTo = getattr(flt, "_linkedTo", None)
                if linkedTo is not None:
                    link = XoverFilterLink()
                    link.addLinkedFilter(flt)
                    link.addLinkedFilter(linkedTo)
                    delattr(flt, "_linkedTo")
                    delattr(linkedTo, "_linkedTo")
                    flt._linkedBy = link
                    linkedTo._linkedBy = link
                    self.markDirty()
            marker = object()
            for flt in self.allFilters():
                if flt.canBeLinked() and getattr(flt, "_linkedBy", marker) is marker:
                    flt._linkedBy = None
                    self.markDirty()
        # if root["saved_version"] < 1038: ... TBD
