from math import pi, tan

from DataClasses.XoverLowPass import XoverLowPass
from helpers.functions import GRAPH_SR


class XoverFoLowpass(XoverLowPass):
    """
    Crosover First Order Butterworth Low-Pass Filter Class
    """
    def __init(self, sink_name="", gain=0, fequency=1000):
        super().__init__(sink_name, gain, fequency)


    @staticmethod
    def filterName():
        return "First Order Butterworth Lowpass"


    @staticmethod
    def frequencyResponseCalculations():
        return 1


    @staticmethod
    def ladspaPluginName():
        return "fo_lowpass"


    @staticmethod
    def mmapFilenamePart():
        return "FoLowpass"


    def _coeffsBA(self):
        """ return 6dB/oct low pass biquad coeffs for given frequency """
        w = 2 * pi * self._frequency
        A = 1 / (tan((w / GRAPH_SR[0]) / 2))
        b0 = 1 / (1 + A)
        b1 = b0
        a1 = (1 - A) / (1 + A)
        return (b0, b1), (1, a1)
