import glob
import mmap
import time
import numpy
import os
import re
try:
    from scipy import signal
except ImportError:
    signal = None # import errors are handled in MainWindow.py
from PyQt5.QtGui import QPixmap, QImage
from CustomWidgets.FilterWidget import FilterWidget
from DataClasses.XoverConnectedObject import XoverConnectedObject
from DataClasses.XoverFilterLink import XoverFilterLink
from helpers.constants import GRAPH_FREQUENCIES
from helpers.functions import dbToGainFactor


class XoverFilter(XoverConnectedObject):
    """
    Crosover Filter Base Class
    """

    def __init__(self, sink_name="", gain=0.0):
        super().__init__()
        if sink_name == "":
            sink_name = re.sub("[^A-Za-z0-9._]", "", self.filterName())
        self._sink_name = sink_name
        self._gain = gain
        self._frdFilename = ""
        self._frequencyResponsePng = b""
        self._v_frequencyResponsePixmap = None
        self._v_dirty = True
        self._v_lastModified = time.time()
        self._v_mmaps = None
        self._v_needMmapUpdate = True
        self._v_widget = None
        self._v_cached_parameterMappings = None


    ## removed for now
    #def aggregatedCoeffsBA(self):
    #    if not self._inputs:
    #        return self._coeffsBA()
    #    b_before, a_before = self._inputs[0].aggregatedCoeffsBA()
    #    b, a = self._coeffsBA()
    #    return numpy.convolve(b_before, b), numpy.convolve(a_before, a)


    def aggregatedFrequencyResponse(self, frequencies=GRAPH_FREQUENCIES):
        if not self._inputs:
            return frequencies, numpy.ones(len(frequencies))
        ## removed for now
        #b, a = self.aggregatedCoeffsBA()
        #return signal.freqz(b, a, worN=frequencies, whole=True)
        wa, ha = self._inputs[0].aggregatedFrequencyResponse(frequencies=frequencies)
        w, h = self.frequencyResponse(frequencies=frequencies)
        return w, h * ha


    def applyFilterParams(self):
        """ apply filter params via mmaps """
        if getattr(self, "_v_needMmapUpdate", None) is None:
            self._v_needMmapUpdate = True
        if not self._v_needMmapUpdate:
            return
        if self._v_mmaps is None:
            self._v_mmaps = []
            os.chdir("/dev/shm")
            for fname in glob.glob("t5_%s_%s_*.*" % (self.mmapFilenamePart(), self.numId())):
                f = open(fname, "a+b")
                self._v_mmaps.append(mmap.mmap(f.fileno(), 0))
                f.close()
        param_bytes = self._filterParamsAsBytes()
        for mmap_ in self._v_mmaps:
            mmap_.seek(0)
            mmap_.write(param_bytes)
        self._v_needMmapUpdate = False


    @staticmethod
    def canBeLinked():
        return False


    def channelsText(self):
        return ",".join(self.sinkChannels())


    def controlValuesText(self):
        raise NotImplementedError


    def disconnectLinks(self):
        if not self.canBeLinked():
            return
        link = self.linkedBy()
        if not link:
            return
        link.removeLinkedFilter(self)


    @staticmethod
    def filterName():
        raise NotImplementedError


    def frequencyResponse(self, frequencies=GRAPH_FREQUENCIES):
        raise NotImplementedError


    def frdFilename(self):
        return self._frdFilename


    def frequencyResponsePixmap(self):
        if getattr(self, "_v_frequencyResponsePixmap", None) is None:
            img = QImage.fromData(self._frequencyResponsePng)
            self._v_frequencyResponsePixmap = QPixmap(img)
        return self._v_frequencyResponsePixmap


    def gain(self):
        return self._gain


    def gainFactor(self):
        return dbToGainFactor(self._gain)


    def gatherFrequencyResponsesUpToInput(self, whs):
        if len(self._inputs) == 0:
            b, a = (1, 0, 0), (1, 0, 0)
            whs.append(signal.freqz(b, a, worN=GRAPH_FREQUENCIES, whole=True))
        else:
            whs.append(self.frequencyResponse())
            self._inputs[0].gatherFrequencyResponsesUpToInput(whs)


    def generatePulseaudioConfig(self):
        num_outputs = len(self._outputs)
        if num_outputs == 1:
            output = self._outputs[0]
            ret = output.generatePulseaudioConfig()
            ret += "# %s sink/filter, connect to %s:\n" % (self.sinkName(), output.sinkName())
            ret += "load-module module-ladspa-sink sink_name=%s sink_properties=device.description=%s " \
                   "sink_master=%s plugin=t5_%s label=%s control=%s\n" % \
                   (self.sinkNameWithPrefix(), self.sinkNameWithPrefix(),
                    output.sinkNameWithPrefix(), self.ladspaPluginName(),
                    self.ladspaPluginName(), self.controlValuesText())
        elif num_outputs > 1:
            ret = ""
            for output in self._outputs:
                ret += output.generatePulseaudioConfig()
            ret += "# %s sink/filter, split to %s:\n" % (self.sinkName(), self.outputSinkNamesText())
            ret += "load-module module-combine-sink sink_name=%s sink_properties=device.description=%s " \
                   "channels=%d channel_map=%s slaves=%s\n" % \
                   (self.sinkNameWithPrefixForSplit(), self.sinkNameWithPrefixForSplit(),
                    self.numChannels(), self.channelsText(), self.outputSinkNamesTextWithPrefix())
            ret += "load-module module-ladspa-sink sink_name=%s sink_properties=device.description=%s " \
                   "sink_master=%s plugin=t5_%s label=%s control=%s\n" % \
                   (self.sinkNameWithPrefix(), self.sinkNameWithPrefix(),
                    self.sinkNameWithPrefixForSplit(), self.ladspaPluginName(),
                    self.ladspaPluginName(), self.controlValuesText())
        else:
            ret = ""
        return ret


    def info(self):
        raise NotImplementedError


    def isDirty(self):
        if getattr(self, "_v_dirty", None) is None:
            self._v_dirty = False
        return self._v_dirty

    @staticmethod
    def ladspaPluginName():
        raise NotImplementedError


    def lastModified(self):
        try:
            return self._v_lastModified
        except AttributeError:
            self._v_lastModified = time.time()
        return self._v_lastModified


    def linkedBy(self) -> XoverFilterLink:
        pass


    def markClean(self):
        self._v_dirty = False


    def markDirty(self):
        self._v_dirty = True
        self._v_lastModified = time.time()
        for output in self._outputs:
            output.markDirty()


    @staticmethod
    def mmapFilenamePart():
        raise NotImplementedError


    def outputWidgets(self):
        return map(lambda x: x.widget(), self._outputs)


    def parameterMappings(self):
        return [
            {
                "description": "Overall Gain [dB]",
                "getter": self.gain,
                "setter": self.setGain,
                "min": -20,
                "default": 0,
                "max": 20,
                "scale": "lin",
                "dividerAfter": True,
            },
        ]


    @staticmethod
    def phaseLowerLimit():
        return -200


    @staticmethod
    def phaseUpperLimit():
        return 200


    def repaintWidget(self):
        self._v_widget.updateFrequencyResponseLabel()
        self._v_widget.repaint()


    def resetMmaps(self):
        self._v_mmaps = None


    def restoreWidget(self, drawArea):
        """ restore widget """
        self._v_widget = FilterWidget(drawArea, self, self._widgetX, self._widgetY)
        self._v_widget.show()


    def setFrdFilename(self, fn):
        self._frdFilename = fn
        self._v_dirty = True


    def setFrequencyResponsePng(self, png):
        self._frequencyResponsePng = png
        self._v_frequencyResponsePixmap = None


    def setGain(self, gain):
        self._gain = gain
        self._v_dirty = True
        self._v_needMmapUpdate = True


    def setSinkName(self, sink_name):
        self._sink_name = sink_name


    def sinkName(self):
        return self._sink_name


    def sinkNameWithPrefix(self):
        return "PaXoverRack.%s" % self._sink_name


    def sinkNameWithPrefixForSplit(self):
        return "PaXoverRack.%s.Splitter" % self._sink_name


    def updateFrequencyResponsePng(self):
        w, h = self.frequencyResponse()
        png = self._v_widget.mainWindow().frequencyResponseGraphPngFor(w, h)
        self._frequencyResponsePng = png
        self._v_frequencyResponsePixmap = None
        self._v_widget.repaint()


    def updateOutputFrequencyResponsePngs(self):
        for output in self._outputs:
            output.updateOutputFrequencyResponsePngs()


    def widget(self):
        return self._v_widget


    def _filterParamsAsBytes(self):
        raise NotImplementedError
