try:
    from ZODB.utils import u64
    from persistent import Persistent
except ImportError:
    u64 = None
    Persistent = object
    pass # import errors will be handled in MainWindow

from helpers.functions import testingIdGeneratorGet


class XoverObject(Persistent):
    """
    Crosover Object Base Class
    """
    def __init__(self):
        # noinspection PyArgumentList
        Persistent.__init__(self)
        self._widgetX = 10
        self._widgetY = 10
        self._mono = False


    def applyFilterParams(self):
        """ noop, must be reimplemented by real filters """
        pass


    def disconnectLinks(self):
        pass


    def info(self):
        raise NotImplementedError


    def hexId(self):
        return "%x" % self.numId()


    def mono(self):
        return self._mono


    @staticmethod
    def numChannels():
        return 2


    def numId(self):
        if self._p_oid is None:
            # generate our own ids for testing
            return testingIdGeneratorGet()
        return u64(self._p_oid)


    def resetMmaps(self):
        """ noop, must be reimplemented by real filters """
        pass


    def setMono(self, mono):
        self._mono = mono


    def setWidgetX(self, x):
        self._widgetX = x


    def setWidgetY(self, y):
        self._widgetY = y


    def sinkChannels(self):
        if self.mono():
            return "mono", "mono"
        return "front-left", "front-right"


    def sinkChannelsText(self):
        return ",".join(self.sinkChannels())


    def tempSinkName(self):
        """ generate temporary sink name from numId """
        return "PaXoverSink%s" % self.numId()


    def widgetX(self):
        return self._widgetX


    def widgetY(self):
        return self._widgetY
