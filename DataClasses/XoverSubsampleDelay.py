from DataClasses.XoverDelay import XoverDelay


class XoverSubsampleDelay(XoverDelay):
    """
    Subsample Accurate Delay Filter Class
    """
    def __init(self, sink_name="", gain=0, delay=0):
        super().__init__(sink_name, gain, delay=delay)


    @staticmethod
    def filterName():
        return "Subsample Accurate Delay"


    @staticmethod
    def ladspaPluginName():
        return "subsample_delay"


    @staticmethod
    def mmapFilenamePart():
        return "SubsampleDelay"


    def _coeffsBA(self):
        """ return linear frequency response """
        delaySamples = self.delaySamples()
        delaySubSamples = delaySamples - int(delaySamples)
        ## removed for now
        # prepend <delaySamples> zeros to coefficients
        #b = numpy.zeros(int(self.delaySamples()) + 2)
        #a = numpy.zeros(int(self.delaySamples()) + 2)
        #b[-2] = 1 - delaySubSamples
        #b[-1] = delaySubSamples
        #a[-2] = 1
        return (1 - delaySubSamples, delaySubSamples), (1, 0)
