import numpy

try:
    from persistent.list import PersistentList
except ImportError:
    PersistentList = None # import errors will be handled in MainWindow

from CustomWidgets.InputWidget import InputWidget
from DataClasses.XoverConnectedObject import XoverConnectedObject
from helpers.constants import GRAPH_FREQUENCIES


class XoverInput(XoverConnectedObject):
    """
    Crosover Input Class
    """

    def __init__(self):
        super().__init__()
        self._outputs = PersistentList()
        self._v_widget = None


    @staticmethod
    def aggregatedCoeffsBA():
        return (1,), (1,)


    @staticmethod
    def aggregatedFrequencyResponse(frequencies=GRAPH_FREQUENCIES):
        return frequencies, numpy.ones(len(frequencies))


    def channelsText(self):
        return ",".join(self.sinkChannels())


    def generatePulseaudioConfig(self):
        num_outputs = len(self._outputs)
        if num_outputs == 1:
            output = self._outputs[0]
            ret = output.generatePulseaudioConfig()
            ret += "# Input sink, connect to %s:\n" % output.sinkName()
            ret += "load-module module-remap-sink sink_name=PaXoverRack.Input sink_properties=device.description=PaXoverRack.Input " \
                   "remix=no master=%s channels=%d master_channel_map=%s channel_map=%s\n" % \
                   (output.sinkNameWithPrefix(), self.numChannels(),
                    output.sinkChannelsText(), self.channelsText())
            ret += "set-default-sink PaXoverRack.Input\n"
        elif num_outputs > 1:
            ret = ""
            for output in self._outputs:
                ret += output.generatePulseaudioConfig()
            ret += "# Input sink, split to %s:\n" % self.outputSinkNamesText()
            ret += "load-module module-combine-sink sink_name=PaXoverRack.Input sink_properties=device.description=PaXoverRack.Input " \
                   "channels=%d channel_map=%s slaves=%s\n" % \
                   (self.numChannels(), self.channelsText(), self.outputSinkNamesTextWithPrefix())
            ret += "set-default-sink PaXoverRack.Input\n"
        else:
            ret = ""
        return ret


    def hasOutputConnections(self):
        return len(self._outputs) > 0


    def info(self):
        return ""


    def inputs(self):
        return []


    def outputWidgets(self):
        return map(lambda x: x.widget(), self._outputs)


    def restoreWidget(self, drawArea):
        """ restore widget """
        self._v_widget = InputWidget(drawArea, self, self._widgetX, self._widgetY)
        self._v_widget.show()


    @staticmethod
    def sinkName():
        return "Input"


    def updateOutputFrequencyResponsePngs(self):
        for output in self._outputs:
            output.updateOutputFrequencyResponsePngs()
