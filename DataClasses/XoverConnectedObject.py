try:
    from persistent.list import PersistentList
except ImportError:
    PersistentList = None # import errors will be handled in MainWindow

from DataClasses.XoverObject import XoverObject

class XoverConnectedObject(XoverObject):
    """
    Crosover Connected Object Base Class
    """

    def __init__(self):
        super().__init__()
        self._outputs = PersistentList()
        self._inputs = PersistentList()


    def connect(self, filter_):
        if not filter_ in self._outputs:
            self._outputs.append(filter_)
            filter_.connectReverse(self)


    def connectReverse(self, input_):
        if not input_ in self._inputs:
            self._inputs.append(input_)


    def disconnect(self, filter_):
        if filter_ in self._outputs:
            self._outputs.remove(filter_)
            filter_.disconnectReverse(self)


    def disconnectAll(self):
        for output in self._outputs[:]:
            self.disconnect(output)
        for input_ in self._inputs[:]:
            input_.disconnect(self)


    def disconnectReverse(self, input_):
        if input_ in self._inputs:
            self._inputs.remove(input_)


    def hasInputConnections(self):
        return len(self._inputs) > 0


    def hasOutputConnections(self):
        return len(self._outputs) > 0


    def info(self):
        raise NotImplementedError


    def inputs(self):
        return self._inputs


    def outputs(self):
        return self._outputs


    def outputSinkNamesText(self):
        return ",".join(map(lambda x: x.sinkName(), self._outputs))


    def outputSinkNamesTextWithPrefix(self):
        return ",".join(map(lambda x: "PaXoverRack." + x.sinkName(), self._outputs))


    def walkAndFindReachable(self):
        ret = []
        for o in self._outputs:
            ret.extend(o.walkAndFindReachable())
        return ret
