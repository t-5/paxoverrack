from DataClasses.XoverDelay import XoverDelay


class XoverSampleDelay(XoverDelay):
    """
    Sample Accurate Delay Filter Class
    """
    def __init(self, sink_name="", gain=0, delay=0):
        super().__init__(sink_name, gain, delay=delay)


    @staticmethod
    def filterName():
        return "Sample Accurate Delay"


    @staticmethod
    def ladspaPluginName():
        return "sample_delay"


    @staticmethod
    def mmapFilenamePart():
        return "SampleDelay"


    def _coeffsBA(self):
        """ return linear frequency response """
        ## removed for now
        # prepend <delaySamples> zeros to coefficients
        #ret = numpy.zeros(int(self.delaySamples()) + 1)
        #ret[-1] = 1
        return (1,), (1,)
