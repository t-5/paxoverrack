import struct
from math import pi, sin, cos, sqrt

try:
    from scipy import signal
except ImportError:
    signal = None # import errors are handled in MainWindow.py

from DataClasses.XoverFilter import XoverFilter
from helpers.constants import GRAPH_FREQUENCIES
from helpers.functions import GRAPH_SR



class XoverParamEq(XoverFilter):
    def __init__(self, sink_name="", gain=0,
                 lowShelfFrequency=100, lowShelfGain=0, lowShelfQ=1,
                 eq1Frequency=300, eq1Gain=0, eq1Q=1,
                 eq2Frequency=1000, eq2Gain=0, eq2Q=1,
                 eq3Frequency=3000, eq3Gain=0, eq3Q=1,
                 highShelfFrequency=10000, highShelfGain=0, highShelfQ=1):
        super().__init__(sink_name, gain)
        self._lowShelfFrequency = lowShelfFrequency
        self._lowShelfGain = lowShelfGain
        self._lowShelfQ = lowShelfQ
        self._eq1Frequency = eq1Frequency
        self._eq1Gain = eq1Gain
        self._eq1Q = eq1Q
        self._eq2Frequency = eq2Frequency
        self._eq2Gain = eq2Gain
        self._eq2Q = eq2Q
        self._eq3Frequency = eq3Frequency
        self._eq3Gain = eq3Gain
        self._eq3Q = eq3Q
        self._highShelfFrequency = highShelfFrequency
        self._highShelfGain = highShelfGain
        self._highShelfQ = highShelfQ


    def activeBands(self):
        return len(list(filter(lambda x: x != 0, (self._lowShelfGain,
                                                  self._eq1Gain,
                                                  self._eq2Gain,
                                                  self._eq3Gain,
                                                  self._highShelfGain))))


    def controlValuesText(self):
        """ return control values for ladspa plugin """
        return "%.5f,%.5f,%.5f,%.5f,%.5f,%.5f,%.5f,%.5f,%.5f,%.5f,%.5f,%.5f,%.5f,%.5f,%.5f,%.5f,%.5f" % \
               (self._lowShelfFrequency, self._lowShelfGain, self._lowShelfQ,
                self._eq1Frequency, self._eq1Gain, self._eq1Q,
                self._eq2Frequency, self._eq2Gain, self._eq2Q,
                self._eq3Frequency, self._eq3Gain, self._eq3Q,
                self._highShelfFrequency, self._highShelfGain, self._highShelfQ,
                self._gain, self.numId())

    def eq1Frequency(self):
        return self._eq1Frequency


    def eq1Gain(self):
        return self._eq1Gain


    def eq1Q(self):
        return self._eq1Q


    def eq2Frequency(self):
        return self._eq2Frequency


    def eq2Gain(self):
        return self._eq2Gain


    def eq2Q(self):
        return self._eq2Q


    def eq3Frequency(self):
        return self._eq3Frequency


    def eq3Gain(self):
        return self._eq3Gain


    def eq3Q(self):
        return self._eq3Q


    @staticmethod
    def filterName():
        return "Parametric EQ"


    def frequencyResponse(self, frequencies=GRAPH_FREQUENCIES):
        """ return w, h frequency response components for all filters """
        hs = []
        b, a = self._coeffsBALowShelf()
        w, h = signal.freqz(b, a, worN=frequencies, whole=True)
        hs.append(h)
        b, a = self._coeffsBAPeaking1()
        w, h = signal.freqz(b, a, worN=frequencies, whole=True)
        hs.append(h)
        b, a = self._coeffsBAPeaking2()
        w, h = signal.freqz(b, a, worN=frequencies, whole=True)
        hs.append(h)
        b, a = self._coeffsBAPeaking3()
        w, h = signal.freqz(b, a, worN=frequencies, whole=True)
        hs.append(h)
        b, a = self._coeffsBAHighShelf()
        w, h = signal.freqz(b, a, worN=frequencies, whole=True)
        hs.append(h)
        # multiply all values in the five calculated frequency response hs
        for i in range(1,5):
            for j in range(0, len(hs[0])):
                hs[0][j] = hs[i][j] * hs[0][j]
        return w, hs[0] * self.gainFactor()


    @staticmethod
    def gainGraphLowerLimit():
        return -20


    @staticmethod
    def gainGraphUpperLimit():
        return 20


    def highShelfFrequency(self):
        return self._highShelfFrequency


    def highShelfGain(self):
        return self._highShelfGain


    def highShelfQ(self):
        return self._highShelfQ


    def info(self):
        return "Gain: %0.1fdB | Active Bands: %d" % (self._gain, self.activeBands())


    @staticmethod
    def ladspaPluginName():
        return "3band_parameq_with_shelves"


    def lowShelfFrequency(self):
        return self._lowShelfFrequency


    def lowShelfGain(self):
        return self._lowShelfGain


    def lowShelfQ(self):
        return self._lowShelfQ


    @staticmethod
    def mmapFilenamePart():
        return "3BandParamEqWithShelves"


    def parameterMappings(self):
        if getattr(self, "_v_cached_parameterMappings", None) is None:
            ret = super().parameterMappings()[:]
            ret.append({
                "description": "Low Shelf Frequency [Hz]",
                "getter": self.lowShelfFrequency,
                "setter": self.setLowShelfFrequency,
                "min": 2,
                "default": 100,
                "max": 20000,
                "scale": "log2",
            })
            ret.append({
                "description": "Low Shelf Gain [dB]",
                "getter": self.lowShelfGain,
                "setter": self.setLowShelfGain,
                "min": -20,
                "default": 0,
                "max": 20,
                "scale": "lin",
            })
            ret.append({
                "description": "Low Shelf Q",
                "getter": self.lowShelfQ,
                "setter": self.setLowShelfQ,
                "min": 0.1,
                "default": 0.707,
                "max": 10,
                "scale": "log10",
                "dividerAfter": True,
            })
            ret.append({
                "description": "EQ1 Center Frequency [Hz]",
                "getter": self.eq1Frequency,
                "setter": self.setEq1Frequency,
                "min": 2,
                "default": 300,
                "max": 20000,
                "scale": "log2",
            })
            ret.append({
                "description": "EQ1 Gain [dB]",
                "getter": self.eq1Gain,
                "setter": self.setEq1Gain,
                "min": -20,
                "default": 0,
                "max": 20,
                "scale": "lin",
            })
            ret.append({
                "description": "EQ1 Q",
                "getter": self.eq1Q,
                "setter": self.setEq1Q,
                "min": 0.1,
                "default": 0.707,
                "max": 10,
                "scale": "log10",
                "dividerAfter": True,
            })
            ret.append({
                "description": "EQ2 Center Frequency [Hz]",
                "getter": self.eq2Frequency,
                "setter": self.setEq2Frequency,
                "min": 2,
                "default": 1000,
                "max": 20000,
                "scale": "log2",
            })
            ret.append({
                "description": "EQ2 Gain [dB]",
                "getter": self.eq2Gain,
                "setter": self.setEq2Gain,
                "min": -20,
                "default": 0,
                "max": 20,
                "scale": "lin",
            })
            ret.append({
                "description": "EQ2 Q",
                "getter": self.eq2Q,
                "setter": self.setEq2Q,
                "min": 0.1,
                "default": 0.707,
                "max": 10,
                "scale": "log10",
                "dividerAfter": True,
            })
            ret.append({
                "description": "EQ3 Center Frequency [Hz]",
                "getter": self.eq3Frequency,
                "setter": self.setEq3Frequency,
                "min": 2,
                "default": 3000,
                "max": 20000,
                "scale": "log2",
            })
            ret.append({
                "description": "EQ3 Gain [dB]",
                "getter": self.eq3Gain,
                "setter": self.setEq3Gain,
                "min": -20,
                "default": 0,
                "max": 20,
                "scale": "lin",
            })
            ret.append({
                "description": "EQ3 Q",
                "getter": self.eq3Q,
                "setter": self.setEq3Q,
                "min": 0.1,
                "default": 0.707,
                "max": 10,
                "scale": "log10",
                "dividerAfter": True,
            })
            ret.append({
                "description": "High Shelf Frequency [Hz]",
                "getter": self.highShelfFrequency,
                "setter": self.setHighShelfFrequency,
                "min": 2,
                "default": 10000,
                "max": 20000,
                "scale": "log2",
            })
            ret.append({
                "description": "High Shelf Gain [dB]",
                "getter": self.highShelfGain,
                "setter": self.setHighShelfGain,
                "min": -20,
                "default": 0,
                "max": 20,
                "scale": "lin",
            })
            ret.append({
                "description": "High Shelf Q",
                "getter": self.highShelfQ,
                "setter": self.setHighShelfQ,
                "min": 0.1,
                "default": 0.707,
                "max": 10,
                "scale": "log10",
            })
            self._v_cached_parameterMappings = ret
        return self._v_cached_parameterMappings


    def setLowShelfFrequency(self, lowShelfFrequency):
        self._lowShelfFrequency = lowShelfFrequency
        self._v_dirty = True
        self._v_needMmapUpdate = True

    def setLowShelfGain(self, lowShelfGain):
        self._lowShelfGain = lowShelfGain
        self._v_dirty = True
        self._v_needMmapUpdate = True

    def setLowShelfQ(self, lowShelfQ):
        self._lowShelfQ = lowShelfQ
        self._v_dirty = True
        self._v_needMmapUpdate = True

    def setEq1Frequency(self, eq1Frequency):
        self._eq1Frequency = eq1Frequency
        self._v_dirty = True
        self._v_needMmapUpdate = True

    def setEq1Gain(self, eq1Gain):
        self._eq1Gain = eq1Gain
        self._v_dirty = True
        self._v_needMmapUpdate = True

    def setEq1Q(self, eq1Q):
        self._eq1Q = eq1Q
        self._v_dirty = True
        self._v_needMmapUpdate = True

    def setEq2Frequency(self, eq2Frequency):
        self._eq2Frequency = eq2Frequency
        self._v_dirty = True
        self._v_needMmapUpdate = True

    def setEq2Gain(self, eq2Gain):
        self._eq2Gain = eq2Gain
        self._v_dirty = True
        self._v_needMmapUpdate = True

    def setEq2Q(self, eq2Q):
        self._eq2Q = eq2Q
        self._v_dirty = True
        self._v_needMmapUpdate = True

    def setEq3Frequency(self, eq3Frequency):
        self._eq3Frequency = eq3Frequency
        self._v_dirty = True
        self._v_needMmapUpdate = True

    def setEq3Gain(self, eq3Gain):
        self._eq3Gain = eq3Gain
        self._v_dirty = True
        self._v_needMmapUpdate = True

    def setEq3Q(self, eq3Q):
        self._eq3Q = eq3Q
        self._v_dirty = True
        self._v_needMmapUpdate = True

    def setHighShelfFrequency(self, highShelfFrequency):
        self._highShelfFrequency = highShelfFrequency
        self._v_dirty = True
        self._v_needMmapUpdate = True

    def setHighShelfGain(self, highShelfGain):
        self._highShelfGain = highShelfGain
        self._v_dirty = True
        self._v_needMmapUpdate = True

    def setHighShelfQ(self, highShelfQ):
        self._highShelfQ = highShelfQ
        self._v_dirty = True
        self._v_needMmapUpdate = True


    def _coeffsBAHighShelf(self):
        """ return filter coefficients of high shelf filter """
        w0 = 2.0 * pi * self._highShelfFrequency / GRAPH_SR[0]
        alpha = sin(w0) / (2.0 * self._highShelfQ)
        A = pow(10, self._highShelfGain / 40.0)
        b0 =      A*((A+1.0) + (A-1.0)*cos(w0) + 2.0*sqrt(A)*alpha)
        b1 = -2.0*A*((A-1.0) + (A+1.0)*cos(w0))
        b2 =      A*((A+1.0) + (A-1.0)*cos(w0) - 2.0*sqrt(A)*alpha)
        a0 =         (A+1.0) - (A-1.0)*cos(w0) + 2.0*sqrt(A)*alpha
        a1 =    2.0*((A-1.0) - (A+1.0)*cos(w0))
        a2 =         (A+1.0) - (A-1.0)*cos(w0) - 2.0*sqrt(A)*alpha
        return (b0, b1, b2), (a0, a1, a2)


    def _coeffsBALowShelf(self):
        """ return filter coefficients of low shelf filter """
        w0 = 2.0 * pi * self._lowShelfFrequency / GRAPH_SR[0]
        alpha = sin(w0) / (2.0 * self._lowShelfQ)
        A = pow(10, self._lowShelfGain / 40.0)
        b0 =     A*( (A+1.0) - (A-1.0)*cos(w0) + 2.0*sqrt(A)*alpha)
        b1 = 2.0*A*( (A-1.0) - (A+1.0)*cos(w0))
        b2 =     A*( (A+1.0) - (A-1.0)*cos(w0) - 2.0*sqrt(A)*alpha)
        a0 =         (A+1.0) + (A-1.0)*cos(w0) + 2.0*sqrt(A)*alpha
        a1 =  -2.0*( (A-1.0) + (A+1.0)*cos(w0))
        a2 =         (A+1.0) + (A-1.0)*cos(w0) - 2.0*sqrt(A)*alpha
        return (b0, b1, b2), (a0, a1, a2)


    @staticmethod
    def _coeffsBAPeaking(f, g, q):
        """ return filter coefficients of peaking eq filter """
        w0 = 2.0 * pi * f / GRAPH_SR[0]
        alpha = sin(w0) / (2.0 * q)
        A = pow(10, g / 40.0)
        b0 =  1.0 + alpha * A
        b1 = -2.0 * cos(w0)
        b2 =  1.0 - alpha * A
        a0 =  1.0 + alpha / A
        a1 = -2.0 * cos(w0)
        a2 =  1.0 - alpha / A
        return (b0, b1, b2), (a0, a1, a2)


    def _coeffsBAPeaking1(self):
        return self._coeffsBAPeaking(self._eq1Frequency, self.eq1Gain(), self._eq1Q)


    def _coeffsBAPeaking2(self):
        return self._coeffsBAPeaking(self._eq2Frequency, self.eq2Gain(), self._eq2Q)


    def _coeffsBAPeaking3(self):
        return self._coeffsBAPeaking(self._eq3Frequency, self.eq3Gain(), self._eq3Q)


    def _filterParamsAsBytes(self):
        """ return packed bytes object for writing to mmap """
        return struct.pack("fffffffffffffffff",
                           1.0, # changed flag
                           self._lowShelfFrequency,
                           self._lowShelfGain,
                           self._lowShelfQ,
                           self._eq1Frequency,
                           self._eq1Gain,
                           self._eq1Q,
                           self._eq2Frequency,
                           self._eq2Gain,
                           self._eq2Q,
                           self._eq3Frequency,
                           self._eq3Gain,
                           self._eq3Q,
                           self._highShelfFrequency,
                           self._highShelfGain,
                           self._highShelfQ,
                           self._gain)
