# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Ui_FrequencyResponseDialog.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_FrequencyResponseDialog(object):
    def setupUi(self, FrequencyResponseDialog):
        FrequencyResponseDialog.setObjectName("FrequencyResponseDialog")
        FrequencyResponseDialog.resize(500, 240)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(FrequencyResponseDialog.sizePolicy().hasHeightForWidth())
        FrequencyResponseDialog.setSizePolicy(sizePolicy)
        FrequencyResponseDialog.setMinimumSize(QtCore.QSize(500, 240))
        FrequencyResponseDialog.setMaximumSize(QtCore.QSize(500, 240))
        self.frequencResponseLabel = QtWidgets.QLabel(FrequencyResponseDialog)
        self.frequencResponseLabel.setGeometry(QtCore.QRect(5, 5, 490, 191))
        self.frequencResponseLabel.setText("")
        self.frequencResponseLabel.setScaledContents(True)
        self.frequencResponseLabel.setObjectName("frequencResponseLabel")
        self.pushButtonClose = QtWidgets.QPushButton(FrequencyResponseDialog)
        self.pushButtonClose.setGeometry(QtCore.QRect(200, 200, 109, 35))
        self.pushButtonClose.setObjectName("pushButtonClose")

        self.retranslateUi(FrequencyResponseDialog)
        QtCore.QMetaObject.connectSlotsByName(FrequencyResponseDialog)

    def retranslateUi(self, FrequencyResponseDialog):
        _translate = QtCore.QCoreApplication.translate
        FrequencyResponseDialog.setWindowTitle(_translate("FrequencyResponseDialog", "Dialog"))
        self.pushButtonClose.setText(_translate("FrequencyResponseDialog", "Close"))
