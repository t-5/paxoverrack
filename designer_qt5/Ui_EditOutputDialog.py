# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Ui_EditOutputDialog.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_EditOutputDialog(object):
    def setupUi(self, EditOutputDialog):
        EditOutputDialog.setObjectName("EditOutputDialog")
        EditOutputDialog.resize(400, 311)
        self.buttonBox = QtWidgets.QDialogButtonBox(EditOutputDialog)
        self.buttonBox.setGeometry(QtCore.QRect(50, 270, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayoutWidget = QtWidgets.QWidget(EditOutputDialog)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(10, 10, 446, 251))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.comboBoxOutputSink = QtWidgets.QComboBox(self.gridLayoutWidget)
        self.comboBoxOutputSink.setObjectName("comboBoxOutputSink")
        self.gridLayout.addWidget(self.comboBoxOutputSink, 2, 1, 1, 1)
        self.labelSinkNameHints = QtWidgets.QLabel(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.labelSinkNameHints.setFont(font)
        self.labelSinkNameHints.setObjectName("labelSinkNameHints")
        self.gridLayout.addWidget(self.labelSinkNameHints, 1, 1, 1, 1)
        self.SinkName = QtWidgets.QLabel(self.gridLayoutWidget)
        self.SinkName.setObjectName("SinkName")
        self.gridLayout.addWidget(self.SinkName, 0, 0, 1, 1)
        self.lineEditSinkName = QtWidgets.QLineEdit(self.gridLayoutWidget)
        self.lineEditSinkName.setObjectName("lineEditSinkName")
        self.gridLayout.addWidget(self.lineEditSinkName, 0, 1, 1, 1)
        self.labelEmpty = QtWidgets.QLabel(self.gridLayoutWidget)
        self.labelEmpty.setText("")
        self.labelEmpty.setObjectName("labelEmpty")
        self.gridLayout.addWidget(self.labelEmpty, 1, 0, 1, 1)
        self.labelOutputSinkName = QtWidgets.QLabel(self.gridLayoutWidget)
        self.labelOutputSinkName.setObjectName("labelOutputSinkName")
        self.gridLayout.addWidget(self.labelOutputSinkName, 2, 0, 1, 1)
        self.label = QtWidgets.QLabel(self.gridLayoutWidget)
        self.label.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 3, 0, 1, 1)
        self.listWidgetOutputChannels = QtWidgets.QListWidget(self.gridLayoutWidget)
        self.listWidgetOutputChannels.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
        self.listWidgetOutputChannels.setObjectName("listWidgetOutputChannels")
        self.gridLayout.addWidget(self.listWidgetOutputChannels, 3, 1, 1, 1)
        self.labelMono = QtWidgets.QLabel(self.gridLayoutWidget)
        self.labelMono.setObjectName("labelMono")
        self.gridLayout.addWidget(self.labelMono, 4, 0, 1, 1)
        self.labelSwapChannels = QtWidgets.QLabel(self.gridLayoutWidget)
        self.labelSwapChannels.setObjectName("labelSwapChannels")
        self.gridLayout.addWidget(self.labelSwapChannels, 5, 0, 1, 1)
        self.checkBoxMono = QtWidgets.QCheckBox(self.gridLayoutWidget)
        self.checkBoxMono.setObjectName("checkBoxMono")
        self.gridLayout.addWidget(self.checkBoxMono, 4, 1, 1, 1)
        self.checkBoxSwapChannels = QtWidgets.QCheckBox(self.gridLayoutWidget)
        self.checkBoxSwapChannels.setObjectName("checkBoxSwapChannels")
        self.gridLayout.addWidget(self.checkBoxSwapChannels, 5, 1, 1, 1)

        self.retranslateUi(EditOutputDialog)
        self.buttonBox.accepted.connect(EditOutputDialog.accept)
        self.buttonBox.rejected.connect(EditOutputDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(EditOutputDialog)
        EditOutputDialog.setTabOrder(self.lineEditSinkName, self.comboBoxOutputSink)
        EditOutputDialog.setTabOrder(self.comboBoxOutputSink, self.listWidgetOutputChannels)
        EditOutputDialog.setTabOrder(self.listWidgetOutputChannels, self.buttonBox)

    def retranslateUi(self, EditOutputDialog):
        _translate = QtCore.QCoreApplication.translate
        EditOutputDialog.setWindowTitle(_translate("EditOutputDialog", "Dialog"))
        self.labelSinkNameHints.setText(_translate("EditOutputDialog", "(this name must be unique!)"))
        self.SinkName.setText(_translate("EditOutputDialog", "Internal name:"))
        self.labelOutputSinkName.setText(_translate("EditOutputDialog", "Output Sink:"))
        self.label.setText(_translate("EditOutputDialog", "Output Channels:"))
        self.labelMono.setText(_translate("EditOutputDialog", "Stereo/Mono:"))
        self.labelSwapChannels.setText(_translate("EditOutputDialog", "Channel swapping:"))
        self.checkBoxMono.setText(_translate("EditOutputDialog", "Downmix to mono"))
        self.checkBoxSwapChannels.setText(_translate("EditOutputDialog", "Swap left/right"))
