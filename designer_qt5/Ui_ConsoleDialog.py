# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Ui_ConsoleDialog.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_ConsoleDialog(object):
    def setupUi(self, ConsoleDialog):
        ConsoleDialog.setObjectName("ConsoleDialog")
        ConsoleDialog.setWindowModality(QtCore.Qt.WindowModal)
        ConsoleDialog.resize(600, 400)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(ConsoleDialog.sizePolicy().hasHeightForWidth())
        ConsoleDialog.setSizePolicy(sizePolicy)
        ConsoleDialog.setMinimumSize(QtCore.QSize(600, 400))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/ConsoleDialog/console.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        ConsoleDialog.setWindowIcon(icon)
        self.verticalLayout = QtWidgets.QVBoxLayout(ConsoleDialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.textEditConsole = QtWidgets.QTextEdit(ConsoleDialog)
        font = QtGui.QFont()
        font.setFamily("Noto Mono")
        self.textEditConsole.setFont(font)
        self.textEditConsole.setReadOnly(True)
        self.textEditConsole.setObjectName("textEditConsole")
        self.verticalLayout.addWidget(self.textEditConsole)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.checkBoxAutoClose = QtWidgets.QCheckBox(ConsoleDialog)
        self.checkBoxAutoClose.setObjectName("checkBoxAutoClose")
        self.horizontalLayout.addWidget(self.checkBoxAutoClose)
        self.spinBoxAutoClose = QtWidgets.QSpinBox(ConsoleDialog)
        self.spinBoxAutoClose.setMinimum(1)
        self.spinBoxAutoClose.setObjectName("spinBoxAutoClose")
        self.horizontalLayout.addWidget(self.spinBoxAutoClose)
        self.labelAutoClose = QtWidgets.QLabel(ConsoleDialog)
        self.labelAutoClose.setObjectName("labelAutoClose")
        self.horizontalLayout.addWidget(self.labelAutoClose)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.pushButtonClose = QtWidgets.QPushButton(ConsoleDialog)
        self.pushButtonClose.setObjectName("pushButtonClose")
        self.horizontalLayout.addWidget(self.pushButtonClose)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(ConsoleDialog)
        QtCore.QMetaObject.connectSlotsByName(ConsoleDialog)

    def retranslateUi(self, ConsoleDialog):
        _translate = QtCore.QCoreApplication.translate
        ConsoleDialog.setWindowTitle(_translate("ConsoleDialog", "Pulseaudio Crossover Rack - Console"))
        self.checkBoxAutoClose.setText(_translate("ConsoleDialog", "Automatically close after "))
        self.labelAutoClose.setText(_translate("ConsoleDialog", "s"))
        self.pushButtonClose.setText(_translate("ConsoleDialog", "Close"))
import PaXoverRack_rc
