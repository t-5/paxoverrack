# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Ui_EditFilterDialog.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_EditFilterDialog(object):
    def setupUi(self, EditFilterDialog):
        EditFilterDialog.setObjectName("EditFilterDialog")
        EditFilterDialog.setWindowModality(QtCore.Qt.ApplicationModal)
        EditFilterDialog.resize(500, 608)
        EditFilterDialog.setMinimumSize(QtCore.QSize(500, 400))
        EditFilterDialog.setMaximumSize(QtCore.QSize(500, 16777215))
        self.scrollArea = QtWidgets.QScrollArea(EditFilterDialog)
        self.scrollArea.setGeometry(QtCore.QRect(5, 200, 490, 400))
        self.scrollArea.setMinimumSize(QtCore.QSize(0, 0))
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 488, 398))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.replacementFrame = QtWidgets.QFrame(EditFilterDialog)
        self.replacementFrame.setGeometry(QtCore.QRect(5, 5, 490, 191))
        self.replacementFrame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.replacementFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.replacementFrame.setObjectName("replacementFrame")

        self.retranslateUi(EditFilterDialog)
        QtCore.QMetaObject.connectSlotsByName(EditFilterDialog)

    def retranslateUi(self, EditFilterDialog):
        _translate = QtCore.QCoreApplication.translate
        EditFilterDialog.setWindowTitle(_translate("EditFilterDialog", "Dialog"))
